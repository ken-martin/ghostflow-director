# End-to-end tests

This is where testing of `ghostflow-director` from start to finish happens. It
starts by setting up a pristine Gitlab and registering an administrator user.
It then sets up a local git repository as well as users and repositories on
Gitlab.

The main bits of the test occurs at the bottom of the script where merge
requests are opened, comments made and checked, etc.

## Status

This is the status of the bits successfully tested by the script so far. Some
bits are essentially tested by virtue of just being required for other things
to work, but it would be nice to have things be explicit.

  * [x] clone action tested
    - [x] make sure repos are cloned properly
    - [x] make sure submodules are linked properly
    - [x] check refspecs
  * [x] check action tested
    - [x] `Do: check` always fires
    - [x] anyone can check branches
    - [x] check happens on MR open
    - [x] check happens on MR update
    - [x] behavior when the MR is closed
    - [x] faked `ghostflow-branch-check` status is ignored
    - [x] ensure that award was given to command comments
    - [x] test combined with other actions
  * [x] stage action tested
    - [x] permissions enforced
    - [x] fails when checks are failing
    - [x] `Do: stage` on already staged branch
    - [x] `Do: unstage` on non-staged branch
    - [x] policies work
      - [x] unstage
      - [x] ignore
      - [x] restage
    - [x] `quiet` works
    - [x] `keep_topics` works
    - [x] behavior when the MR is closed
    - [x] tagging stage with jobs works
    - [x] ensure that award was given to command comments
    - [x] test combined with other actions
    - [x] `Do: unstage` can be performed by submitter
    - [x] error when not available
  * [ ] merge action tested
    - [x] permissions enforced
    - [x] fails when checks are failing
    - [x] `Do: merge` on already merged branch
    - [x] `Do: merge` on staged branch
    - [x] `quiet` works
    - [x] `log_limit` works
    - [x] behavior when the MR is closed
    - [x] ensure that award was given to command comments
    - [x] test combined with other actions
    - [x] conflict messages
    - [ ] push is retried
    - [x] error when not available
    - [ ] source branch is removed if requested (needs Gitlab support)

# Requirements

The requirements of the script are minimal. This is to help keep it easy to
use.

# Tools

  * docker: used to run Gitlab and must be usable by the user running the test;
  * jq: used to process JSON in shell;
  * curl: used to communicate with Gitlab for the script's purposes; and
  * git: used to create the repository and check the results of the director.

## Environment

  * `HOSTNAME`: This needs to be a routable hostname the container can use to
    contact the `webhook-listen` binary.
  * `PATH`: Must be set so that `webhook-listen` and `ghostflow-director` are
    available.

# Logs

Everything from the tests should go into the `ghostflow-director-test-tmp`
directory. Files of note:

  * `director-test-$USER` and `director-test-$USER.pub`: SSH keys for the users;
  * `ghostflow-director.json`: the configuration file for ghostflow-director;
  * `ghostflow-director.log`: log file from `ghostflow-director`;
  * `secrets.json`: secrets file for ghostflow-director;
  * `ssh_config`: SSH configuration file used by the tests;
  * `webhook-listen.log`: log file from `webhook-listen`;
  * `webhook.json`: the configuration file for `webhook-listen`;
  * `queue/`: the queue of events dropped from `webhook-listen` for
    `ghostflow-director`;
  * `repo/`: local working repository for the tests; and
  * `work/`: working directory for `ghostflow-director`

# Debugging

The following commands are useful for debugging the tests:

  * `journalctl --user SYSLOG_IDENTIFIER={ghostflow-director,webhook-listen} --since=$TIME`:
    View the explicit logging of the tools.
  * `docker exec -it ghostflow-director-gitlab-test cat $LOGFILE`: View the logs of gitlab
    itself
    - `/var/log/gitlab/gitlab-rails/production.log`: HTTP queries
    - `/var/log/gitlab/gitlab-rails/sidekiq.log`: event/webhook logs
  * Logging into the Gitlab instance inside of the container may be done with
    the `root` user, password `ghostflow-director-test`. If a
    non-administrator login is wanted, all users share the same password. The
    IP is printed at the beginning, but may also be discovered by running
    `docker inspect ghostflow-director-gitlab-test | jq '.[0].NetworkSettings.IPAddress' | tr -d \"`.
  * The docker instance is torn down unless the `keep_docker` environment
    variable is non-empty.

# Cleanup

The script cleans up after itself upon success, but leaves the container
running when things fail so that debugging may occur. The following will clean up the tests:

```sh
docker stop ghostflow-director-gitlab-test
docker rm ghostflow-director-gitlab-test
killall webhook-listen
killall ghostflow-director
```

The script takes care of cleaning up the temporary directory before running.

# Tools

The following tools are available to inspect the outcome of the tests:

  * `inspect-delays.py`: Gathers statistics from the job files about the
    longest delay seen between when an event occurred and when it was processed.

# Known Issues

  * Gitlab can be a pain sometimes with this setup. Events are delayed randomly
    for large amounts of time (20 seconds seen). There are delays in the test
    in an attempt to smooth over this problem, but it hasn't been fool-proof.
  * Sometimes startup fails with a random 422 error from curl; cause unknown.
