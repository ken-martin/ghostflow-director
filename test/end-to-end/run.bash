#!/usr/bin/env bash

# Error out if a command fails.
set -e

readonly docker_name="ghostflow-director-gitlab-test"
readonly admin_password="ghostflow-director-test"
readonly webhook_port="50000"

__log_state="print_date"
log () {
    local color="$1"
    readonly color
    shift

    case "$__log_state" in
        no_date)
            ;;
        print_date)
            echo >&2 -n "$( date ) "
            ;;
    esac

    echo >&2 -n "$color"
    echo >&2 "$@"$'\033[00m'

    if [ "$1" = "-n" ]; then
        __log_state="no_date"
    else
        __log_state="print_date"
    fi
}

info () {
    log $'\033[32m' "$@"
}

warn () {
    log $'\033[33m' "$@"
}

error () {
    log $'\033[31m\033[01m' "$@"
}

die () {
    error "error:" "$@"
    return 1
}

require_binary () {
    local name="$1"
    readonly name
    shift

    which "$name" > /dev/null || \
        die "It appears that \`$name\` is not available in \`$PATH\`"
}

require_binary "docker"
require_binary "jq"
require_binary "pwgen"
require_binary "md5sum"
require_binary "sha256sum"
require_binary "sha512sum"
require_binary "ghostflow-director"
require_binary "webhook-listen"

# Check that two strings are the same and error out with a useful message if
# not.
#
# check_string <actual> <expected>
check_string () {
    local actual="$1"
    readonly actual
    shift

    local expected="$1"
    readonly expected
    shift

    if ! [ "$actual" = "$expected" ]; then
        die "unexpected output:"$'\n'"actual:   $actual"$'\n'"expected: $expected"
    fi
}

if [ -z "$DOCKER_RUNNING" ]; then
    info "Building the docker image..."

    # Build the docker container.
    docker build --no-cache . > /dev/null

    info "Running the docker image..."

    # Run the docker container.
    readonly docker_id="$( docker run --detach --hostname="$docker_name" --name="$docker_name" docker.io/gitlab/gitlab-ce:latest )"

    info "To watch the docker output, run: \`docker attach $docker_name\`"

    # Extract the IP from the container.
    info -n "Extracting the IP of the docker image..."
    readonly gitlab_addr="$( docker inspect "$docker_name" | jq '.[0].NetworkSettings.IPAddress' | tr -d \" )"
    info "$gitlab_addr"
else
    readonly gitlab_addr="$1"
    shift
fi
readonly docker_id

# Wait for docker to start.
info -n "Waiting for Gitlab to be available..."

while ! curl --fail "http://$gitlab_addr" > /dev/null 2> /dev/null; do
    info -n "."
    sleep 5
done

info "done."

# Fetches from a URL in a loop, handling 5xx errors as transient since Gitlab
# has a tendency to put nginx up before the Ruby bits are listening on the
# other side.
#
# loop_fetch <curl args>... > <curl output>
loop_fetch () {
    local stdout="$tmpdir/loop_fetch.out"
    readonly stdout

    local stderr="$tmpdir/loop_fetch.err"
    readonly stderr

    while true; do
        if curl --fail "$@" > "$stdout" 2> "$stderr"; then
            break
        else
            # It seems that we sometimes get 5xx errors with all of our rapid
            # API requests. 409 and 422 have also cropped up.
            if grep -q -e 'error: 5..' "$stderr"; then
                cat >&2 "$stderr"
                cat >&2 "$stdout"
                warn "warn: got a 5xx error; retrying"
                sleep 2
            else
                cat >&2 "$stderr"
                cat >&2 "$stdout"
                return 1
            fi
        fi
    done

    cat "$stdout"
}

# Set up and clear out the temporary directory.
readonly tmpdir="$PWD/ghostflow-director-test-tmp"
rm -rf "$tmpdir"
mkdir -p "$tmpdir"

# Set the administrator password.
readonly cookie_file="$tmpdir/gitlab_cookies.txt"

extract_value () {
    sed -n -e '/value/s/.*value="\([^"]*\)".*/\1/p'
}

extract_csrf () {
    grep -e "authenticity_token" | \
        tr " " $'\n' | \
        grep -A1 -e "authenticity_token" | \
        extract_value | \
        head -n1 | \
        sed -e 's/+/%2B/g'
}

# Fetch the password reset form.
info "Setting the administrator password..."

readonly password_reset_file="$tmpdir/password_reset_extraction.txt"
loop_fetch \
    --cookie-jar "$cookie_file" \
    --include \
    --location \
    "http://$gitlab_addr" \
    > "$password_reset_file"

# Set the password.
readonly set_session="$( cat "$cookie_file" | grep -e "_gitlab_session" | cut -d $'\t' -f7 )"
readonly reset_token="$( cat "$password_reset_file" | grep -e "^Location: " | sed -ne '/reset_password_token/s/.*?reset_password_token=\(.*\).$/\1/p' )"
readonly reset_csrf_token="$( cat "$password_reset_file" | extract_csrf )"
loop_fetch \
    --header "Cookie: _gitlab_session=$set_session" \
    --data "authenticity_token=$reset_csrf_token" \
    --data "user[reset_password_token]=$reset_token" \
    --data "user[password]=$admin_password" \
    --data "user[password_confirmation]=$admin_password" \
    --data "_method=put" \
    --data "utf8=✓" \
    --data "commit=Change+your+password" \
    "http://$gitlab_addr/users/password" \
    > /dev/null

# Sign in as the admin user.
info "Signing in..."

# Fetch the signin form.
readonly signin_file="$tmpdir/signin_extraction.txt"
loop_fetch \
    --cookie-jar "$cookie_file" \
    --location \
    "http://$gitlab_addr/users/sign_in" \
    > "$signin_file"

readonly signin_session="$( cat "$cookie_file" | grep -e "_gitlab_session" | cut -d $'\t' -f7 )"
readonly signin_csrf_token="$( cat "$signin_file" | extract_csrf )"
loop_fetch \
    --verbose \
    --header "Cookie: _gitlab_session=$signin_session" \
    --cookie-jar "$cookie_file" \
    --data "authenticity_token=$signin_csrf_token" \
    --data "user[login]=root" \
    --data "user[password]=$admin_password" \
    --data "user[remember_me]=0" \
    --data "utf8=✓" \
    --data "commit=Sign+in" \
    "http://$gitlab_addr/users/sign_in" \
    > /dev/null

# Fetch the account page.
info "Extracting the private token..."

readonly login_session="$( cat "$cookie_file" | grep -e "_gitlab_session" | cut -d $'\t' -f7 )"
readonly account_file="$tmpdir/account_extraction.txt"
loop_fetch \
    --header "Cookie: _gitlab_session=$login_session" \
    --location \
    "http://$gitlab_addr/profile/account" \
    > "$account_file"

# Extract the private token from the account page.
readonly private_token="$( cat "$account_file" | grep -A1 -e "Private token" | extract_value )"

# Add the webhook listener as a system hook.
info "Adding the system hook..."

# Grab the admin hooks page.
readonly admin_hook_file="$tmpdir/admin_hook_extraction.txt"
loop_fetch \
    --header "Cookie: _gitlab_session=$login_session" \
    --location \
    "http://$gitlab_addr/admin/hooks" \
    > "$admin_hook_file"

# Register the webhook listener.
readonly admin_hook_csrf_token="$( cat "$admin_hook_file" | extract_csrf )"
loop_fetch \
    --header "Cookie: _gitlab_session=$login_session" \
    --data "authenticity_token=$admin_hook_csrf_token" \
    --data "hook[url]=http://$HOSTNAME:$webhook_port/gitlab" \
    --data "hook[token]=" \
    --data "hook[push_events]=1" \
    --data "hook[tag_push_events]=1" \
    --data "hook[enable_ssl_verification]=0" \
    --data "utf8=✓" \
    "http://$gitlab_addr/admin/hooks" \
    > /dev/null

# XXX(gitlab): Gitlab has a tendency to get 409 Conflict problems if we start
# testing too soon. This is probably way more time than is necessary.
info -n "Sleeping for 15 seconds to give gitlab some time to start..."
for x in $( seq 1 15 ); do
    sleep 1
    info -n "$x..."
done
info ""

# Start the webhook listener.
info "Starting the webhook listener..."

readonly queuedir="$tmpdir/queue"
mkdir -p "$queuedir"

sed -e "s,PATH,$queuedir," \
    < "webhook.json.in" \
    > "$tmpdir/webhook.json"
RUST_BACKTRACE=1 webhook-listen -dd --address "$HOSTNAME:$webhook_port" --config "$tmpdir/webhook.json" > "$tmpdir/webhook-listen.log" 2>&1 &

# Write the SSH configuration file.
cat > "$tmpdir/ssh_config" <<EOF
Host *
    StrictHostKeyChecking no
    UserKnownHostsFile /dev/null
Host $docker_name
    HostName $gitlab_addr
    IdentityFile "$tmpdir/ghostflow-director-test-root"
EOF

# Use the configuration file.
GIT_SSH_COMMAND="ssh -F $tmpdir/ssh_config"
export GIT_SSH_COMMAND

# Set up ghostflow-director.
info "Starting ghostflow-director..."

readonly workdir="$tmpdir/work"
mkdir -p "$workdir"

sed -e "s/TOKEN/$private_token/" \
    < "secrets.json.in" \
    > "$tmpdir/secrets.json"

sed -e "s,WORKDIR,$workdir," \
    -e "s,QUEUE,$queuedir," \
    -e "s,GITLAB_HOST,$gitlab_addr," \
    -e "s,SECRETS,$tmpdir/secrets.json," \
    -e "s,HOSTNAME,$HOSTNAME," \
    < "ghostflow-director-boot.yaml.in" \
    > "$tmpdir/ghostflow-director.yaml"
RUST_BACKTRACE=1 ghostflow-director -dd --config "$tmpdir/ghostflow-director.yaml" > "$tmpdir/ghostflow-director.log" 2>&1 &

readonly gitlab_log="$tmpdir/gitlab.log"
# Call the gitlab API.
#
# gitlab_api <path> <curl args>... > <gitlab output>
gitlab_api () {
    local path="$1"
    readonly path
    shift

    # If we're POSTing, clear the waited flag.
    [ -n "$*" ] && \
        __waited_for_gitlab=false

    echo "" >> "$gitlab_log"
    echo "----------------------------------------" >> "$gitlab_log"
    echo "$path" "$@" >> "$gitlab_log"
    loop_fetch \
        --header "PRIVATE-TOKEN: $private_token" \
        "$@" \
        "http://$gitlab_addr/api/v4$path" | \
        tee -a "$gitlab_log"
}

# Create a user on Gitlab.
#
# gitlab_add_user <username> > <user object>
gitlab_add_user () {
    local name="$1"
    readonly name
    shift

    gitlab_api "/users" \
        --data "email=$name@example.com" \
        --data "password=password" \
        --data "username=$name" \
        --data "name=$name" \
        --data "confirm=0"
}

# Create the users for the test.
#
# The users have the following powers:
#
#   maintainer: unused for now.
#   origin: user to hold all target repositories.
#   fork: a user with Developer access to the target repository.
#   other: a user with no special access to the target repository.
readonly users="maintainer origin fork other"

readonly root_id=1
for user in $users; do
    info "Adding the user $user..."

    gitlab_add_user "$user" \
        > "$tmpdir/user_$user"

    id="$( cat "$tmpdir/user_$user" | jq '.id' )"

    if [ -z "$id" ]; then
        die "failed to get the id of the user $user"
    fi

    eval "readonly ${user}_id=$id"
done

# Generate and upload an SSH key to gitlab.
#
# gitlab_add_ssh_key <username>
gitlab_add_ssh_key () {
    local user="$1"
    readonly user
    shift

    local id
    eval "id=\$${user}_id"
    readonly id

    # Create an SSH key for the user.
    info "Creating the SSH key for $user..."

    SSH_ASKPASS=false ssh-keygen -f "$tmpdir/ghostflow-director-test-$user" \
        > /dev/null \
        < /dev/null

    # Encode the SSH key.
    local pubkey="$( sed -e 's/+/%2B/g;s/ /+/g' \
        < "$tmpdir/ghostflow-director-test-$user.pub" )"
    readonly pubkey

    info "Setting the SSH key for $user..."

    gitlab_api "/users/$id/keys" \
        --data "title=$user" \
        --data "key=$pubkey" \
        > /dev/null
}

# Setup the SSH keys for the users to use.

# Add a key for the admin user.
gitlab_add_ssh_key "root"

for user in $users; do
    gitlab_add_ssh_key "$user"

    cat >> "$tmpdir/ssh_config" <<EOF
Host gitlab_$user
    User git
    HostName $gitlab_addr
    IdentityFile "$tmpdir/ghostflow-director-test-$user"
    IdentitiesOnly yes
EOF
done

# Lock down the SSH configuration file.
chmod 400 "$tmpdir/ssh_config"

# Setup the main repo.
info "Creating the origin/example project..."

origin_repo_id="$( gitlab_api "/projects" \
    --data "name=example" \
    --data "sudo=origin" \
    --data "visibility=public" | \
    jq '.id' )"
__waited_for_gitlab=false

# Add the fork user as a developer.
info "Adding the fork user as a developer to origin/example..."

gitlab_api "/projects/$origin_repo_id/members" \
    --data "user_id=$fork_id" \
    --data "access_level=30" \
    > /dev/null

# Initialize the local repo.
readonly repodir="$tmpdir/repo"
mkdir -p "$repodir"

# Branches for which we test actions.
readonly branches="master next pu"

info "Creating the test repository..."

touch "$tmpdir/git.log"
test_git () {
    git "$@" >> "$tmpdir/git.log" 2>&1
}

pushd "$repodir" > /dev/null
test_git init
test_git remote add origin "ssh://gitlab_origin/origin/example.git"
echo "first file" > "first"
test_git add "first"
test_git commit -m "first commit"

for branch in $branches; do
    [ "$branch" != "master" ] && \
        test_git branch "$branch" master

    test_git push origin "$branch"
done
popd > /dev/null

__waited_for_gitlab=false

# Setup the forks.
info "Forking the origin/example repository..."

# Create the forks on Gitlab.
fork_repo_id="$( gitlab_api "/projects/$origin_repo_id/fork" \
    --data "sudo=fork" | \
    jq '.id' )"
if [ -z "$fork_repo_id" ]; then
  die "Failed fork origin/example to 'fork' user."
fi
other_repo_id="$( gitlab_api "/projects/$origin_repo_id/fork" \
    --data "sudo=other" | \
    jq '.id' )"
if [ -z "$other_repo_id" ]; then
  die "Failed fork origin/example to 'other' user."
fi
__waited_for_gitlab=false

pushd "$repodir" > /dev/null
for branch in $branches; do
    info "Prepping the $branch branch..."

    test_git checkout -b "$branch-update" master
    echo "$branch-update" > "update"
    test_git add "update"
    test_git commit -m "$branch update"

    test_git checkout -b "$branch-conflict"
    echo "$branch-conflict" > "conflict"
    test_git add "conflict"
    test_git commit -m "$branch conflict"
done
popd > /dev/null

pushd "$repodir" > /dev/null
for remote in fork other; do
    info "Setting up the $remote fork..."

    test_git remote add "$remote" "ssh://gitlab_$remote/$remote/example.git"

    for branch in $branches; do
        info "Setting up the $remote/$branch branch..."

        test_git checkout -b "$remote-$branch-work" "$branch"

        # Create an update to the branch fails the checks.
        echo "trailing whitespace " > "$remote-$branch-bad-whitespace"
        test_git add "$remote-$branch-bad-whitespace"
        test_git commit -m "$remote-$branch bad-whitespace"
        test_git branch "$remote-$branch-bad-whitespace"
        test_git reset HEAD~

        # Create an update to the branch which is OK to merge.
        echo "initial" > "$remote-$branch-init"
        test_git add "$remote-$branch-init"
        test_git commit -m "$remote-$branch init"
        test_git branch "$remote-$branch-init"

        # Create an update to the branch which is OK to merge.
        echo "$remote update" > "$remote-update"
        test_git add "$remote-update"
        test_git commit -m "$remote update"
        test_git branch "$remote-$branch-update"

        echo "$remote conflict" > "$branch-branch-conflict"
        test_git add "$branch-branch-conflict"
        test_git commit -m "$branch conflict"
        test_git branch "$remote-$branch-branch-conflict"

        echo "$remote-$branch conflict" > "conflict"
        test_git add "conflict"
        test_git commit -m "$remote-branch conflict"
        test_git branch "$remote-$branch-conflict"

        test_git reset HEAD~~
        test_git branch "$remote-$branch-ok"
    done
done
popd > /dev/null

for user in origin; do
    info "Checking for webhooks on the $user/example project..."

    eval "id=\$${user}_repo_id"
    hook_url="$( gitlab_api "/projects/$id/hooks" | jq '.[0] | .url' )"

    check_string "$hook_url" "\"http://$HOSTNAME:$webhook_port/gitlab\"" || \
        die "The webhook for the $user/example project is not set; this" \
            "probably means that the container cannot contact your machine" \
            "(using $HOSTNAME)"
done

# Now use the real ghostflow-director.yaml.
info "Restarting ghostflow-director..."

readonly test_job_queuedir="$tmpdir/test/queue"
readonly test_job_archivedir="$tmpdir/test/archive"
mkdir -p "$test_job_archivedir"

sed -e "s,WORKDIR,$workdir," \
    -e "s,TEST_JOB_QUEUE,$test_job_queuedir," \
    -e "s,QUEUE,$queuedir," \
    -e "s,GITLAB_HOST,$gitlab_addr," \
    -e "s,SECRETS,$tmpdir/secrets.json," \
    -e "s,HOSTNAME,$HOSTNAME," \
    < "ghostflow-director.yaml.in" \
    > "$tmpdir/ghostflow-director.yaml"

RUST_BACKTRACE=1 ghostflow-director --config "$tmpdir/ghostflow-director.yaml" --reload

# Wait for `ghostflow-director` to get through all the job files present.
#
# This blocks until it appears that `ghostflow-director` has processed all of
# its data.
#
# Gitlab is intermittent when tossing out webhooks and timestamps show that our
# tools are behaving well and sidekiq is delaying things. Hopefully all of the
# nasty logic and arbitrary sleeping can be corralled into here.
#
# XXX(gitlab): It should be improved to look for contents in the job files. It
# can then look in the right subdirectory to see if it has already been
# processed or not.
__waited_for_gitlab=false
wait_for_empty_queue () {
    # If we have already waited for Gitlab before poking it again, skip waiting
    # for the queue.
    $__waited_for_gitlab && \
        return 0

    if ls "$queuedir/"*.json > /dev/null 2> /dev/null; then
        info -n "Job queue is not empty..."
    else
        info -n "Waiting for ghostflow-director to receive a job..."
        local waited
        # XXX(gitlab): Seems that there can be a delay in Gitlab getting
        # ghostflow-director the event. The logs point to sidekiq delaying the
        # webhook firing for up to 20 seconds in testing. No root cause found
        # yet; could be related to Gitlab being "cold" when initially started?
        local max_wait=30
        readonly max_wait
        for waited in $( seq 1 "$max_wait" ); do
            ls "$queuedir/"*.json > /dev/null 2> /dev/null && \
                break
            info -n "."
            sleep 1
        done

        if [ "$waited" = "$max_wait" ]; then
            warn -n "timed out waiting to see a job; assuming it appeared..."
        else
            info -n "seen..."
        fi
    fi
    info -n "waiting for a clean queue..."
    while ls "$queuedir/"*.json > /dev/null 2> /dev/null; do
        info -n "."
        # Wait for what ghostflow-director did to appear in gitlab's database.
        sleep 1
    done
    # ghostflow-director has been seen to take up to 4 seconds.
    sleep 4
    info "done."

    if ls "$queuedir/fail/"*.json > /dev/null 2> /dev/null; then
        die "There was a failure; please see the queue"
    fi

    __waited_for_gitlab=true
}

# Runs an expression up to 10 times with a delay between to deal with Gitlab
# delays.
#
# wait_for <expression arg>...
wait_for () {
    "$@" && \
        return 0

    info -n "Retrying..."

    local loops
    # Retry up to 10 times.
    for loop in $( seq 1 10 ); do
        info -n "$loop..."

        if "$@"; then
            info "success."
            return 0
        fi

        sleep "$loop"
    done

    error "failed."
    return 1
}

info "Ensuring the git repo is cloned..."

readonly example_gitdir="$workdir/gitlab/projects/origin/example.git"
wait_for [ -d "$example_gitdir" ]

pushd "$example_gitdir" > /dev/null
check_string "$( git config --get remote.origin.tagopt )" "--no-tags"
check_string "$( git config --get remote.origin.url )" "git@$docker_name:origin/example.git"
if git config --get remote.origin.fetch; then
    die "the work repository should not have fetch refspecs set"
fi
check_string "$( readlink "modules/relative" )" "$workdir/gitlab/projects/origin/submodule.git"
check_string "$( readlink "modules/absolute" )" "$workdir"
popd > /dev/null

# Create an issue on Gitlab.
#
# gitlab_create_issue <username> \
#     <source repo id> <title> \
#     > <issue id>
gitlab_create_issue () {
    local user="$1"
    readonly user
    shift

    local repo="$1"
    readonly repo
    shift

    local title="$1"
    readonly title
    shift

    gitlab_api "/projects/$repo/issues" \
        --data "title=$title" \
        --data "sudo=$user"
}

# Create a merge request on Gitlab.
#
# gitlab_create_mr <username> \
#     <source repo id> <source branch> \
#     <target repo id> <target branch> \
#     <remove topic> <description> \
#     > <merge request id>
gitlab_create_mr () {
    local user="$1"
    readonly user
    shift

    local source_repo="$1"
    readonly source_repo
    shift

    local source_branch="$1"
    readonly source_branch
    shift

    local target_repo="$1"
    readonly target_repo
    shift

    local target_branch="$1"
    readonly target_branch
    shift

    local remove_topic="$1"
    readonly remove_topic
    shift

    local description="$1"
    readonly description
    shift

    local remove_source_branch
    if $remove_topic; then
        remove_source_branch='--data remove_source_branch=1'
    else
        # XXX(gitlab): It seems that passing `remove_source_branch=0` causes a
        # backtrace on the server.
        remove_source_branch=
    fi
    readonly remove_source_branch

    gitlab_api "/projects/$source_repo/merge_requests" \
        --data "source_branch=$source_branch" \
        --data "target_branch=$target_branch" \
        --data "target_project_id=$target_repo" \
        --data "title=merge request for $source_branch" \
        $remove_source_branch \
        --data "description=$description" \
        --data "sudo=$user" | \
        jq '.id'
}

# Check for an award on the created comment.
#
# check_for_award <project id> <merge request id> <note id>
check_for_award () {
    local project_id="$1"
    readonly project_id
    shift

    local mr_id="$1"
    readonly mr_id
    shift

    local note_id="$1"
    readonly note_id
    shift

    info "Making sure that ghostflow-director has marked the comment..."

    local award_name="$( gitlab_api "/projects/$project_id/merge_requests/$mr_id/notes/$note_id/award_emoji" | \
        jq '.[0].name' )"
    readonly award_name

    check_string "$award_name" "\"robot\""
}

# Add a comment to a merge request.
#
# gitlab_mr_comment_ignored <project id> <merge request id> <username> <body>
gitlab_mr_comment_ignored () {
    local project_id="$1"
    readonly project_id
    shift

    local mr_id="$1"
    readonly mr_id
    shift

    local user="$1"
    readonly user
    shift

    local reason="$1"
    readonly reason
    shift

    local body="$1"
    readonly body
    shift

    local content="$reason"$'\n\n'"$body"
    readonly content

    info "$reason"

    local note_id="$( gitlab_api "/projects/$project_id/merge_requests/$mr_id/notes" \
        --data "sudo=$user" \
        --data "body=$content" | \
        jq '.id' )"
    readonly note_id
    __waited_for_gitlab=false

    wait_for_empty_queue

    local awards="$( gitlab_api "/projects/$project_id/merge_requests/$mr_id/notes/$note_id/award_emoji" )"
    readonly awards

    check_string "$awards" "[]"
}

# Add a comment to a merge request.
#
# gitlab_mr_comment <project id> <merge request id> <username> <body>
gitlab_mr_comment () {
    local project_id="$1"
    readonly project_id
    shift

    local mr_id="$1"
    readonly mr_id
    shift

    local user="$1"
    readonly user
    shift

    local reason="$1"
    readonly reason
    shift

    local body="$1"
    readonly body
    shift

    local content="$reason"$'\n\n'"$body"
    readonly content

    info "$reason"

    local note_id="$( gitlab_api "/projects/$project_id/merge_requests/$mr_id/notes" \
        --data "sudo=$user" \
        --data "body=$content" | \
        jq '.id' )"
    readonly note_id
    __waited_for_gitlab=false

    wait_for_empty_queue

    wait_for check_for_award "$project_id" "$mr_id" "$note_id"
}

check_stage_branch () {
    local branch="$1"
    readonly branch
    shift

    local expected="$1"
    readonly expected
    shift

    local matches_latest="$1"
    readonly matches_latest
    shift

    local branch_to_check="$1"
    readonly branch_to_check
    shift

    info "Fetching the HEAD of the stage for $branch..."

    pushd "$repodir" > /dev/null
    # Fetch the current stage.
    test_git fetch origin "+refs/stage/$branch/head:refs/heads/stage"
    # List the branches merged into the stage.
    local merged="$( git log --first-parent --merges --oneline stage | \
        sed -n -e "s/^.*Stage topic '\(.*\)-for-$branch'$/\1/p" | \
        tr $'\n' " " | \
        sed -e 's/ $//' )"
    readonly merged

    info "Checking the set of branches on stage for $branch..."

    local expected_state
    if ! $matches_latest || [ -z "$branch_to_check" ]; then
        expected_state="$expected"
    elif [ -z "$expected" ]; then
        expected_state="$branch_to_check"
    else
        expected_state="$branch_to_check $expected"
    fi
    readonly expected_state

    # Check that the state of the stage is as expected. The space is intended;
    # the `tr` command above transforms the trailing newline into it.
    check_string "$merged" "$expected_state" || \
        return 1

    info "Checking the refs of the branches on the stage for $branch..."

    local stage_ref="stage"
    local remote
    local merged_ref
    local remote_ref
    # Check the state of the branches merged into the stage.
    for remote in $merged; do
        merged_ref="$( git rev-parse "$stage_ref^2" )"
        remote_ref="$( git rev-parse "refs/remotes/$remote/$remote-for-$branch" )"

        # Ensure the latest branch is on the stage unless the branch is
        # known to not be up-to-date.
        if $matches_latest || [ "$branch_to_check" != "$remote" ]; then
            check_string "$merged_ref" "$remote_ref" || \
                return 1
        fi

        # Go back one parent on the stage.
        stage_ref="$stage_ref~"
    done
    popd > /dev/null
}

# Check the state of the stage branch.
#
# The `<expected state>` argument is a list of remotes to ensure are present on
# the stage, from oldest to newest. It should *not* include the branch which
# was just operated on because its presence should is based on the `<matches
# latest>` parameter.
#
# The `<matches latest>` argument is an expression whether the latest branch is
# meant to be present or not. If it is, it is expected to be the last branch
# merged.
#
# check_stage <branch> <expected state> <matches latest> <branch to check>
check_stage () {
    local branch="$1"
    readonly branch
    shift

    info "Checking the state of the stage for $branch..."
    wait_for_empty_queue

    wait_for check_stage_branch "$branch" "$@"
}

# Checks the state of a merge request after it has been updated
#
# Depending on a stage policy, a branch can be in any number of states after an
# update has been pushed to the source repository. This function checks the
# stage for the current state of the staging branch based upon the policy of
# the branch and whether it should have been integrated successfully or not.
#
# post_mr_update_check <branch> <policy> <state> <exists> <fork name> \
#     <merge request id>
post_mr_update_check () {
    local branch="$1"
    readonly branch
    shift

    local policy="$1"
    readonly policy
    shift

    local state="$1"
    readonly state
    shift

    local exists="$1"
    readonly exists
    shift

    local mr="$1"
    readonly mr
    shift

    local id="$1"
    readonly id
    shift

    local expected
    local matches_latest
    local need_stage
    local need_unstage
    case "$policy-$state" in
        *-new)
            # If the branch is new to the stage, it should not be on the stage,
            # we don't care about matching the latest branch heads, and we need
            # to stage the branch afterwards.
            expected="$exists"
            matches_latest=false
            need_stage=true
            need_unstage=false
            ;;
        ignore-conflict)
            # The branch will still be on the stage in its old position, but we
            # need to remove it from the stage so that it matches the other
            # policies which automatically remove it.
            if [ -z "$exists" ]; then
                expected="$mr"
            else
                expected="$exists $mr"
            fi
            matches_latest=false
            need_stage=false
            need_unstage=true
            ;;
        unstage-conflict)
            # If the policy is to unstage, it should not be on the stage, we
            # don't care about matching the latest branch heads, and there is
            # no need to stage the branch afterwards.
            expected="$exists"
            matches_latest=false
            need_stage=false
            need_unstage=false

            # Make sure the unstaging happened.
            check_status "$mr" "$branch" "ghostflow-stager" "success" "unstaged"
            ;;
        *-conflict)
            # If the branch should have been unstaged due to conflicts, it
            # should not be on the stage, we don't care about matching the
            # latest branch, and staging won't work anyways.
            expected="$exists"
            matches_latest=false
            need_stage=false
            need_unstage=false

            # Make sure it conflicted on the stage.
            check_status "$mr" "$branch" "ghostflow-stager" "failed" "failed to merge: 1 conflicting paths"
            ;;
        *-unstage)
            # If the branch should have been unstaged, it should not be on the
            # stage, we don't care about matching the latest branch, and we
            # should not put it back on the stage.
            expected="$exists"
            matches_latest=false
            need_stage=false
            need_unstage=false

            # Make sure the unstaging happened.
            check_status "$mr" "$branch" "ghostflow-stager" "success" "unstaged"
            ;;
        unstage-*)
            # If the policy is to unstage, it should not be on the stage, we
            # don't care about matching the latest branch heads, and we need to
            # stage the branch afterwards.
            expected="$exists"
            matches_latest=false
            need_stage=true
            need_unstage=false

            check_mr_comment "$quiet" "$id" 'This merge request has been unstaged as per policy.'
            ;;
        ignore-*)
            # If the policy is to ignore updates, it should be at the back of
            # the stage, we don't care about matching the latest branch heads,
            # and we need to stage the branch afterwards.
            if [ -z "$exists" ]; then
                expected="$mr"
            else
                expected="$exists $mr"
            fi
            matches_latest=false
            need_stage=true
            need_unstage=false
            ;;
        restage-*)
            # If the policy is to restage updates, it should be at the front of
            # the stage (check_stage will do that with matches_latest=true), we
            # do care about matching the latest branch heads, and we don't need
            # to stage the branch afterwards.
            expected="$exists"
            matches_latest=true
            need_stage=false
            need_unstage=false

            check_mr_comment "$quiet" "$id" 'Successfully staged.'
            check_status "$mr" "$branch" "ghostflow-stager" "success" "staged"
            ;;
        *)
            die "unhandled policy/state pair: policy: $policy, state: $state"
            ;;
    esac
    readonly expected
    readonly matches_latest
    readonly need_stage
    readonly need_unstage

    if $need_stage && $need_unstage; then
        die "a stage branch cannot need to be staged and unstaged at the same time"
    fi

    check_stage "$branch" "$expected" "$matches_latest" "$mr"

    # Restage if necessary.
    if $need_stage; then
        gitlab_mr_comment "$origin_repo_id" "$id" "origin" "Fixing up the stage by staging so that the state is consistent with all policies..." "Do: stage"
        check_stage "$branch" "$exists" "true" "$mr"
        check_mr_comment "$quiet" "$id" 'Successfully staged.'
        check_status "$mr" "$branch" "ghostflow-stager" "success" "staged"
    fi

    # Unstage if necessary.
    if $need_unstage; then
        gitlab_mr_comment "$origin_repo_id" "$id" "origin" "Fixing up the stage by unstaging so that the state is consistent with all policies..." "Do: unstage"
        check_stage "$branch" "$exists" "false" "$mr"
        check_mr_comment "$quiet" "$id" 'This merge request has been unstaged upon request.'
        check_status "$mr" "$branch" "ghostflow-stager" "success" "unstaged"
    fi
}

# Check the state of the stage after the base branch is updated.
#
# See check_stage for the format of the `<state>` parameter.
#
# post_branch_update_check <branch> <state>
post_branch_update_check () {
    local branch="$1"
    readonly branch
    shift

    local state="$1"
    readonly state
    shift

    check_stage "$branch" "$state" "true" ""
}

# Push a branch to a remote.
#
# Branches are named `<remote>-<branch>-<step>` locally and pushed to
# `<remote>-for-<branch>` on the remote. This allows the local repository to
# hold multiple states of a branch simultaneously.
#
# push_branch <remote> <branch> <step>
push_branch () {
    local remote="$1"
    readonly remote
    shift

    local branch="$1"
    readonly branch
    shift

    local step="$1"
    readonly step
    shift

    info "Pushing $branch-$step to $remote..."

    __waited_for_gitlab=false

    test_git -C "$repodir" push --force "$remote" "$remote-$branch-$step:$remote-for-$branch"
}

last_mr_comment () {
    local project="$1"
    readonly project
    shift

    local mr="$1"
    readonly mr
    shift

    local expected="$1"
    readonly expected
    shift

    local comment="$( gitlab_api "/projects/$project/merge_requests/$mr/notes?per_page=100" | \
        jq 'sort_by(.id) | .[-1].body' )"
    readonly comment

    check_string "$comment" "\"$expected\""
}

# Check the latest comment on a merge request.
#
# The `<quiet>` parameter is an expression which if evaluates to `true` implies
# that the comment should not appear if the policy is for `ghostflow-director`
# to be silent about that response.
#
# check_mr_comment <quiet> <merge request id> <expected content>
check_mr_comment () {
    local quiet="$1"
    readonly quiet
    shift

    local id="$1"
    readonly id
    shift

    local expected="$1"
    readonly expected
    shift

    # Ignore the check if the policy is to be quiet.
    $quiet && \
        return 0

    info "Fetching the latest comment from MR $id..."
    wait_for_empty_queue

    wait_for last_mr_comment "$origin_repo_id" "$id" "$expected"
}

last_mr_status () {
    local project="$1"
    readonly project
    shift

    local sha="$1"
    readonly sha
    shift

    local name="$1"
    readonly name
    shift

    local status="$1"
    readonly status
    shift

    local description="$1"
    readonly description
    shift

    local actual="$( gitlab_api "/projects/$id/repository/commits/$sha/statuses?name=$name" | \
        jq -c '.[0] | {"status": .status, "description": .description}' )"
    readonly actual

    check_string "$actual" "{\"status\":\"$status\",\"description\":\"$description\"}"
}

# Checks the commit status for a commit.
#
# Ensures that the latest status on a commit matches some state.
#
# check_status <remote> <branch> <name> <status> <descripiton>
check_status () {
    local remote="$1"
    readonly remote
    shift

    local branch="$1"
    readonly branch
    shift

    local name="$1"
    readonly name
    shift

    local status="$1"
    readonly status
    shift

    local description="$1"
    readonly description
    shift

    local sha="$( git -C "$repodir" rev-parse "refs/remotes/$remote/$remote-for-$branch" )"
    readonly sha

    info "Checking the $name status of the MR from $remote for $branch..."
    wait_for_empty_queue

    local id
    eval "id=\$${remote}_repo_id"
    readonly id

    last_mr_status "$id" "$sha" "$name" "$status" "$description"
}

# Check that a test job was dropped.
check_for_test_job () {
    local backend="$1"
    readonly backend
    shift

    local kind="$1"
    readonly kind
    shift

    # Bail out if we're not using the jobs test backend.
    [ "$backend" = "jobs" ] || \
        return 0

    wait_for_empty_queue

    info "Checking that a test job appeared..."
    ls "$test_job_queuedir/"*.json > /dev/null 2> /dev/null || \
        die "Did not see a test job file appear."

    local job
    for job in "$test_job_queuedir/"*.json; do
        readonly job

        local actual_kind="$( jq '.kind' < "$job" )"
        readonly actual_kind

        case "$kind" in
            any|all)
                case "$actual_kind" in
                    '"mr_update"'|'"test_action"'|'"branch_push"')
                        info "Found a $actual_kind job in $job"
                        ;;
                    *)
                        die "unknown test job kind: $actual_kind"
                        ;;
                esac
                ;;
            *)
                check_string "$actual_kind" "\"$kind\""
                ;;
        esac

        mv "$job" "$test_job_archivedir/"

        [ "$kind" = "all" ] || \
            break
    done

    if ls "$queuedir/fail/"*.json > /dev/null 2> /dev/null; then
        warn "There are test jobs still in the queue"
    else
        info "The test job queue is empty"
    fi
}

# Check that a test ref was created.
check_for_test_ref () {
    local expect="$1"
    readonly expect
    shift

    local backend="$1"
    readonly backend
    shift

    local id="$1"
    readonly id
    shift

    # Bail out if we're not using the refs test backend.
    [ "$backend" = "refs" ] || \
        return 0

    wait_for_empty_queue

    info "Checking that a test ref appeared..."
    pushd "$repodir" > /dev/null
    if $expect; then
        if ! test_git ls-remote --exit-code origin "refs/test-topics/$id"; then
            die "expected refs/test-topics/$id on the remote"
        fi
    else
        if test_git ls-remote --exit-code origin "refs/test-topics/$id"; then
            die "unexpected refs/test-topics/$id on the remote"
        fi
    fi
    popd > /dev/null
}

# Check the commit and branch statuses for the given merge request.
#
# check_statuses <remote> <branch>
check_statuses () {
    local remote="$1"
    readonly remote
    shift

    local branch="$1"
    readonly remote
    shift

    local sha="$( git -C "$repodir" rev-parse "refs/remotes/$remote/$remote-for-$branch" )"
    readonly sha

    local description="overall branch status for the content checks against $branch\n\nBranch-at: $sha"
    readonly description

    check_status "$remote" "$branch" "ghostflow-commit-check" "success" "basic content checks"
    check_status "$remote" "$branch" "ghostflow-branch-check-$branch" "success" "$description"
}

tag_stage () {
    local branch="$1"
    readonly branch
    shift

    local policy="$1"
    readonly policy
    shift

    local mr_id="$1"
    readonly mr_id
    shift

    local reason="nightly"
    readonly reason

    local ref_date_format="static"
    readonly ref_date_format

    wait_for_empty_queue

    # Pick up a ref to the stage before tagging.
    pushd "$repodir" > /dev/null
    test_git fetch origin \
        "+refs/stage/$branch/head:refs/heads/stage-pre"
    popd > /dev/null

    sed -e "s,BRANCH,$branch," \
        -e "s,REF_DATE_FORMAT,$ref_date_format," \
        -e "s,REASON,$reason," \
        -e "s,POLICY,$policy," \
        < "tag-stage.json.in" \
        > "$queuedir/tag-$branch.json"
    __waited_for_gitlab=false

    local comment_content
    local expected_stage
    case "$policy" in
        keep_topics)
            expected_stage="other"
            ;;
        clear_stage)
            comment_content="This merge request has been pushed for nightly testing as of static and unstaged."
            expected_stage=""
            ;;
        *)
            die "unknown tag policy: $policy"
            ;;
    esac
    readonly comment_content
    readonly expected_stage

    if [ -n "$comment_content" ]; then
        check_mr_comment "false" "$mr_id" "$comment_content"
    fi
    check_status "other" "$branch" "ghostflow-stager" "success" "staged for $reason testing refs/stage/$branch/$reason/$ref_date_format"
    check_stage "$branch" "$expected_stage" "true" ""

    info "Checking the state of the stage and the created tag..."

    pushd "$repodir" > /dev/null
    test_git fetch origin \
        "+refs/heads/$branch:refs/heads/remote-$branch" \
        "+refs/stage/$branch/head:refs/heads/stage" \
        "+refs/stage/$branch/$reason/latest:refs/heads/stage-$reason/latest" \
        "+refs/stage/$branch/$reason/$ref_date_format:refs/heads/stage-$reason/date"

    local branch_ref="$( git rev-parse "refs/heads/remote-$branch" )"
    readonly branch_ref
    local pre_tag_ref="$( git rev-parse "refs/heads/stage-pre" )"
    readonly pre_tag_ref
    local current_ref="$( git rev-parse "refs/heads/stage" )"
    readonly current_ref
    local latest_ref="$( git rev-parse "refs/heads/stage-$reason/latest" )"
    readonly latest_ref
    local date_ref="$( git rev-parse "refs/heads/stage-$reason/date" )"
    readonly date_ref
    popd > /dev/null

    case "$policy" in
        keep_topics)
            # Keeping the topics should keep the stage where it was and the
            # tagged stage refs should match it.
            check_string "$pre_tag_ref" "$current_ref"
            check_string "$current_ref" "$latest_ref"
            check_string "$current_ref" "$date_ref"
            ;;
        clear_stage)
            # Clearing the stage should make the stage the same as the target
            # branch and the tagged stage refs should match the pre-tag stage
            # ref.
            check_string "$branch_ref" "$current_ref"
            check_string "$pre_tag_ref" "$latest_ref"
            check_string "$pre_tag_ref" "$date_ref"
            ;;
    esac
}

# Check the an issue is closed and has a set of labels.
#
# check_issue <project id> <issue id> <labels>
check_issue () {
    local repo_id="$1"
    readonly repo_id
    shift

    local issue_id="$1"
    readonly issue_id
    shift

    local labels="$1"
    readonly labels
    shift

    local mr_issue="$( gitlab_api "/projects/$repo_id/issues/$issue_id" )"
    readonly mr_issue

    local issue_state="$( echo "$mr_issue" | jq '.state' )"
    readonly issue_state
    local issue_labels="$( echo "$mr_issue" | jq '.labels | join(",")' )"
    readonly issue_labels

    info "Checking the status of associated issues..."
    check_string "$issue_state" "\"closed\""
    info "Checking the labels of associated issues..."
    check_string "$issue_labels" "\"$labels\""
}

info "Checking handling of data refs..."

readonly datafile="$tmpdir/datafile"
pwgen 40 1 > "$datafile"
readonly data_md5="$( md5sum "$datafile" | cut -f1 "-d " )"
readonly data_sha256="$( sha256sum "$datafile" | cut -f1 "-d " )"
readonly data_sha512="$( sha512sum "$datafile" | cut -f1 "-d " )"
pushd "$repodir" > /dev/null
readonly data_id="$( git hash-object -w -t blob --stdin < "$datafile" )"

info "Creating data refs..."
test_git update-ref "refs/data/MD5/$data_md5" "$data_id"
test_git update-ref "refs/data/SHA256/$data_sha256" "$data_id"
test_git update-ref "refs/data/SHA512/$data_sha512" "$data_id"
test_git update-ref "refs/data/nosuchhash/notahash" "$data_id"
test_git update-ref "refs/data/MD5/notahash" "$data_id"
test_git update-ref "refs/data/SHA256/notahash" "$data_id"
test_git update-ref "refs/data/SHA512/notahash" "$data_id"

info "Pushing data refs..."
test_git push fork "refs/data/*:refs/data/*" "+master:__data-trigger"
test_git push fork ":__data-trigger"
__waited_for_gitlab=false

wait_for_empty_queue

if test_git ls-remote --exit-code fork "refs/data/*"; then
    die "found remote data references in the repository"
fi
popd > /dev/null

[ -f "$workdir/data/MD5/$data_md5" ] || \
    die "MD5 data was not synced"
[ -f "$workdir/data/SHA256/$data_sha256" ] || \
    die "SHA256 data was not synced"
[ -f "$workdir/data/SHA512/$data_sha512" ] || \
    die "SHA512 data was not synced"

[ -f "$workdir/data/MD5/notahash" ] && \
    die "invalid MD5 was synced"
[ -f "$workdir/data/SHA256/notahash" ] && \
    die "invalid SHA256 was synced"
[ -f "$workdir/data/SHA512/notahash" ] && \
    die "invalid SHA512 was synced"

# Perform tests for each branch (which have different stage policies).
for branch in $branches; do
    # Push the initial state for the branch to the remote.
    push_branch "fork" "$branch" "init"
    push_branch "other" "$branch" "init"

    # Set local variables based on the policies we expect to see.
    case "$branch" in
        master)
            policy="unstage"
            need_master=true
            quiet=true
            tag_policy="clear_stage"
            remove_topic=true
            limit_log=true
            test_backend="refs"
            labels="closed-by-ghostflow"
            ;;
        next)
            policy="ignore"
            need_master=false
            quiet=false
            tag_policy="keep_topics"
            remove_topic=false
            limit_log=false
            test_backend="jobs"
            labels="closed-by-ghostflow"
            ;;
        pu)
            policy="restage"
            need_master=false
            quiet=false
            tag_policy="keep_topics"
            remove_topic=false
            limit_log=false
            test_backend="jobs"
            labels=""
            ;;
        *)
            die "unhandled branch: $branch"
            ;;
    esac

    info "========================================================================"
    info "*** Working on the $branch branch"

    info "------------------------------------------------------------------------"
    info "Creating issue to close from an MR"
    info "------------------------------------------------------------------------"

    issue="$( gitlab_create_issue "origin" "$origin_repo_id" "Example issue for $branch" )"
    __waited_for_gitlab=false
    issue_id="$( echo "$issue" | jq '.id' )"
    issue_iid="$( echo "$issue" | jq '.iid' )"

    info "------------------------------------------------------------------------"
    info "Creating merge requests"
    info "------------------------------------------------------------------------"

    info "Creating merge requests into $branch..."
    fork_mr_id="$( gitlab_create_mr "fork" "$fork_repo_id" "fork-for-$branch" "$origin_repo_id" "$branch" "$remove_topic" "Fixes #$issue_iid." )"
    __waited_for_gitlab=false
    check_for_test_job "$test_backend" "mr_update"
    other_mr_id="$( gitlab_create_mr "other" "$other_repo_id" "other-for-$branch" "$origin_repo_id" "$branch" "$remove_topic" "" )"
    __waited_for_gitlab=false
    check_for_test_job "$test_backend" "mr_update"

    check_statuses "fork" "$branch"
    check_statuses "other" "$branch"

    info "------------------------------------------------------------------------"
    info "Making sure that help works"
    info "------------------------------------------------------------------------"

    # `Do: help` should comment again, even with insufficient permissions.
    check_help='The `check` action performs checks on the content and structure of the merge request according to the project'"'"'s settings. Generally, all messages should be addressed. Those which mention a specific commit require that the commit itself be fixed. That is, an additional commit which fixes the issue will not address the problem with the original commit.'
    case "$policy" in
        unstage)
            stage_policy_help='removed from the stage'
            ;;
        ignore)
            stage_policy_help='left as-is on the stage'
            ;;
        restage)
            stage_policy_help='automatically restaged'
            ;;
    esac
    stage_help='The `stage` action takes the topic from the merge request and adds it to the \"stage\". This is a branch which contains all of the staged topics merged in on top of the target branch. Conflicts between topics on the stage are handled in a \"first-come, first-served\" fashion, so if a topic conflicts with another, waiting until it is either merged into the target branch or removed from the stage is best. When updating a merge request with new code, a topic that has already been staged is '"$stage_policy_help"'.'
    unstage_help='The `unstage` action removes the topic from the stage.'
    case "$test_backend" in
        refs)
            test_help='The `test` action creates a special reference on the repository which indicates that the merge request should be tested. Test machines look at these special references and run the tests. The `--stop` argument may be given to remove the special reference. The reference is also removed when the merge request is closed (for any reason). Updates to the merge request leave any existing test reference in place and require either a new `Do: test` to update it to the new code or a `Do: test --stop` to remove it.'
            ;;
        jobs)
            test_help='The `test` action drops a test job file for processing by the testing infrastructure.'
            ;;
    esac
    merge_help='The `merge` action merges the merge request into the target branch. It fails if the merge request has not passed the checks or it is considered a work-in-progress. The name of the topic merged into the target branch may be changed with either the `--topic` argument or using a `Topic-rename` trailer in the description.'
    reformat_help='The `reformat` action rewrites the topic for the merge request according to the project coding guidelines. Every commit will have its content changed so that it follows the coding guidelines. Any commit which is empty after the reformatting will be dropped from the topic. After performing a reformat, the resulting topic is force-pushed as the source topic of the merge request. In order to use the reformatted branch, it must be fetched and updated locally, otherwise the reformatting will need to be redone.'

    help_content="Messages:\n\n  - $check_help"
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "other" "Making sure that the \`help\` command only reports what is allowed..." "Do: help"
    check_mr_comment "false" "$fork_mr_id" "$help_content"

    help_content="Messages:\n\n  - $check_help\n  - $unstage_help\n  - $reformat_help"
    gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "other" "Making sure that the \`help\` command only reports what is allowed for submitters..." "Do: help"
    check_mr_comment "false" "$other_mr_id" "$help_content"

    help_content="Messages:\n\n  - $check_help\n  - $stage_help\n  - $unstage_help\n  - $test_help\n  - $merge_help\n  - $reformat_help"
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Making sure that the \`help\` command reports all allowed commands..." "Do: help"
    check_mr_comment "false" "$fork_mr_id" "$help_content"

    info "------------------------------------------------------------------------"
    info "Making sure that checks work"
    info "------------------------------------------------------------------------"

    push_branch "fork" "$branch" "bad-whitespace"
    check_for_test_job "$test_backend" "mr_update"

    fork_mr_head="$( git -C "$repodir" rev-parse "refs/remotes/fork/fork-for-$branch" )"
    check_fail_comment="Errors:\n\n  - commit $fork_mr_head adds bad whitespace:\n\n        fork-$branch-bad-whitespace:1: trailing whitespace.\n        +trailing whitespace \n\n\nPlease rewrite commits to fix the errors listed above (adding fixup commits will not resolve the errors) and force-push the branch again to update the merge request."

    sha="$( git -C "$repodir" rev-parse "refs/remotes/fork/fork-for-$branch" )"

    check_status "fork" "$branch" "ghostflow-commit-check" "failed" "basic content checks"
    check_status "fork" "$branch" "ghostflow-branch-check-$branch" "failed" "overall branch status for the content checks against $branch\n\nBranch-at: $sha"
    check_mr_comment "false" "$fork_mr_id" "$check_fail_comment"

    # `Do: check` should comment again, even with insufficient permissions.
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "other" "Making sure that the \`check\` command is always allowed..." "Do: check"
    check_mr_comment "false" "$fork_mr_id" "$check_fail_comment"

    # `Do: stage` should fail due to a missing check.
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Making sure that staging fails when checks are failing..." "Do: stage"
    check_mr_comment "false" "$fork_mr_id" "Errors:\n\n  - Refusing to stage; topic is failing the checks."

    # `Do: merge` should fail due to a missing check.
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Making sure that merging fails when checks are failing..." "Do: merge"
    check_mr_comment "false" "$fork_mr_id" "Errors:\n\n  - Refusing to merge; topic is failing the checks."

    # Faked MR checks are not sufficient.
    gitlab_api "/projects/$fork_repo_id/statuses/$fork_mr_head" \
        --data "state=success" \
        --data "name=ghostflow-branch-check-$branch" \
        --data "description=fake check" \
        --data "sudo=fork" \
        > /dev/null
    check_status "fork" "$branch" "ghostflow-branch-check-$branch" "success" "fake check"

    # `Do: stage` should fail due to a missing check.
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Making sure that staging fails when checks are failing..." "Do: stage"
    check_mr_comment "false" "$fork_mr_id" "Errors:\n\n  - Refusing to stage; topic is missing the checks."

    # `Do: merge` should fail due to a missing check.
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Making sure that merging fails when checks are failing..." "Do: merge"
    check_mr_comment "false" "$fork_mr_id" "Errors:\n\n  - Refusing to merge; topic is missing the checks."

    push_branch "fork" "$branch" "init"
    check_statuses "fork" "$branch"
    check_for_test_job "$test_backend" "mr_update"

    info "------------------------------------------------------------------------"
    info "Ensuring permission checks work"
    info "------------------------------------------------------------------------"

    # Should fail; insufficient permissions.
    gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "other" "Staging into $branch from other with no permissions..." "Do: stage"
    check_mr_comment "false" "$other_mr_id" 'Errors:\n\n  - @other: insufficient privileges for the commands for `'"$branch"'`: `stage`.'

    gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "other" "Merging into $branch from other with no permissions..." "Do: merge"
    check_mr_comment "false" "$other_mr_id" 'Errors:\n\n  - @other: insufficient privileges for the commands for `'"$branch"'`: `merge`.'

    gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "other" "Testing against $branch from other with no permissions..." "Do: test"
    check_mr_comment "false" "$other_mr_id" 'Errors:\n\n  - @other: insufficient privileges for the commands for `'"$branch"'`: `test`.'

    # Stage the branches (should fail if master permissions are necessary).
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "fork" "Staging into $branch from fork with Developer permissions..." "Do: stage"
    wait_for_empty_queue
    gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "fork" "Staging into $branch from other with Developer permissions..." "Do: stage"

    if $need_master; then
        check_mr_comment "false" "$fork_mr_id" 'Errors:\n\n  - @fork: insufficient privileges for the commands for `'"$branch"'`: `stage`.'
        check_mr_comment "false" "$other_mr_id" 'Errors:\n\n  - @fork: insufficient privileges for the commands for `'"$branch"'`: `stage`.'

        gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "fork" "Merging into $branch with insufficient permissions..." "Do: merge"
        check_mr_comment "false" "$fork_mr_id" 'Errors:\n\n  - @fork: insufficient privileges for the commands for `'"$branch"'`: `merge`.'

        gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "fork" "Testing against $branch with insufficient permissions..." "Do: test"
        check_mr_comment "false" "$fork_mr_id" 'Errors:\n\n  - @fork: insufficient privileges for the commands for `'"$branch"'`: `test`.'

        gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Fixing up staging fork into $branch due to insufficient permissions..." "Do: stage"
        wait_for_empty_queue
        gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "origin" "Fixing up staging other into $branch due to insufficient permissions..." "Do: stage"
    fi

    check_status "fork" "$branch" "ghostflow-stager" "success" "staged"
    check_status "other" "$branch" "ghostflow-stager" "success" "staged"

    # Ensure the stage is as we expect.
    check_stage "$branch" "other fork" "true" ""

    info "------------------------------------------------------------------------"
    info "Checking attempts to restage a topic"
    info "------------------------------------------------------------------------"

    gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "origin" "Testing that staging other into $branch again is detected..." "Do: stage"
    check_mr_comment "$quiet" "$other_mr_id" 'This topic has already been staged; ignoring the request to stage.'

    info "------------------------------------------------------------------------"
    info "Ensuring permission checks work for unstaging"
    info "------------------------------------------------------------------------"

    # Should fail; insufficient permissions.
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "other" "Unstaging other from $branch with insufficient permissions..." "Do: unstage"
    check_mr_comment "false" "$fork_mr_id" 'Errors:\n\n  - @other: insufficient privileges for the commands for `'"$branch"'`: `unstage`.'

    if $need_master; then
        # Should fail; insufficient permissions.
        gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "fork" "Unstaging other from $branch with Developer permissions..." "Do: unstage"
        check_mr_comment "false" "$other_mr_id" 'Errors:\n\n  - @fork: insufficient privileges for the commands for `'"$branch"'`: `unstage`.'
    fi

    info "------------------------------------------------------------------------"
    info "Ensuring that testing works"
    info "------------------------------------------------------------------------"

    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Testing against $branch..." "Do: test"

    case "$test_backend" in
        jobs)
            check_mr_comment "$quiet" "$fork_mr_id" 'This topic has been queued for testing.'
            check_for_test_job "$test_backend" "test_action"
            ;;
        refs)
            check_status "fork" "$branch" "ghostflow-test" "success" "pushed for testing"
            check_for_test_ref "true" "$test_backend" "$fork_mr_id"

            gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Stopping testing against $branch..." "Do: test --stop"
            check_status "fork" "$branch" "ghostflow-test" "success" "removed from testing"

            check_for_test_ref "false" "$test_backend" "$fork_mr_id"
            ;;
        *)
            die "unhandled test backend: $test_backend"
            ;;
    esac

    info "------------------------------------------------------------------------"
    info "Checking that updating topics works"
    info "------------------------------------------------------------------------"

    # Update the first branch.
    info "Pushing an update for $branch to fork..."
    push_branch "fork" "$branch" "update"
    post_mr_update_check "$branch" "$policy" "update" "other" "fork" "$fork_mr_id"
    check_mr_comment "$quiet" "$fork_mr_id" 'Successfully staged.'
    check_statuses "fork" "$branch"
    check_for_test_job "$test_backend" "mr_update"

    # Update the second branch.
    info "Pushing a branch-conflict for $branch to other..."
    push_branch "other" "$branch" "branch-conflict"
    post_mr_update_check "$branch" "$policy" "update" "fork" "other" "$other_mr_id"
    check_mr_comment "$quiet" "$other_mr_id" 'Successfully staged.'
    check_statuses "other" "$branch"
    check_for_test_job "$test_backend" "mr_update"

    info "------------------------------------------------------------------------"
    info "Checking that updating topics with merge conflicts works"
    info "------------------------------------------------------------------------"

    # Update the first branch again (conflicts with the other branch).
    info "Pushing a branch-conflict for $branch to fork..."
    push_branch "fork" "$branch" "branch-conflict"
    post_mr_update_check "$branch" "$policy" "conflict" "other" "fork" "$fork_mr_id"
    if [ "$policy" = "restage" ]; then
        # Only the restage policy will cause an error comment; the others will
        # not be on the stage for other reasons (`unstage` because it got
        # pushed; `ignore` from `post_mr_update_check` to make testing
        # consistent).
        check_mr_comment "false" "$fork_mr_id" 'This merge request has been unstaged due to merge conflicts in the following paths:\n\n  - `'"$branch"'-branch-conflict`'
    fi
    check_statuses "fork" "$branch"
    check_for_test_job "$test_backend" "mr_update"

    # Update the first branch again (fine again).
    info "Pushing an ok branch for $branch to fork..."
    push_branch "fork" "$branch" "ok"
    post_mr_update_check "$branch" "$policy" "new" "other" "fork" "$fork_mr_id"
    check_mr_comment "$quiet" "$fork_mr_id" 'Successfully staged.'
    check_statuses "fork" "$branch"
    check_for_test_job "$test_backend" "mr_update"

    info "------------------------------------------------------------------------"
    info "Checking that unstaging works"
    info "------------------------------------------------------------------------"

    # Unstage the first branch.
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "fork" "Unstaging the fork branch from the $branch stage..." "Do: unstage"
    post_mr_update_check "$branch" "$policy" "unstage" "other" "fork" "$fork_mr_id"
    check_mr_comment "$quiet" "$fork_mr_id" 'This merge request has been unstaged upon request.'

    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "fork" "Testing that unstaging an unstaged branch for $branch is detected..." "Do: unstage"
    check_mr_comment "$quiet" "$fork_mr_id" 'Failed to find this merge request on the stage; ignoring the request to unstage it.'

    info "------------------------------------------------------------------------"
    info "Checking that updating the base branch works"
    info "------------------------------------------------------------------------"

    # Update the base branch.
    info "Updating the $branch branch..."
    test_git -C "$repodir" push origin "$branch-update:$branch"
    __waited_for_gitlab=false
    post_branch_update_check "$branch" "other"
    check_status "other" "$branch" "ghostflow-stager" "success" "staged"
    check_for_test_job "$test_backend" "branch_push"
    # XXX(gitlab): https://gitlab.com/gitlab-org/gitlab-ce/issues/19802
    check_for_test_job "$test_backend" "branch_push"

    info "------------------------------------------------------------------------"
    info "Checking that updating the base branch with conflicts works"
    info "------------------------------------------------------------------------"

    # Update the second branch (OK for now).
    info "Updating the $branch branch to have a conflict..."
    push_branch "other" "$branch" "conflict"
    post_mr_update_check "$branch" "$policy" "update" "" "other" "$other_mr_id"
    check_for_test_job "$test_backend" "mr_update"

    # Update the base branch (conflicts).
    info "Pushing an base-conflicting branch for $branch to other..."
    test_git -C "$repodir" push origin "$branch-conflict:$branch"
    __waited_for_gitlab=false
    post_branch_update_check "$branch" ""
    # Only the restage policy will cause an error comment; the others will
    # not be on the stage for other reasons (`unstage` because it got
    # pushed; `ignore` from `post_mr_update_check`) to make testing
    # consistent.
    check_mr_comment "false" "$other_mr_id" 'This merge request has been unstaged due to an update to the '"$branch"' branch causing merge conflicts in the following paths:\n\n  - `conflict`'

    gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "origin" "Attempting to merge into $branch with unsupported arguments..." "Do: merge --unknown-flag"
    check_mr_comment "$quiet" "$other_mr_id" 'Errors:\n\n  - Unrecognized `merge` arguments: `error: Found argument '"'"'--unknown-flag'"'"' which wasn'"'"'t expected, or isn'"'"'t valid in this context`.'

    gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "origin" "Attempting to merge into $branch with conflicts..." "Do: merge"
    check_mr_comment "$quiet" "$other_mr_id" 'This merge request contains conflicts with `'"$branch"'` in the following paths:\n\n  - `conflict`'

    check_stage "$branch" "" "true" ""
    check_for_test_job "$test_backend" "any"
    check_for_test_job "$test_backend" "any"

    info "------------------------------------------------------------------------"
    info "Checking that tagging the stage works properly"
    info "------------------------------------------------------------------------"

    # Rewind and restage the branch.
    push_branch "other" "$branch" "ok"
    check_statuses "other" "$branch"

    check_stage "$branch" "" "true" ""
    check_for_test_job "$test_backend" "mr_update"

    gitlab_mr_comment "$origin_repo_id" "$other_mr_id" "origin" "Staging $branch so that we can test stage tagging policies..." "Do: stage"
    check_mr_comment "$quiet" "$other_mr_id" 'Successfully staged.'
    check_status "other" "$branch" "ghostflow-stager" "success" "staged"

    # Ensure the stage is as we expect.
    check_stage "$branch" "other" "true" ""

    tag_stage "$branch" "$tag_policy" "$other_mr_id"

    info "------------------------------------------------------------------------"
    info "Checking behavior for closed MRs."
    info "------------------------------------------------------------------------"

    # Close the MR.
    gitlab_api "/projects/$origin_repo_id/merge_requests/$other_mr_id" \
        -X PUT \
        --data "state_event=close" \
        > /dev/null

    if [ "$tag_policy" = "keep_topics" ]; then
        # Make sure the topic is removed from the stage.
        check_mr_comment "false" "$other_mr_id" 'This merge request has been unstaged because it has been closed.'
        check_status "other" "$branch" "ghostflow-stager" "success" "unstaged"
    fi
    check_for_test_job "$test_backend" "mr_update"

    # Try and check a closed MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$other_mr_id" "origin" "Trying to check a closed MR..." "Do: check"

    # Try and stage a closed MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$other_mr_id" "origin" "Trying to stage a closed MR..." "Do: stage"

    # Try and unstage a closed MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$other_mr_id" "origin" "Trying to unstage a closed MR..." "Do: unstage"

    # Try and merge a closed MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$other_mr_id" "origin" "Trying to merge a closed MR..." "Do: merge"

    # Try and test a closed MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$other_mr_id" "origin" "Trying to test a closed MR..." "Do: test"

    # The stage should be empty.
    check_stage "$branch" "" "true" ""

    info "------------------------------------------------------------------------"
    info "Checking that merging a merge request removes it from the stage"
    info "------------------------------------------------------------------------"

    # Stage the fork branch.
    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Staging into $branch to test stage status after a merge..." "Do: stage"
    check_mr_comment "$quiet" "$fork_mr_id" 'Successfully staged.'
    check_status "fork" "$branch" "ghostflow-stager" "success" "staged"

    gitlab_mr_comment "$origin_repo_id" "$fork_mr_id" "origin" "Merging the MR..." "Do: merge"
    #check_mr_comment "$quiet" "$fork_mr_id" 'Successfully merged and pushed.'
    check_mr_comment "$quiet" "$fork_mr_id" 'This merge request has been unstaged because it has already been merged.'
    check_status "fork" "$branch" "ghostflow-stager" "success" "unstaged: already merged"
    check_for_test_job "$test_backend" "any"
    check_for_test_job "$test_backend" "any"
    # XXX(gitlab): https://gitlab.com/gitlab-org/gitlab-ce/issues/19802
    check_for_test_job "$test_backend" "any"

    pushd "$repodir" > /dev/null
    test_git fetch -p "origin"
    merge_message="$( git log --format=%B --max-count=1 "refs/remotes/origin/$branch" )"
    merged_topic="$( git log "--format=%h %s" --abbrev=8 --max-count=1 "refs/remotes/origin/$branch^2" )"
    merged_topic_parent="$( git log "--format=%h %s" --abbrev=8 --max-count=1 "refs/remotes/origin/$branch^2~" )"

    if [ "$branch" = "master" ]; then
        merge_commit_header="Merge topic 'fork-for-$branch'"$'\n\n'
    else
        merge_commit_header="Merge topic 'fork-for-$branch' into $branch"$'\n\n'
    fi
    if $limit_log; then
        merge_commit_log="$merged_topic"$'\n'"..."
    else
        merge_commit_log="$merged_topic"$'\n'"$merged_topic_parent"
    fi
    merge_commit_trailers=$'\n\n'"Acked-by: Ghostflow Director <ghostflow@example.com>"$'\n'"Merge-request: !$fork_mr_id"

    check_string "$merge_message" "$merge_commit_header$merge_commit_log$merge_commit_trailers"
    popd > /dev/null

    info "Checking the status of the source topic..."
    pushd "$repodir" > /dev/null
    if test_git ls-remote --exit-code "fork" "refs/heads/fork-for-$branch"; then
        removed_topic=false
    else
        ls_remote_exit_code="$?"

        if [ "$ls_remote_exit_code" = "2" ]; then
            removed_topic=true
        else
            die "Failed to check the remote for branch deletion."
        fi
    fi
    popd > /dev/null

    if $remove_topic; then
        if $removed_topic; then
            info "Successfully removed the source topic."
        else
            die "Failed to remove the source topic."
        fi
    else
        if $removed_topic; then
            die "Failed to preserve the source topic."
        else
            info "Successfully preserved the source topic."
        fi
    fi

    if [ "$branch" = "master" ]; then
        check_issue "$origin_repo_id" "$issue_id" "$labels"
    else
        # XXX(gitlab): Only MRs targeting the default branch close issues.
        #
        # See https://gitlab.com/gitlab-org/gitlab-ce/issues/28569.
        #wait_for check_issue "$origin_repo_id" "$issue_id" "$labels"
        true
    fi

    # Try and check a merged MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$fork_mr_id" "origin" "Trying to check a merged MR..." "Do: check"

    # Try and stage a merged MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$fork_mr_id" "origin" "Trying to stage a merged MR..." "Do: stage"

    # Try and unstage a merged MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$fork_mr_id" "origin" "Trying to unstage a merged MR..." "Do: unstage"

    # Try and merge a merged MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$fork_mr_id" "origin" "Trying to merge a merged MR..." "Do: merge"

    # Try and test a merged MR.
    gitlab_mr_comment_ignored "$origin_repo_id" "$fork_mr_id" "origin" "Trying to test a merged MR..." "Do: test"

    # The stage should be empty.
    check_stage "$branch" "" "true" ""
done

# Stop ghostflow-director.
RUST_BACKTRACE=1 ghostflow-director --config "$tmpdir/ghostflow-director.yaml" --exit

if [ -z "$DOCKER_RUNNING" ]; then
    if [ -z "$keep_docker" ]; then
        info "Shutting down the docker image..."

        # Stop the container.
        docker stop "$docker_name"

        # Remove the container.
        docker rm "$docker_name"
    else
        info "The docker container is still running and may be shut down with:"
        info "  docker stop $docker_name"
        info "  docker rm $docker_name"
    fi

    info "Shutting down the webhook listener and ghostflow-director..."

    # Kill tools
    kill %1
    kill %2
fi
