// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Configuration management data structures.

mod defaults;
mod checks;
pub mod io;
mod types;

pub use self::io::Config as ConfigRead;
pub use self::types::*;

#[cfg(test)]
pub use self::checks::Checks;
