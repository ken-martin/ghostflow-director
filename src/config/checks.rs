// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::git_checks::{BranchCheck, Check, GitCheckConfiguration, TopicCheck};
use crates::git_checks::checks::*;
use crates::git_workarea::Identity;
use crates::serde_json::{self, Value};

use config::defaults;
use error::*;

use std::time::Duration;

macro_rules! checks {
    { $req_kind:expr, $req_config:expr, $( $kind:expr => $config:ty, $dest:expr, )+ } => {
        match $req_kind {
            $( $kind => Ok($dest.push(Box::new(serde_json::from_value::<$config>($req_config)
                .chain_err(|| format!("failed to parse {} config",
                                      $req_kind))?
                .into_check()))), )*
            _ => Err(format!("unknown check: {}", $req_kind).into()),
        }
    }
}

#[derive(Default)]
/// A collection of checks.
pub struct Checks {
    /// Per-commit checks.
    commit_checks: Vec<Box<Check>>,
    /// Per-branch checks.
    branch_checks: Vec<Box<BranchCheck>>,
    /// Per-topic checks.
    topic_checks: Vec<Box<TopicCheck>>,
}

impl Checks {
    /// Create an empty set of checks.
    pub fn new() -> Self {
        Checks {
            commit_checks: Vec::new(),
            branch_checks: Vec::new(),
            topic_checks: Vec::new(),
        }
    }

    /// Add a check to the collection.
    pub fn add_check(&mut self, kind: &str, config: Value) -> Result<()> {
        checks! {
            kind, config,
            "allow_robot" => AllowRobotConfig, self.branch_checks,
            "bad_commits" => BadCommitsConfig, self.commit_checks,
            "changelog" => ChangelogConfig, self.commit_checks,
            "changelog/topic" => ChangelogConfig, self.topic_checks,
            "check_end_of_line" => CheckEndOfLineConfig, self.commit_checks,
            "check_end_of_line/topic" => CheckEndOfLineConfig, self.topic_checks,
            "check_executable_permissions" => CheckExecutablePermissionsConfig, self.commit_checks,
            "check_executable_permissions/topic" => CheckExecutablePermissionsConfig, self.topic_checks,
            "check_size" => CheckSizeConfig, self.commit_checks,
            "check_size/topic" => CheckSizeConfig, self.topic_checks,
            "check_whitespace" => CheckWhitespaceConfig, self.commit_checks,
            "check_whitespace/topic" => CheckWhitespaceConfig, self.topic_checks,
            "commit_subject" => CommitSubjectConfig, self.commit_checks,
            "formatting" => FormattingConfig, self.commit_checks,
            "formatting/topic" => FormattingConfig, self.topic_checks,
            "invalid_paths" => InvalidPathsConfig, self.commit_checks,
            "invalid_paths/topic" => InvalidPathsConfig, self.topic_checks,
            "invalid_utf8" => InvalidUtf8Config, self.commit_checks,
            "invalid_utf8/topic" => InvalidUtf8Config, self.topic_checks,
            "lfs_pointer" => LfsPointerConfig, self.commit_checks,
            "lfs_pointer/topic" => LfsPointerConfig, self.topic_checks,
            "reject_merges" => RejectMergesConfig, self.commit_checks,
            "reject_separate_root" => RejectSeparateRootConfig, self.commit_checks,
            "reject_symlinks" => RejectSymlinksConfig, self.commit_checks,
            "reject_symlinks/topic" => RejectSymlinksConfig, self.topic_checks,
            "release_branch" => ReleaseBranchConfig, self.branch_checks,
            "restricted_path" => RestrictedPathConfig, self.commit_checks,
            "restricted_path/topic" => RestrictedPathConfig, self.topic_checks,
            "submodule_available" => SubmoduleAvailableConfig, self.commit_checks,
            "submodule_rewind" => SubmoduleRewindConfig, self.commit_checks,
            "submodule_watch" => SubmoduleWatchConfig, self.commit_checks,
            "third_party" => ThirdPartyConfig, self.commit_checks,
            "valid_name" => ValidNameConfig, self.commit_checks,
        }
    }

    /// Whether there are any checks or not.
    pub fn is_empty(&self) -> bool {
        self.commit_checks.is_empty() &&
            self.branch_checks.is_empty() &&
            self.topic_checks.is_empty()
    }

    /// Get the configuration for the checks.
    pub fn config(&self) -> GitCheckConfiguration {
        let mut conf = GitCheckConfiguration::new();

        for check in &self.commit_checks {
            conf.add_check(check.as_ref());
        }

        for check in &self.branch_checks {
            conf.add_branch_check(check.as_ref());
        }

        for check in &self.topic_checks {
            conf.add_topic_check(check.as_ref());
        }

        conf
    }
}

#[derive(Deserialize, Debug)]
struct AllowRobotConfig {
    name: String,
    email: String,
}

impl AllowRobotConfig {
    fn into_check(self) -> AllowRobot {
        let identity = Identity::new(&self.name, &self.email);
        AllowRobot::new(identity)
    }
}

#[derive(Deserialize, Debug)]
struct BadCommitsConfig {
    bad_commits: Vec<String>,
}

impl BadCommitsConfig {
    fn into_check(self) -> BadCommits {
        BadCommits::new(&self.bad_commits)
    }
}

#[serde(tag="style")]
#[derive(Deserialize, Debug)]
enum ChangelogConfig {
    #[serde(rename="directory")]
    Directory {
        path: String,
        #[serde(default)]
        extension: Option<String>,
        #[serde(default="defaults::default_false")]
        required: bool,
    },
    #[serde(rename="file")]
    File {
        path: String,
        #[serde(default="defaults::default_false")]
        required: bool,
    }
}

impl ChangelogConfig {
    fn into_check(self) -> Changelog {
        let (style, required) = match self {
            ChangelogConfig::Directory { path, extension, required } => {
                (ChangelogStyle::directory(path, extension), required)
            },
            ChangelogConfig::File { path, required } => {
                (ChangelogStyle::file(path), required)
            },
        };

        let mut check = Changelog::new(style);

        check.required(required);

        check
    }
}

#[derive(Deserialize, Debug)]
struct CheckEndOfLineConfig {
}

impl CheckEndOfLineConfig {
    fn into_check(self) -> CheckEndOfLine {
        CheckEndOfLine::new()
    }
}

#[derive(Deserialize, Debug)]
struct CheckExecutablePermissionsConfig {
    extensions: Vec<String>,
}

impl CheckExecutablePermissionsConfig {
    fn into_check(self) -> CheckExecutablePermissions {
        CheckExecutablePermissions::new(&self.extensions)
    }
}

fn default_max_size() -> usize {
    1 << 20
}

#[derive(Deserialize, Debug)]
struct CheckSizeConfig {
    #[serde(default="default_max_size")]
    max_size: usize,
}

impl CheckSizeConfig {
    fn into_check(self) -> CheckSize {
        CheckSize::new(self.max_size)
    }
}

#[derive(Deserialize, Debug)]
struct CheckWhitespaceConfig {
}

impl CheckWhitespaceConfig {
    fn into_check(self) -> CheckWhitespace {
        CheckWhitespace::new()
    }
}

fn default_min_subject_length() -> usize {
    8
}

fn default_max_subject_length() -> usize {
    78
}

#[derive(Deserialize, Debug)]
struct CommitSubjectConfig {
    #[serde(default="default_min_subject_length")]
    min_length: usize,
    #[serde(default="default_max_subject_length")]
    max_length: usize,

    #[serde(default="defaults::default_true")]
    check_wip: bool,
    #[serde(default="defaults::default_true")]
    check_rebase_commands: bool,
}

impl CommitSubjectConfig {
    fn into_check(self) -> CommitSubject {
        let mut check = CommitSubject::new();

        check
            .with_summary_limits(self.min_length, self.max_length)
            .check_work_in_progress(self.check_wip)
            .check_rebase_commands(self.check_rebase_commands);

        check
    }
}

#[derive(Deserialize, Debug)]
struct FormattingConfig {
    #[serde(default)]
    name: Option<String>,
    kind: String,
    formatter: String,
    #[serde(default)]
    config_files: Vec<String>,
    #[serde(default)]
    fix_message: Option<String>,
    #[serde(default)]
    timeout: Option<u64>,
}

impl FormattingConfig {
    fn into_check(self) -> Formatting {
        let mut check = Formatting::new(self.kind, self.formatter);

        check.add_config_files(self.config_files);

        self.name.map(|name| check.named(name));
        self.fix_message.map(|message| check.with_fix_message(message));
        self.timeout.map(|timeout| check.with_timeout(Duration::from_secs(timeout)));

        check
    }
}

fn default_invalid_characters() -> String {
    r#"<>:"|?*"#.to_string()
}

#[derive(Deserialize, Debug)]
struct InvalidPathsConfig {
    #[serde(default="default_invalid_characters")]
    invalid_characters: String,
}

impl InvalidPathsConfig {
    fn into_check(self) -> InvalidPaths {
        InvalidPaths::new(self.invalid_characters)
    }
}

#[derive(Deserialize, Debug)]
struct InvalidUtf8Config {
}

impl InvalidUtf8Config {
    fn into_check(self) -> InvalidUtf8 {
        InvalidUtf8::new()
    }
}

#[derive(Deserialize, Debug)]
struct LfsPointerConfig {
}

impl LfsPointerConfig {
    fn into_check(self) -> LfsPointer {
        LfsPointer::new()
    }
}

#[derive(Deserialize, Debug)]
struct RejectMergesConfig {
}

impl RejectMergesConfig {
    fn into_check(self) -> RejectMerges {
        RejectMerges::new()
    }
}

#[derive(Deserialize, Debug)]
struct RejectSeparateRootConfig {
}

impl RejectSeparateRootConfig {
    fn into_check(self) -> RejectSeparateRoot {
        RejectSeparateRoot::new()
    }
}

#[derive(Deserialize, Debug)]
struct RejectSymlinksConfig {
}

impl RejectSymlinksConfig {
    fn into_check(self) -> RejectSymlinks {
        RejectSymlinks::new()
    }
}

#[derive(Deserialize, Debug)]
struct ReleaseBranchConfig {
    #[serde(default="defaults::default_release_branch")]
    branch: String,
    disallowed_commit: String,
    #[serde(default="defaults::default_false")]
    required: bool,
}

impl ReleaseBranchConfig {
    fn into_check(self) -> ReleaseBranch {
        let mut check = ReleaseBranch::new(self.branch, self.disallowed_commit);

        check.set_required(self.required);

        check
    }
}

#[derive(Deserialize, Debug)]
struct RestrictedPathConfig {
    restricted_path: String,
    #[serde(default="defaults::default_true")]
    required: bool,
}

impl RestrictedPathConfig {
    fn into_check(self) -> RestrictedPath {
        let mut check = RestrictedPath::new(&self.restricted_path);

        check.required(self.required);

        check
    }
}

#[derive(Deserialize, Debug)]
struct SubmoduleAvailableConfig {
    #[serde(default="defaults::default_false")]
    require_first_parent: bool,
}

impl SubmoduleAvailableConfig {
    fn into_check(self) -> SubmoduleAvailable {
        let mut check = SubmoduleAvailable::new();

        check.require_first_parent(self.require_first_parent);

        check
    }
}

#[derive(Deserialize, Debug)]
struct SubmoduleRewindConfig {
}

impl SubmoduleRewindConfig {
    fn into_check(self) -> SubmoduleRewind {
        SubmoduleRewind::new()
    }
}

#[derive(Deserialize, Debug)]
struct SubmoduleWatchConfig {
    #[serde(default="defaults::default_false")]
    reject_additions: bool,
    #[serde(default="defaults::default_false")]
    reject_removals: bool,
}

impl SubmoduleWatchConfig {
    fn into_check(self) -> SubmoduleWatch {
        let mut check = SubmoduleWatch::new();

        check
            .reject_additions(self.reject_additions)
            .reject_removals(self.reject_removals);

        check
    }
}

#[derive(Deserialize, Debug)]
struct ThirdPartyConfig {
    name: String,
    path: String,
    root: String,
    script: String,
}

impl ThirdPartyConfig {
    fn into_check(self) -> ThirdParty {
        ThirdParty::new(self.name, self.path, self.root, self.script)
    }
}

#[derive(Deserialize, Debug, Clone, Copy)]
enum ValidNameFullNamePolicyIO {
    #[serde(rename="required")]
    Required,
    #[serde(rename="preferred")]
    Preferred,
    #[serde(rename="optional")]
    Optional,
}

impl Default for ValidNameFullNamePolicyIO {
    fn default() -> Self {
        ValidNameFullNamePolicyIO::Required
    }
}

impl From<ValidNameFullNamePolicyIO> for ValidNameFullNamePolicy {
    fn from(policy: ValidNameFullNamePolicyIO) -> Self {
        match policy {
            ValidNameFullNamePolicyIO::Required => ValidNameFullNamePolicy::Required,
            ValidNameFullNamePolicyIO::Preferred => ValidNameFullNamePolicy::Preferred,
            ValidNameFullNamePolicyIO::Optional => ValidNameFullNamePolicy::Optional,
        }
    }
}

#[derive(Deserialize, Debug)]
struct ValidNameConfig {
    #[serde(default)]
    full_name_policy: ValidNameFullNamePolicyIO,
}

impl ValidNameConfig {
    fn into_check(self) -> ValidName {
        let mut check = ValidName::new();

        check
            .set_full_name_policy(self.full_name_policy.into());

        check
    }
}
