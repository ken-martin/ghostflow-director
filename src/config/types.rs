// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::Utc;
use crates::ghostflow::actions::{check, clone, data, follow, merge, reformat, stage, test};
use crates::ghostflow::host::{AccessLevel, HostedProject, HostingService, Membership, MergeRequest,
                              User};
use crates::ghostflow::host::Result as HostResult;
use crates::ghostflow::utils::Trailer;
use crates::git_topic_stage::Stager;
use crates::git_workarea::{CommitId, GitContext, Identity};
use crates::itertools::Itertools;
use crates::rayon::prelude::*;
use crates::serde_json::{self, Value};

use config::checks::Checks;
use config::io;
use error::Result;

use std::cell::{RefCell, RefMut};
use std::collections::hash_map::HashMap;
use std::collections::hash_set::HashSet;
use std::path::{Path, PathBuf};
use std::result;
use std::sync::Arc;
use std::time::Duration;

pub use config::io::StageUpdatePolicy;

/// Main configuration object for the robot.
pub struct Config {
    /// The archive directory.
    pub archive_dir: PathBuf,
    /// The directory to watch for new jobs.
    pub queue_dir: PathBuf,
    /// Host configuration.
    pub hosts: HashMap<String, Host>,
}

impl Config {
    /// Read the configuration from a path.
    ///
    /// The `connect_to_host` function constructs `HostingService` instances given an API and a URL
    /// to communicate with.
    pub fn from_path<P, F>(path: P, connect_to_host: F) -> Result<Self>
        where P: AsRef<Path>,
              F: Fn(&str, &Option<String>, Value) -> Result<Arc<HostingService>>,
    {
        io::Config::from_path(path).and_then(|config| {
            let archive_dir = config.archive_dir().to_path_buf();
            let queue_dir = config.queue_dir().to_path_buf();
            let workdir = PathBuf::from(config.workdir);
            let hosts = config.hosts
                .into_iter()
                .map(|(name, host)| {
                    let host_workdir = workdir.join(&name);
                    let secrets = host.secrets()?;
                    let service = connect_to_host(&host.host_api, &host.host_url, secrets)?;
                    let host = Host::new(host, service, host_workdir)?;

                    Ok((name, host))
                })
                .collect::<Result<HashMap<_, _>>>()?;

            Ok(Config {
                archive_dir: archive_dir,
                queue_dir: queue_dir,
                hosts: hosts,
            })
        })
    }
}

/// Configuration for a host.
pub struct Host {
    /// The API used to communicate with the host.
    pub api: String,
    /// The `HostingService` instance to communicate with the host.
    pub service: Arc<HostingService>,
    /// The users to notify when errors occur with the workflow.
    pub maintainers: Vec<String>,
    /// Projects which have workflow actions configured.
    pub projects: HashMap<String, Project>,
    /// A directory which may be used for on-disk scratchspace.
    pub workdir: PathBuf,
    /// The webhook URL to register for new projects.
    pub webhook_url: Option<String>,
}

impl Host {
    /// Create a new host object from the configuration block.
    fn new(host: io::Host, service: Arc<HostingService>, workdir: PathBuf) -> Result<Self> {
        if host.maintainers.is_empty() {
            return Err("no maintainers were specified".into());
        }

        let project_workdir = workdir.join("projects");
        let maintainers = host.maintainers.clone();
        let projects = host.projects
            .into_par_iter()
            .map(|(name, project)| {
                let project = Project::new(&name,
                                           project,
                                           service.clone(),
                                           &project_workdir,
                                           &maintainers)?;

                Ok((name, project))
            })
            .collect::<Vec<_>>()
            .into_iter()
            .collect::<Result<HashMap<_, _>>>()?;

        Ok(Host {
            api: host.host_api,
            service: service,
            maintainers: host.maintainers,
            projects: projects,
            workdir: workdir,
            webhook_url: host.webhook_url,
        })
    }
}

/// Configuration for a project.
pub struct Project {
    /// The `HostingService` instance to communicate with the host.
    pub service: Arc<HostingService>,
    /// A context for working with the git repository of the project.
    pub context: GitContext,
    /// Branches which have workflow actions configured.
    pub branches: HashMap<String, Branch>,
    /// Data handling for the project.
    pub data: Option<data::Data>,
    /// A list of all branches in the project.
    pub branch_names: Vec<String>,
    /// Users who have non-anonymous access to the project.
    members: RefCell<HashMap<u64, Membership>>,
    /// Users who should be notified on important events.
    maintainers: Vec<String>,
}

impl Project {
    /// Create a new project object from the configuration block.
    fn new(name: &str, project: io::Project, service: Arc<HostingService>, workdir: &Path,
           admins: &[String])
           -> Result<Self> {
        use error::ResultExt;
        let hosted_project = HostedProject {
            name: name.to_string(),
            service: service.clone(),
        };
        let members = Self::member_map(hosted_project.members()
            .chain_err(|| "failed to get project members")?);
        let mut clone = clone::Clone_::new(workdir, hosted_project.clone());
        project.submodules
            .iter()
            .foreach(|(name, path)| {
                let link = clone::CloneSubmoduleLink::new(path);
                clone.with_submodule(name, link);
            });
        let context = clone.clone_watched_repo()
            .chain_err(|| "failed to clone repository")?;
        let identity = Identity::new(project.name.clone(), project.email.clone());
        let project_checks = project.checks.clone();
        let maintainers = project.maintainers
            .iter()
            .cloned()
            .chain(admins.iter().cloned())
            .collect();
        let data = if let Some(data_conf) = project.data {
            let mut data = data::Data::new(context.clone());

            if data_conf.destinations.is_empty() {
                bail!("there must be at least one destination for syncing data");
            }

            data_conf.destinations
                .iter()
                .fold(&mut data, data::Data::add_destination);

            if data_conf.keep_refs {
                data.keep_refs();
            }

            data.ref_namespace(data_conf.namespace);

            Some(data)
        } else {
            None
        };

        let branches = project.branches
            .into_par_iter()
            .map(|(branch_name, branch)| {
                let mut checks = Checks::new();
                let mut seen = HashSet::new();

                branch.checks
                    .iter()
                    .chain(project_checks.iter())
                    .map(|(name, check_config)| {
                        if seen.insert(name) {
                            if let Some(ref kind) = check_config.kind {
                                checks.add_check(kind, check_config.config.clone())
                            } else {
                                Ok(())
                            }
                        } else {
                            Ok(())
                        }
                    })
                    .collect::<Result<Vec<_>>>()?;

                if checks.is_empty() {
                    warn!(target: "ghostflow-director/checks",
                          "no checks configured for a branch");
                }

                let mut cbranch = Branch::new(&branch_name, checks, context.clone(), identity.clone());

                cbranch.add_issue_labels(branch.issue_labels.iter());

                if let Some(ref follow) = branch.follow {
                    cbranch.add_follow(follow);
                }

                if let Some(ref merge) = branch.merge {
                    cbranch.add_merge(merge, hosted_project.clone())
                        .chain_err(|| "failed to add merge action")?;
                }

                if let Some(ref reformat) = branch.reformat {
                    cbranch.add_reformat(reformat, hosted_project.clone())
                        .chain_err(|| "failed to add reformat action")?;
                }

                if let Some(ref stage) = branch.stage {
                    cbranch.add_stage(stage, hosted_project.clone())
                        .chain_err(|| "failed to add stage action")?;
                }

                if let Some(ref test) = branch.test {
                    cbranch.add_test(test, hosted_project.clone())
                        .chain_err(|| "failed to add test action")?;
                }

                Ok((branch_name.clone(), cbranch))
            })
            .collect::<Vec<_>>()
            .into_iter()
            .collect::<Result<HashMap<_, _>>>()?;

        let ref_prefix = "refs/heads/";
        let for_each_ref = context.git()
            .arg("for-each-ref")
            .arg("--format=%(refname)")
            .arg(ref_prefix)
            .output()
            .chain_err(|| "failed to construct for-each-ref command")?;
        if !for_each_ref.status.success() {
            bail!("could not list refs in the {} project: {}",
                  name,
                  String::from_utf8_lossy(&for_each_ref.stderr));
        }
        let branch_ref_names = String::from_utf8_lossy(&for_each_ref.stdout);
        let branch_names = branch_ref_names.lines()
            .map(|refname| refname[ref_prefix.len()..].to_string())
            .collect();

        Ok(Project {
            service: service,
            context: context,
            branches: branches,
            data: data,
            branch_names: branch_names,
            members: RefCell::new(members),
            maintainers: maintainers,
        })
    }

    /// Add a member to the project.
    ///
    /// Overwrites the previous membership structure.
    pub fn add_member(&self, member: Membership) {
        self.members
            .borrow_mut()
            .insert(member.user.id, member);
    }

    /// Remove a member from the project.
    pub fn remove_member(&self, user: &User) {
        self.members
            .borrow_mut()
            .remove(&user.id);
    }

    /// Get the access level of a user to the project.
    pub fn access_level(&self, user: &User) -> AccessLevel {
        self.members
            .borrow()
            .get(&user.id)
            .and_then(|member| {
                // If the user's membership has expired, ignore their access level.
                if let Some(expiration) = member.expiration {
                    if expiration <= Utc::now() {
                        return None;
                    }
                }

                Some(member.access_level)
            })
            .unwrap_or(AccessLevel::Contributor)
    }

    /// Fetch membership information from the host.
    pub fn refresh_membership(&self, project: &str) -> HostResult<()> {
        *self.members.borrow_mut() = Self::member_map(self.service.members(project)?);

        Ok(())
    }

    /// Create a hashmap of user IDs to their membership.
    fn member_map(members: Vec<Membership>) -> HashMap<u64, Membership> {
        members.into_iter()
            .map(|member| (member.user.id, member))
            .collect()
    }

    /// Get the check action for a given branch.
    fn check_action<'a>(&'a self, branch: &'a Branch) -> check::Check {
        check::Check::new(self.context.clone(),
                          self.service.clone(),
                          branch.checks.config(),
                          &self.maintainers)
    }

    /// Check that a merge request is OK.
    pub fn check_mr(&self, branch: &Branch, mr: &MergeRequest)
                    -> check::Result<check::CheckStatus> {
        self.check_action(branch)
            .check_mr(format!("mr/{}", mr.id), &CommitId::new(&branch.name), mr)
    }

    /// Check that a merge request is OK with a given commit.
    pub fn check_mr_with(&self, branch: &Branch, mr: &MergeRequest, commit: &CommitId)
                         -> check::Result<check::CheckStatus> {
        self.check_action(branch)
            .check_mr_with(format!("mr/{}", mr.id),
                           &CommitId::new(&branch.name),
                           mr,
                           commit)
    }
}

/// The filter for the workflow policy.
pub struct WorkflowMergePolicyFilter {
    /// The trailers for the merge commit message.
    trailers: Vec<Trailer>,
    /// Errors which have occurred due to the policy.
    errors: Vec<String>,
}

impl merge::MergePolicyFilter for WorkflowMergePolicyFilter {
    fn process_trailer(&mut self, trailer: &Trailer, _: Option<&User>) {
        // Ignore trailers without a `-by` suffix.
        if !trailer.token.ends_with("-by") {
            return;
        }

        // TODO: Allow only a whitelist of trailers.
        // TODO: Require at least an Acked-by (with sufficient permissions).
        // TODO: Require Reviewed-by (with sufficient permissions/assignee(s)).
        // TODO: Look at Rejected-by.

        // Accept the trailer.
        self.trailers.push(trailer.clone());
    }

    fn result(self) -> result::Result<Vec<Trailer>, Vec<String>> {
        if self.errors.is_empty() {
            Ok(self.trailers)
        } else {
            Err(self.errors)
        }
    }
}

#[derive(Debug)]
/// The policy structure for merging.
pub struct WorkflowMergePolicy {
    /// The merge policy for the branch.
    conf: io::MergePolicy,
    /// The identity of the director.
    identity: Identity,
}

impl WorkflowMergePolicy {
    /// Create a new merge policy.
    fn new(conf: io::MergePolicy, identity: Identity) -> Self {
        WorkflowMergePolicy {
            conf: conf,
            identity: identity,
        }
    }
}

impl merge::MergePolicy for WorkflowMergePolicy {
    type Filter = WorkflowMergePolicyFilter;

    fn for_mr(&self, _: &MergeRequest) -> Self::Filter {
        let mut trailers = Vec::new();

        trailers.push(Trailer::new("Acked-by", format!("{}", self.identity)));

        WorkflowMergePolicyFilter {
            trailers: trailers,
            errors: Vec::new(),
        }
    }
}

/// Representation of a check action for a branch.
pub struct CheckAction;

/// Representation of a follow action for a branch.
pub struct FollowAction {
    /// The follow action itself.
    follow: follow::Follow,
}

impl FollowAction {
    /// A reference to the merge workflow action.
    pub fn follow(&self) -> &follow::Follow {
        &self.follow
    }
}

/// Representation of a merge action for a branch.
pub struct MergeAction {
    /// The merge action itself for the default policy.
    merge: merge::Merge<WorkflowMergePolicy>,
    /// Access level requirement to be able to use the stage action for a branch.
    pub access_level: AccessLevel,
}

impl MergeAction {
    /// A reference to the merge workflow action.
    pub fn merge(&self) -> &merge::Merge<WorkflowMergePolicy> {
        &self.merge
    }
}

/// Representation of a reformat action for a branch.
pub struct ReformatAction {
    /// The reformat action itself.
    reformat: reformat::Reformat,
    /// Access level requirement to be able to use the reformat action for a branch.
    pub access_level: AccessLevel,
}

impl ReformatAction {
    /// A reference to the reformat workflow action.
    pub fn reformat(&self) -> &reformat::Reformat {
        &self.reformat
    }
}

/// Representation of a stage action for a branch.
pub struct StageAction {
    /// The stage action itself.
    stage: RefCell<stage::Stage>,
    /// The policy for updating branches on this stage.
    pub policy: StageUpdatePolicy,
    /// Access level requirement to be able to use the stage action for a branch.
    pub access_level: AccessLevel,
}

impl StageAction {
    /// A mutable reference to the stage workflow action.
    pub fn stage(&self) -> RefMut<stage::Stage> {
        self.stage.borrow_mut()
    }
}

/// The backend action to use for the branch's testing.
pub enum TestBackend {
    /// The branch uses the job-based test backend.
    Jobs {
        /// The action itself.
        action: test::jobs::TestJobs,
        /// The help string for the job handler.
        help: String,
    },
    /// The branch uses the ref-based test backend.
    Refs(test::refs::TestRefs),
}

/// Representation of a test action for a branch.
pub struct TestAction {
    /// The test backend.
    test: TestBackend,
    /// Access level requirement to be able to use the test action for a branch.
    pub access_level: AccessLevel,
    /// Whether to trigger the action for branch updates or not.
    pub test_updates: bool,
}

impl TestAction {
    /// A reference to the test workflow action.
    pub fn test(&self) -> &TestBackend {
        &self.test
    }
}

/// Configuration for a branch within a project.
pub struct Branch {
    /// The name of the branch.
    pub name: String,
    /// Issue labels.
    pub issue_labels: Vec<String>,

    /// The checks for the branch.
    checks: Checks,

    /// The git context for the branch.
    context: GitContext,
    /// The identity to use for actions on the branch.
    identity: Identity,

    /// The check action for the branch.
    check: Option<CheckAction>,
    /// The follow action for the branch.
    follow: Option<FollowAction>,
    /// The merge action for the branch.
    merge: Option<MergeAction>,
    /// The reformat action for the branch.
    reformat: Option<ReformatAction>,
    /// The stage action for the branch.
    stage: Option<StageAction>,
    /// The test action for the branch.
    test: Option<TestAction>,
}

impl From<io::AccessLevelIO> for AccessLevel {
    fn from(access: io::AccessLevelIO) -> Self {
        match access {
            io::AccessLevelIO::Contributor => AccessLevel::Contributor,
            io::AccessLevelIO::Developer => AccessLevel::Developer,
            io::AccessLevelIO::Maintainer => AccessLevel::Maintainer,
            io::AccessLevelIO::Owner => AccessLevel::Owner,
        }
    }
}

impl Branch {
    /// Create a new branch with its checks.
    pub fn new(name: &str, checks: Checks, context: GitContext, identity: Identity) -> Self {
        Branch {
            name: name.to_string(),
            issue_labels: Vec::new(),

            checks: checks,

            context: context,
            identity: identity,

            check: Some(CheckAction),
            follow: None,
            merge: None,
            reformat: None,
            stage: None,
            test: None,
        }
    }

    /// Add issue labels for the branch.
    fn add_issue_labels<I, L>(&mut self, labels: I) -> &mut Self
        where I: Iterator<Item = L>,
              L: ToString,
    {
        self.issue_labels.extend(labels.map(|label| label.to_string()));
        self
    }

    /// Add a merge action to the branch.
    pub fn add_follow(&mut self, follow_conf: &io::Follow) -> &mut Self {
        let mut follow = follow::Follow::new(self.context.clone(), &self.name);
        follow.ref_namespace(&follow_conf.ref_namespace);

        self.follow = Some(FollowAction {
            follow: follow,
        });
        self
    }

    /// Add a merge action to the branch.
    pub fn add_merge(&mut self, merge_conf: &io::Merge, project: HostedProject)
                     -> merge::Result<&mut Self> {
        let policy = WorkflowMergePolicy::new(merge_conf.policy, self.identity.clone());
        let mut settings = merge::MergeSettings::new(&self.name, policy);

        if merge_conf.quiet {
            settings.quiet();
        }

        settings.log_limit(merge_conf.log_limit);

        self.merge = Some(MergeAction {
            merge: merge::Merge::new(self.context.clone(), project, settings),
            access_level: merge_conf.required_access_level.into(),
        });
        Ok(self)
    }

    /// Add a reformat action to the branch.
    pub fn add_reformat(&mut self, reformat_conf: &io::Reformat, project: HostedProject)
                        -> reformat::Result<&mut Self> {
        let mut reformat = reformat::Reformat::new(self.context.clone(), project);

        let formatters = reformat_conf.formatters
            .iter()
            .map(|formatter_conf| {
                let mut formatter = reformat::Formatter::new(&formatter_conf.kind,
                                                             &formatter_conf.formatter)?;
                formatter.add_config_files(formatter_conf.config_files.iter());
                formatter_conf.timeout.map(|timeout| formatter.with_timeout(Duration::from_secs(timeout)));
                Ok(formatter)
            })
            .collect::<reformat::Result<Vec<_>>>()?;
        reformat.add_formatters(formatters);

        self.reformat = Some(ReformatAction {
            reformat: reformat,
            access_level: reformat_conf.required_access_level.into(),
        });
        Ok(self)
    }

    /// Add a stage action to the branch.
    pub fn add_stage(&mut self, stage_conf: &io::Stage, project: HostedProject)
                     -> stage::Result<&mut Self> {
        let branch = format!("refs/stage/{}/head", self.name);
        let stager = Stager::from_branch(&self.context,
                                         CommitId::new(&self.name),
                                         CommitId::new(branch),
                                         self.identity.clone())?;

        let mut stage = stage::Stage::new(stager, &self.name, project)?;

        if stage_conf.quiet {
            stage.quiet();
        }

        self.stage = Some(StageAction {
            stage: RefCell::new(stage),
            policy: stage_conf.update_policy,
            access_level: stage_conf.required_access_level.into(),
        });
        Ok(self)
    }

    /// Add a test action to the branch.
    pub fn add_test(&mut self, test_conf: &io::Test, project: HostedProject)
                    -> test::Result<&mut Self> {
        use crates::ghostflow::actions::test::ResultExt;

        let (test_backend, updates) = match test_conf.backend {
            io::TestBackend::Jobs => {
                let config: io::TestJobsConfig = serde_json::from_value(test_conf.config
                        .clone()).chain_err(|| "failed to deserialize the test jobs config")?;
                let mut action = test::jobs::TestJobs::new(&config.queue, project)
                    .chain_err(|| "failed to construct the test jobs action")?;

                if config.quiet {
                    action.quiet();
                }

                let backend = TestBackend::Jobs {
                    action: action,
                    help: config.help_string,
                };

                (backend, config.test_updates)
            },
            io::TestBackend::Refs => {
                let config: io::TestRefsConfig = serde_json::from_value(test_conf.config
                        .clone()).chain_err(|| "failed to deserialize the test refs config")?;
                let mut action = test::refs::TestRefs::new(self.context.clone(), project);

                if config.quiet {
                    action.quiet();
                }
                action.ref_namespace(config.namespace);

                (TestBackend::Refs(action), false)
            },
        };

        self.test = Some(TestAction {
            test: test_backend,
            access_level: test_conf.required_access_level.into(),
            test_updates: updates,
        });

        Ok(self)
    }

    /// Get the help action for the branch.
    pub fn help(&self) -> Option<()> {
        Some(())
    }

    /// Get the check action for the branch.
    pub fn check(&self) -> Option<&CheckAction> {
        self.check.as_ref()
    }

    /// Get the follow action for the branch.
    pub fn follow(&self) -> Option<&FollowAction> {
        self.follow.as_ref()
    }

    /// Get the merge action for the branch.
    pub fn merge(&self) -> Option<&MergeAction> {
        self.merge.as_ref()
    }

    /// Get the reformat action for the branch.
    pub fn reformat(&self) -> Option<&ReformatAction> {
        self.reformat.as_ref()
    }

    /// Get the stage action for the branch.
    pub fn stage(&self) -> Option<&StageAction> {
        self.stage.as_ref()
    }

    /// Get the test action for the branch.
    pub fn test(&self) -> Option<&TestAction> {
        self.test.as_ref()
    }
}
