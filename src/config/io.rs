// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Configuration reader types.
//!
//! This module contains types which represent the configuration of the director.

use crates::itertools::Itertools;
use crates::serde_json::{self, Value};
use crates::serde_yaml;
use crates::yaml_merge_keys;

use config::checks::Checks;
use config::defaults;
use error::*;

use std::collections::hash_map::HashMap;
use std::fs::File;
use std::path::Path;

fn empty_object() -> Value {
    Value::Object(Default::default())
}

fn default_data_ref_namespace() -> String {
    "data".to_string()
}

#[derive(Deserialize, Serialize, Debug, Clone)]
/// Configuration for handling of data.
pub struct Data {
    #[serde(default="defaults::default_false")]
    /// Whether the keep data refs after handling data pushes.
    pub keep_refs: bool,

    #[serde(default="default_data_ref_namespace")]
    /// The namespace for data references.
    pub namespace: String,

    /// The destinations for the data.
    pub destinations: Vec<String>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
/// Configuration for a check.
pub struct CheckConfig {
    #[serde(default)]
    /// The kind of check to perform.
    ///
    /// Skip to remove the check.
    pub kind: Option<String>,
    #[serde(default="empty_object")]
    /// Configuration object for the check.
    pub config: Value,
}

fn default_follow_ref_namespace() -> String {
    "follow".to_string()
}

#[derive(Deserialize, Serialize, Debug)]
/// Configuration for the follow action for a branch.
pub struct Follow {
    #[serde(default="default_follow_ref_namespace")]
    /// The namespace for follow references.
    pub ref_namespace: String,
}

#[derive(Deserialize, Serialize, Debug, Default, Clone, Copy)]
/// Policies for collecting information from a merge request before merging.
pub struct MergePolicy;

#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
/// Access level strings.
pub enum AccessLevelIO {
    #[serde(rename="contributor")]
    /// A user external to the project.
    Contributor,
    #[serde(rename="developer")]
    /// A user with permission to develop on the project directly.
    Developer,
    #[serde(rename="maintainer")]
    /// A user with permission to maintain integration branches on the project.
    Maintainer,
    #[serde(rename="owner")]
    /// A user with permission to administrate the project.
    Owner,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
/// Configuration for the merge action for a branch.
pub struct Merge {
    #[serde(default="defaults::default_false")]
    /// If true, only errors and essential comments will be made to the service.
    pub quiet: bool,
    /// The maximum number of commit summaries to show in the merge commit message.
    ///
    /// Set to `null` for `unlimited` and `0` for no logs.
    pub log_limit: Option<usize>,

    /// The access level required to be able to stage.
    pub required_access_level: AccessLevelIO,

    #[serde(default)]
    /// The policy to use for merging.
    pub policy: MergePolicy,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
/// Configuration for a formatting tool on a project.
pub struct Formatter {
    /// The "kind" of formatter.
    ///
    /// Used for attribute lookup.
    pub kind: String,
    /// The path to the formatter.
    pub formatter: String,

    #[serde(default)]
    /// Paths within the repository which contain configuration for the formatter.
    pub config_files: Vec<String>,

    #[serde(default)]
    /// A timeout for running the formatter, in seconds.
    pub timeout: Option<u64>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
/// Configuration for the stage action for a branch.
pub struct Reformat {
    /// The access level required to be able to stage.
    pub required_access_level: AccessLevelIO,

    /// The formatters to use for the action.
    pub formatters: Vec<Formatter>,
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
/// Policies for updates to branches which have been merged into the stage.
pub enum StageUpdatePolicy {
    #[serde(rename="ignore")]
    /// Updates should be ignored; the previous merge should be used instead.
    Ignore,
    #[serde(rename="restage")]
    /// The updated branch should be added to the stage.
    Restage,
    #[serde(rename="unstage")]
    /// The branch should be removed from the stage when an update appears.
    Unstage,
}

impl Default for StageUpdatePolicy {
    fn default() -> Self {
        StageUpdatePolicy::Unstage
    }
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
/// Configuration for the stage action for a branch.
pub struct Stage {
    #[serde(default="defaults::default_false")]
    /// If true, only errors and essential comments will be made to the service.
    pub quiet: bool,

    /// The access level required to be able to stage.
    pub required_access_level: AccessLevelIO,

    #[serde(default)]
    /// The update policy for merge requests which receive new code.
    pub update_policy: StageUpdatePolicy,
}

#[derive(Deserialize, Serialize, Debug, Clone, Copy)]
/// Backends for performing tests for projects.
pub enum TestBackend {
    #[serde(rename="jobs")]
    /// Job files will be created in a directory for test commands.
    Jobs,
    #[serde(rename="refs")]
    /// Refs will be pushed to the remote to indicate topics to test.
    Refs,
}

#[derive(Deserialize, Serialize, Debug)]
/// Configuration for the `jobs` test backend.
pub struct TestJobsConfig {
    #[serde(default="defaults::default_false")]
    /// If true, only errors and essential comments will be made to the service.
    pub quiet: bool,

    /// The directory to drop job files into.
    pub queue: String,

    #[serde(default="defaults::default_true")]
    /// Whether to test updates to the branch itself or not.
    pub test_updates: bool,

    /// The help string for usage of the backend.
    pub help_string: String,
}

fn default_test_ref_namespace() -> String {
    "test-topics".to_string()
}

#[derive(Deserialize, Serialize, Debug)]
/// Configuration for the `refs` test backend.
pub struct TestRefsConfig {
    #[serde(default="defaults::default_false")]
    /// If true, only errors and essential comments will be made to the service.
    pub quiet: bool,

    #[serde(default="default_test_ref_namespace")]
    /// The namespace for test references.
    pub namespace: String,
}

#[derive(Deserialize, Serialize, Debug)]
/// Configuration for the test action for a branch.
pub struct Test {
    /// The access level required to be able to test.
    pub required_access_level: AccessLevelIO,

    /// The backend to use for testing topics for this branch.
    pub backend: TestBackend,

    #[serde(default="empty_object")]
    /// The configuration for the backend.
    pub config: Value,
}

#[derive(Deserialize, Serialize, Debug)]
/// Configuration for a branch within a project.
pub struct Branch {
    #[serde(default)]
    /// The follow configuration.
    pub follow: Option<Follow>,
    #[serde(default)]
    /// The merge configuration.
    pub merge: Option<Merge>,
    #[serde(default)]
    /// The reformat configuration.
    pub reformat: Option<Reformat>,
    #[serde(default)]
    /// The stage configuration.
    pub stage: Option<Stage>,
    #[serde(default)]
    /// The test configuration.
    pub test: Option<Test>,

    #[serde(default)]
    /// Branch-specific checks.
    ///
    /// Checks which share the same name as the project override the project's check.
    pub checks: HashMap<String, CheckConfig>,

    #[serde(default)]
    /// Labels to add to issues which have been closed by merged merge requests.
    pub issue_labels: Vec<String>,
}

#[derive(Deserialize, Serialize, Debug)]
/// Configuration for a project.
pub struct Project {
    #[serde(default)]
    /// Project-wide checks.
    pub checks: HashMap<String, CheckConfig>,
    #[serde(default)]
    /// The data configuration for the project.
    pub data: Option<Data>,

    /// The name to use when committing to this project.
    pub name: String,
    /// The email to use when committing to this project.
    pub email: String,

    /// The branches which should be watched.
    pub branches: HashMap<String, Branch>,

    #[serde(default)]
    /// The submodule git directories for the project.
    pub submodules: HashMap<String, String>,

    #[serde(default)]
    /// Project maintainers.
    pub maintainers: Vec<String>,
}

#[derive(Deserialize, Serialize, Debug)]
/// Configuration for a host.
pub struct Host {
    /// The API the host uses for communication.
    pub host_api: String,
    /// The URL to the host.
    pub host_url: Option<String>,
    /// The list of maintainers on the host to notify when errors occur.
    pub maintainers: Vec<String>,
    /// The path to a JSON file containing private information to contact this host.
    pub secrets_path: String,
    /// The webhook URL to register for new projects.
    pub webhook_url: Option<String>,

    /// The projects to handle on this host.
    ///
    /// All events for other projects are ignored.
    pub projects: HashMap<String, Project>,
}

impl Host {
    /// The secrets object.
    pub fn secrets(&self) -> Result<Value> {
        let fin = File::open(&self.secrets_path)
            .chain_err(|| format!("failed to open secrets file '{}'",
                                  self.secrets_path))?;
        Ok(serde_json::from_reader(fin)
           .chain_err(|| format!("failed to read secrets from '{}'",
                                 self.secrets_path))?)
    }
}

#[derive(Deserialize, Serialize, Debug)]
/// Main configuration object for the robot.
pub struct Config {
    /// A scratch space directory for the host.
    pub workdir: String,

    /// Host configuration.
    pub hosts: HashMap<String, Host>,

    archive: Option<String>,
    queue: String,
}

impl Config {
    /// Read the configuration from a path.
    pub fn from_path<P: AsRef<Path>>(path: P) -> Result<Self> {
        File::open(path.as_ref())
            .chain_err(|| format!("failed to open config file '{}'",
                                  path.as_ref().display()))
            .and_then(|fin| {
                serde_yaml::from_reader(fin)
                    .chain_err(|| format!("failed to parse config from '{}'",
                                          path.as_ref().display()))
            })
            .and_then(|doc| {
                yaml_merge_keys::merge_keys_serde(doc)
                    .chain_err(|| format!("failed to handle merge keys from '{}'",
                                          path.as_ref().display()))
            })
            .and_then(|doc| {
                serde_yaml::from_value(doc)
                    .chain_err(|| format!("failed to construct configuration from '{}'",
                                          path.as_ref().display()))
            })
    }

    /// The directory to read for input data.
    pub fn queue_dir(&self) -> &Path {
        Path::new(&self.queue)
    }

    /// The directory to place processed job files.
    pub fn archive_dir(&self) -> &Path {
        Path::new(self.archive.as_ref().unwrap_or(&self.queue))
    }

    /// Verify all of the check configuration blocks in the configuration file.
    pub fn verify_all_check_configs(self) -> Result<()> {
        // Check the checks in each host.
        self.hosts
            .into_iter()
            .map(|(_, host)| {
                // And in each project.
                host.projects
                    .into_iter()
                    .map(|(_, project)| {
                        // First get the project-wide checks.
                        let project_checks = project.checks
                            .values()
                            .cloned()
                            .collect_vec();

                        project_checks.into_iter()
                            // And merge them with the branch-specific checks.
                            .chain(project.branches
                                .into_iter()
                                .map(|(_, branch)| {
                                    branch.checks
                                        .into_iter()
                                        .map(|(_, check)| {
                                            check
                                        })
                                })
                                .flatten())
                    })
                    .flatten()
            })
            .flatten()
            // Split the check configuration into an owned kind and configuration block.
            .map(|check_config| {
                (check_config.kind.clone(), check_config.config)
            })
            // Parse each check configuration section out.
            .map(|(check_kind, config): (Option<String>, Value)| {
                check_kind.map(|kind| {
                    let mut checks = Checks::default();
                    checks.add_check(&kind, config)
                })
            })
            // Skip any branch checks which are deleting a project-level check.
            .filter_map(|x| x)
            // Collect them up as results.
            .collect::<Result<Vec<_>>>()
            // And drop the actual check results.
            .map(|_| ())
    }
}
