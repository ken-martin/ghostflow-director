// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::ghostflow::host::HostingService;
use crates::ghostflow_gitlab::GitlabService;
use crates::gitlab::Gitlab;
use crates::gitlab::systemhooks::{GroupMemberSystemHook, ProjectMemberSystemHook, ProjectSystemHook};
use crates::gitlab::types::NoteType;
use crates::gitlab::webhooks::{MergeRequestHook, NoteHook, PushHook};
use crates::json_job_dispatch::{Director, Handler, HandlerResult, Result as JobResult};
use crates::serde::de::DeserializeOwned;
use crates::serde_json::{self, Value};

use config::Host;
use error::*;
use handlers::common::handlers::*;
use handlers::common::jobs::{BatchBranchJob, ClearTestRefs, TagStage, UpdateFollowRefs};
use handlers::gitlab::traits::*;

use std::sync::Arc;

/// Connect to a Gitlab from its configuration block.
pub fn connect_to_host(url: &Option<String>, secrets: Value) -> Result<Arc<HostingService>> {
    let host = url.as_ref().map_or("gitlab.com", |u| u.as_str());
    let token = if let Some(token) = secrets.pointer("/token").and_then(|t| t.as_str()) {
        token
    } else {
        bail!("gitlab requires a token to be provided");
    };
    let use_ssl = secrets.pointer("/use_ssl").and_then(Value::as_bool).unwrap_or(true);

    let ctor = if use_ssl {
        Gitlab::new
    } else {
        Gitlab::new_insecure
    };

    let gitlab =
        ctor(host, token).chain_err(|| format!("failed to construct service for host {}", host))?;
    Ok(GitlabService::new(gitlab).map(Arc::new)
        .chain_err(|| format!("failed to connect to host {}", host))?)
}

/// The handler for Gitlab events.
struct GitlabHandler {
    /// The host block for this handler.
    host: Host,
    /// The name to use for this handler.
    name: String,
}

impl GitlabHandler {
    /// Create a new handler.
    fn new(host: Host, name: &str) -> Self {
        GitlabHandler {
            host: host,
            name: name.to_string(),
        }
    }

    /// Parse an object into a type.
    fn parse_object<F, T>(object: &Value, callback: F) -> JobResult<HandlerResult>
        where T: DeserializeOwned,
              F: Fn(T) -> JobResult<HandlerResult>,
    {
        match serde_json::from_value::<T>(object.clone()) {
            Ok(hook) => callback(hook),
            Err(err) => Ok(HandlerResult::Fail(Box::new(err))),
        }
    }

    /// Handle a job.
    fn handle_kind(&self, kind: &str, object: &Value) -> JobResult<HandlerResult> {
        match kind {
            "merge_request" => {
                Self::parse_object(object, |hook: MergeRequestHook| {
                    GitlabMergeRequestInfo::from_web_hook(self.host.service.as_ref(),
                                                          &hook.object_attributes)
                        .map(|mr| handle_merge_request_update(object, &self.host, mr))
                        .unwrap_or_else(|err| Ok(HandlerResult::Fail(Box::new(err))))
                })
            },
            "note" => {
                Self::parse_object(object, |hook: NoteHook| {
                    let note_type = hook.object_attributes.noteable_type;
                    if let NoteType::MergeRequest = note_type {
                        GitlabMergeRequestNoteInfo::from_web_hook(self.host.service.as_ref(), &hook)
                            .map(|note| handle_merge_request_note(object, &self.host, note))
                            .unwrap_or_else(|err| Ok(HandlerResult::Fail(Box::new(err))))
                    } else {
                        Ok(HandlerResult::Reject(format!("unhandled noteable type: {}",
                                                         note_type.as_str())))
                    }
                })
            },
            "push" => {
                Self::parse_object(object, |hook: PushHook| {
                    GitlabPushInfo::from_web_hook(self.host.service.as_ref(), hook)
                        .map(|push| handle_push(object, &self.host, push))
                        .unwrap_or_else(|err| Ok(HandlerResult::Fail(Box::new(err))))
                })
            },
            "project_create" => {
                Self::parse_object(object, |hook: ProjectSystemHook| {
                    self.host
                        .service
                        .repo_by_id(hook.project_id.value())
                        .map(|project| handle_project_creation(object, &self.host, &project))
                        .unwrap_or_else(|err| Ok(HandlerResult::Fail(Box::new(err))))
                })
            },
            "user_add_to_team" |
            "user_remove_from_team" => {
                Self::parse_object(object, |hook: ProjectMemberSystemHook| {
                    self.host
                        .service
                        .repo_by_id(hook.project_id.value())
                        .map(|project| {
                            handle_project_membership_refresh(object, &self.host, &project)
                        })
                        .unwrap_or_else(|err| Ok(HandlerResult::Fail(Box::new(err))))
                })
            },
            "user_add_to_group" |
            "user_remove_from_group" => {
                Self::parse_object(object, |hook: GroupMemberSystemHook| {
                    handle_group_membership_refresh(object, &self.host, &hook.group_name)
                })
            },
            "clear_test_refs" => {
                Self::parse_object(object, |data: BatchBranchJob<ClearTestRefs>| {
                    Ok(handle_clear_test_refs(object, &self.host, data))
                })
            },
            "tag_stage" => {
                Self::parse_object(object, |data: BatchBranchJob<TagStage>| {
                    Ok(handle_stage_tag(object, &self.host, data))
                })
            },
            "update_follow_refs" => {
                Self::parse_object(object, |data: BatchBranchJob<UpdateFollowRefs>| {
                    Ok(handle_update_follow_refs(object, &self.host, data))
                })
            },
            _ => Ok(HandlerResult::Reject(format!("unhandled kind: {}", kind))),
        }
    }
}

impl Handler for GitlabHandler {
    fn add_to_director<'a>(&'a self, director: &mut Director<'a>) -> JobResult<()> {
        let mut add_handler = |kind| director.add_handler(&format!("{}:{}", self.name, kind), self);

        add_handler("merge_request")?;
        add_handler("note")?;
        add_handler("push")?;
        add_handler("project_create")?;
        add_handler("user_add_to_team")?;
        add_handler("user_remove_from_team")?;
        add_handler("user_add_to_group")?;
        add_handler("user_remove_from_group")?;

        add_handler("clear_test_refs")?;
        add_handler("tag_stage")?;
        add_handler("update_follow_refs")?;

        Ok(())
    }

    fn handle(&self, kind: &str, object: &Value) -> JobResult<HandlerResult> {
        let mut split = kind.split(':');

        if let Some(level) = split.next() {
            if level != self.name {
                return Ok(HandlerResult::Reject(format!("handler mismatch: {}", level)));
            }
        } else {
            return Ok(HandlerResult::Reject("handler mismatch".to_string()));
        }

        if let Some(kind) = split.next() {
            self.handle_kind(kind, object)
        } else {
            Ok(HandlerResult::Reject("missing kind".to_string()))
        }
    }
}

/// Create a handler for Gitlab jobs.
pub fn create_handler(host: Host, name: &str) -> Result<Box<Handler>> {
    Ok(Box::new(GitlabHandler::new(host, name)))
}
