// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::chrono::Utc;
use crates::ghostflow::host::{Comment, Commit, HostingService, Result};
use crates::git_workarea::CommitId;
use crates::gitlab::MergeRequestState;
use crates::gitlab::webhooks::{MergeRequestAction, MergeRequestHookAttrs, NoteHook, PushHook};

use handlers::common::data::*;

/// Information for a Gitlab merge request.
pub struct GitlabMergeRequestInfo;

impl GitlabMergeRequestInfo {
    /// Create a merge request information block from a webhook.
    pub fn from_web_hook(service: &HostingService, hook: &MergeRequestHookAttrs)
                         -> Result<MergeRequestInfo> {
        let source_repo = service.repo_by_id(hook.source_project_id.value())?;
        let is_open = match hook.state {
            MergeRequestState::Opened |
            MergeRequestState::Reopened => true,
            MergeRequestState::Closed |
            MergeRequestState::Merged |
            MergeRequestState::Locked => false,
        };
        let date = hook.updated_at;
        let mut mr = service.merge_request_by_id(hook.target_project_id.value(), hook.iid.value())?;
        mr.old_commit = if let Some(ref oldrev) = hook.oldrev {
            Some(Commit {
                repo: source_repo.clone(),
                refname: Some(hook.source_branch.clone()),
                id: CommitId::new(oldrev.value()),
            })
        } else {
            None
        };

        Ok(MergeRequestInfo {
            merge_request: mr,
            was_merged: hook.action == Some(MergeRequestAction::Merge),
            is_open: is_open,
            date: *date.as_ref(),
        })
    }
}

/// Information for a Gitlab merge request comment.
pub struct GitlabMergeRequestNoteInfo;

impl GitlabMergeRequestNoteInfo {
    /// Create a merge request comment information block from a webhook.
    pub fn from_web_hook(service: &HostingService, hook: &NoteHook)
                         -> Result<MergeRequestNoteInfo> {
        let merge_request = hook.merge_request
            .as_ref()
            .ok_or_else(|| "merge request is missing from note")?;
        let note = Comment {
            id: hook.object_attributes.id.value(),
            is_system: hook.object_attributes.system,
            is_branch_update: false,
            created_at: *hook.object_attributes.created_at.as_ref(),
            author: service.user_by_id(hook.object_attributes.author_id.value())?,
            // Remove ZERO WIDTH SPACE from the command. This can occur when copy/pasting contents
            // from an email or rendering of a command into a comment box. Due to its invisibility,
            // it causes confusion when the resulting `not recognized at all` text appears with the
            // invisible character hidden in there.
            content: hook.object_attributes.note.replace('\u{200b}', ""),
        };

        GitlabMergeRequestInfo::from_web_hook(service, merge_request).map(|mr_info| {
            MergeRequestNoteInfo {
                merge_request: mr_info,
                note: note,
            }
        })
    }
}

/// Information for a Gitlab push.
pub struct GitlabPushInfo;

impl GitlabPushInfo {
    /// Create a push information block from a webhook.
    pub fn from_web_hook(service: &HostingService, hook: PushHook) -> Result<PushInfo> {
        let date = if let Some(commit) = hook.commits.last() {
            commit.timestamp
        } else {
            Utc::now()
        };

        Ok(PushInfo {
            commit: Commit {
                repo: service.repo_by_id(hook.project_id.value())?,
                refname: Some(hook.ref_),
                id: CommitId::new(hook.after.value()),
            },
            author: service.user_by_id(hook.user_id.value())?,
            date: date,
        })
    }
}
