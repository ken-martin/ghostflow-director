// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Handlers for jobs for webhooks from hosting services.

use crates::ghostflow::host::HostingService;
use crates::json_job_dispatch::Handler;
use crates::serde_json::Value;

pub mod common;
mod gitlab;

use config::Host;
use error::*;

use std::sync::Arc;

/// Connect to a `HostingService`.
pub fn connect_to_host(api: &str, url: &Option<String>, secrets: Value)
                       -> Result<Arc<HostingService>> {
    match api {
        "gitlab" => gitlab::connect_to_host(url, secrets),
        _ => Err(format!("unknown api: {}", api).into()),
    }
}

/// Create handlers for a host configuration.
pub fn create_handler(host: Host, name: &str) -> Result<Box<Handler>> {
    match host.api.as_str() {
        "gitlab" => gitlab::create_handler(host, name),
        _ => Err(format!("unknown api: {}", host.api).into()),
    }
}
