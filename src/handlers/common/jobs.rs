// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Job data
//!
//! These structures are used to support common jobs that all hosts may use.

use crates::ghostflow::actions::stage::TagStagePolicy;
use crates::itertools::Itertools;

use std::collections::hash_map::{self, HashMap};
use std::vec;

fn default_stage_nightly() -> String {
    "%Y/%m/%d".to_string()
}

fn default_follow_name() -> String {
    "nightly".to_string()
}

#[doc(hidden)]
#[derive(Deserialize, Debug, Clone, Copy)]
pub enum TagStagePolicyIO {
    #[serde(rename="keep_topics")]
    KeepTopics,
    #[serde(rename="clear_stage")]
    ClearStage,
}

impl Default for TagStagePolicyIO {
    fn default() -> Self {
        TagStagePolicyIO::ClearStage
    }
}

impl From<TagStagePolicyIO> for TagStagePolicy {
    fn from(policy: TagStagePolicyIO) -> Self {
        match policy {
            TagStagePolicyIO::KeepTopics => TagStagePolicy::KeepTopics,
            TagStagePolicyIO::ClearStage => TagStagePolicy::ClearStage,
        }
    }
}

#[derive(Deserialize, Debug)]
/// Data required to tag a staging branch.
pub struct TagStage {
    #[serde(default="default_stage_nightly")]
    /// A datetime format string to use for the refname of the tagged stage.
    pub ref_date_format: String,
    /// The reason we are tagging the stage.
    pub reason: String,
    #[serde(default)]
    /// If false, the stage will be cleared every time the stage branch is published.
    pub policy: TagStagePolicyIO,
}

#[derive(Deserialize, Debug, Default, Clone, Copy)]
/// Data required to clear test refs for a project.
pub struct ClearTestRefs;

#[derive(Deserialize, Debug)]
/// Update any follow refs.
pub struct UpdateFollowRefs {
    #[serde(default="default_follow_name")]
    /// A datetime format string to use for the refname of the tagged stage.
    pub name: String,
}

#[derive(Deserialize, Debug)]
// Data required to run batch jobs on branches.
struct BatchProject<T>(HashMap<String, T>);

#[derive(Deserialize, Debug)]
/// Data required to run batch jobs on many branches of projects.
pub struct BatchBranchJob<T>(HashMap<String, BatchProject<T>>);

// TODO: Use `impl Iterator<Item=(String, String, T)>` here.
impl<T> BatchBranchJob<T> {
    /// Iterate over all of the branches.
    pub fn into_iter_branch(self) -> vec::IntoIter<(String, String, T)> {
        self.0
            .into_iter()
            .map(|(project_name, batch_project)| {
                batch_project.0
                    .into_iter()
                    .map(|(branch_name, data)| (project_name.clone(), branch_name, data))
                    .collect::<Vec<_>>()
                    .into_iter()
            })
            .flatten()
            .collect::<Vec<_>>()
            .into_iter()
    }
}

#[derive(Deserialize, Debug)]
/// Data required to run batch jobs on projects.
pub struct BatchProjectJob<T>(HashMap<String, T>);

// TODO: Use `impl Iterator<Item=(String, T)>` here.
impl<T> BatchProjectJob<T> {
    /// Iterate over all of the projects.
    pub fn into_iter_project(self) -> hash_map::IntoIter<String, T> {
        self.0.into_iter()
    }
}
