// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Common data structures required for the workflow.
//!
//! This module contains structures necessary to carry out the actual actions wanted for the
//! requested branches.

use crates::chrono::{DateTime, Utc};
use crates::ghostflow::actions::check;
use crates::ghostflow::host::{CheckStatus, Comment, Commit, HostingService, Membership, MergeRequest,
                              Repo, User};
use crates::ghostflow::utils::TrailerRef;
use crates::git_workarea::{CommitId, ErrorKind, GitContext, Result, ResultExt};

use std::collections::hash_map::HashMap;
use std::iter;

/// Information required to handle a push event.
#[derive(Debug, Clone)]
pub struct PushInfo {
    /// The commit being pushed.
    pub commit: Commit,
    /// The user who pushed the commit.
    pub author: User,
    /// When the push occurred.
    pub date: DateTime<Utc>,
}

/// Information required to handle a merge request update.
#[derive(Debug, Clone)]
pub struct MergeRequestInfo {
    /// The merge request information.
    pub merge_request: MergeRequest,
    /// Indicates whether the MR was just merged or not.
    pub was_merged: bool,
    /// Whether the merge request is open or not.
    pub is_open: bool,
    /// When the merge request was last updated.
    pub date: DateTime<Utc>,
}

/// Information about backporting a merge request into multiple branches.
pub struct BackportInfo {
    /// The branch to backport into.
    branch: String,
    /// The commit to backport.
    commit: Option<CommitId>,
}

impl BackportInfo {
    /// Backport information for the main target of a merge request.
    pub fn main_target<B>(branch: B) -> Self
        where B: ToString,
    {
        BackportInfo {
            branch: branch.to_string(),
            commit: None,
        }
    }

    /// Parse backport information from a string description.
    ///
    /// The format is `branch[:commit]`. Without the `commit` part, the last commit of the merge
    /// request's source topic is implied.
    pub fn from_str<S>(value: S) -> Self
        where S: AsRef<str>,
    {
        let value = value.as_ref();
        let (branch, commit) = match value.bytes().position(|c| c == b':') {
            Some(loc) => {
                let (branch, commit_part) = value.split_at(loc);
                let (_, commit) = commit_part.split_at(1);
                (branch, Some(commit))
            },
            None => (value, None),
        };

        BackportInfo {
            branch: branch.to_string(),
            commit: commit.map(CommitId::new),
        }
    }

    /// The name of the targeted backport branch.
    pub fn branch(&self) -> &str {
        &self.branch
    }

    /// The commit to backport.
    pub fn commit(&self, mr: &MergeRequest, ctx: &GitContext) -> Result<CommitId> {
        self.commit
            .as_ref()
            .map(|commit_id| {
                let rev = commit_id.as_str().replace("HEAD", mr.commit.id.as_str());
                let rev_parse = ctx.git()
                    .arg("rev-parse")
                    .arg("--verify")
                    .arg(format!("{}^{{commit}}", rev))
                    .output()
                    .chain_err(|| "failed to construct rev-parse command")?;
                if !rev_parse.status.success() {
                    bail!(ErrorKind::Git(format!("failed to parse the commit {}: {}",
                                                 rev,
                                                 String::from_utf8_lossy(&rev_parse.stderr))));
                }
                let commit_id = String::from_utf8_lossy(&rev_parse.stdout);
                Ok(CommitId::new(commit_id.trim()))
            })
            .unwrap_or_else(|| Ok(mr.commit.id.clone()))
    }
}

impl MergeRequestInfo {
    fn trailers(&self) -> Vec<TrailerRef> {
        TrailerRef::extract(&self.merge_request.description)
    }

    /// Returns `true` if it appears that the source branch has been deleted.
    pub fn is_source_branch_deleted(&self) -> bool {
        self.merge_request.commit.id.as_str().is_empty()
    }

    /// The name of the topic for the merge request according to its description.
    pub fn topic_rename(&self) -> Option<&str> {
        self.trailers()
            .into_iter()
            .filter_map(|trailer| {
                if trailer.token == "Topic-rename" {
                    Some(trailer.value)
                } else {
                    None
                }
            })
            .next()
    }

    /// The name of the topic for the merge request for use in actions.
    pub fn topic_name(&self) -> &str {
        self.topic_rename()
            .unwrap_or(&self.merge_request.source_branch)
    }

    /// Backport information for the merge request.
    ///
    /// Backport information is stored in the description of the merge request using `Backport`
    /// trailers.
    pub fn backports(&self) -> Vec<BackportInfo> {
        self.trailers()
            .into_iter()
            .filter_map(|trailer| {
                if trailer.token == "Backport" {
                    Some(BackportInfo::from_str(trailer.value))
                } else {
                    None
                }
            })
            .collect()
    }

    /// Determine the check status of a merge request.
    ///
    /// This will check the status messages for each target branch according to the backport
    /// information to ensure that it has been checked according to the backport information
    /// available for the merge request.
    pub fn check_status(&self, service: &HostingService, ctx: &GitContext) -> Result<CheckStatus> {
        let service_user_id = service.service_user().id;
        let status_map = service.get_commit_statuses(&self.merge_request.commit)
            .chain_err(|| ErrorKind::Msg(format!("failed to fetch commit statuses for the mr")))?
            .into_iter()
            // Only look at statuses posted by the current user.
            .filter(|status| status.author.id == service_user_id)
            // Make a mapping from status name to status.
            .map(|status| (status.name.clone(), status))
            .collect::<HashMap<_, _>>();
        let check_statuses = self.backports()
            .into_iter()
            .chain(iter::once(BackportInfo::main_target(&self.merge_request.target_branch)))
            .map(|backport| {
                // The name of the status we should expect.
                let status_name = check::Check::status_name(&backport.branch);
                // The commit that we expect to have been checked.
                let expected_commit = backport.commit(&self.merge_request, ctx)?;
                Ok(status_map.get(&status_name)
                    .and_then(|status| {
                        // Extract the trailers from the status description.
                        TrailerRef::extract(&status.description)
                            .into_iter()
                            .filter_map(|trailer| {
                                if trailer.token == "Branch-at" {
                                    // We have the commit that we checked.
                                    Some((status, trailer.value))
                                } else {
                                    None
                                }
                            })
                            .next()
                    })
                    .and_then(|(status, branch_at)| {
                        if expected_commit.as_str() == branch_at {
                            // We checked the same commit we have.
                            Some(status.state.into())
                        } else {
                            None
                        }
                    })
                    // If we don't have a state, we haven't performed the check as intended.
                    .unwrap_or(CheckStatus::Unchecked))
            })
            .collect::<Result<Vec<_>>>()?;

        Ok(if check_statuses.iter().all(CheckStatus::is_ok) {
            CheckStatus::Pass
        } else if check_statuses.iter().all(CheckStatus::is_checked) {
            CheckStatus::Fail
        } else {
            CheckStatus::Unchecked
        })
    }
}

/// Information required to handle a note on a merge request.
#[derive(Debug, Clone)]
pub struct MergeRequestNoteInfo {
    /// The merge request.
    pub merge_request: MergeRequestInfo,
    /// The note.
    pub note: Comment,
}

/// Information required to handle the addition of a user to a project.
#[derive(Debug, Clone)]
pub struct MembershipAdditionInfo {
    /// The repository.
    pub repo: Repo,
    /// The membership.
    pub membership: Membership,
}

/// Information required to handle the removal of a user from a project.
#[derive(Debug, Clone)]
pub struct MembershipRemovalInfo {
    /// The repository.
    pub repo: Repo,
    /// The user.
    pub user: User,
}

#[cfg(test)]
mod test {
    extern crate ghostflow;
    use self::ghostflow::host::{Commit, MergeRequest, Repo, User};

    extern crate git_workarea;
    use self::git_workarea::{CommitId, ErrorKind, GitContext};

    use handlers::common::data::BackportInfo;

    static COMMIT: &'static str = "4ab2a22a300d1425c5033a6429a44d17bb639a11";

    fn commit_context() -> (MergeRequest, GitContext) {
        let git_path = concat!(env!("CARGO_MANIFEST_DIR"), "/.git");
        let repo = Repo {
            name: "self".to_string(),
            url: git_path.to_string(),
            id: 1,
            forked_from: None,
        };
        let user = User {
            id: 0,
            handle: "user".to_string(),
            name: "user".to_string(),
            email: "user@example.com".to_string(),
        };
        let mr = MergeRequest {
            source_repo: repo.clone(),
            source_branch: "topic".to_string(),
            target_repo: repo.clone(),
            target_branch: "master".to_string(),
            id: 1,
            url: git_path.to_string(),
            work_in_progress: false,
            description: "MR description".to_string(),
            old_commit: None,
            commit: Commit {
                repo: repo.clone(),
                id: CommitId::new(COMMIT),
                refname: None,
            },
            author: user.clone(),

            reference: "!1".to_string(),
            remove_source_branch: false,
        };
        let ctx = GitContext::new(git_path);

        (mr, ctx)
    }

    fn parse_backport(value: &str) -> BackportInfo {
        BackportInfo::from_str(value)
    }

    #[test]
    fn test_backport_branch() {
        let backport = parse_backport("target_branch");
        assert_eq!(backport.branch, "target_branch");
        assert_eq!(backport.commit, None);
    }

    #[test]
    fn test_backport_commit_spec() {
        let backport = parse_backport("target_branch:HEAD");
        assert_eq!(backport.branch, "target_branch");
        assert_eq!(backport.commit, Some(CommitId::new("HEAD")));
    }

    #[test]
    fn test_backport_commit_spec_colon() {
        let backport = parse_backport("target_branch:HEAD:with:colons");
        assert_eq!(backport.branch, "target_branch");
        assert_eq!(backport.commit, Some(CommitId::new("HEAD:with:colons")));
    }

    #[test]
    fn test_backport_commit_relative() {
        let backport = parse_backport("target_branch:HEAD^2");
        let (mr, ctx) = commit_context();

        let commit = backport.commit(&mr, &ctx).unwrap();
        assert_eq!(commit,
                   CommitId::new("6781ccf2e07a1c15f8abd922f4d8ae765b10b1c2"));
    }

    #[test]
    fn test_backport_commit_no_exist() {
        let backport = parse_backport("target_branch:deadbeefdeadbeefdeadbeefdeadbeefdeadbeef");
        let (mr, ctx) = commit_context();

        let err = backport.commit(&mr, &ctx).unwrap_err();

        if let ErrorKind::Git(ref msg) = *err.kind() {
            assert_eq!(msg,
                       "failed to parse the commit deadbeefdeadbeefdeadbeefdeadbeefdeadbeef: \
                        fatal: Needed a single revision\n");
        } else {
            panic!("unexpected error: {:?}", err);
        }
    }

    #[test]
    fn test_backport_commit_no_exist_relative() {
        let backport = parse_backport("target_branch:HEAD^3");
        let (mr, ctx) = commit_context();

        let err = backport.commit(&mr, &ctx).unwrap_err();

        if let ErrorKind::Git(ref msg) = *err.kind() {
            assert_eq!(msg,
                       "failed to parse the commit 4ab2a22a300d1425c5033a6429a44d17bb639a11^3: \
                        fatal: Needed a single revision\n");
        } else {
            panic!("unexpected error: {:?}", err);
        }
    }
}
