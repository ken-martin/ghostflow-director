// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::clap::{App, AppSettings, Arg};
use crates::either::Either;
use crates::ghostflow::actions::check;
use crates::ghostflow::actions::merge::MergeActionResult;
use crates::ghostflow::actions::test;
use crates::ghostflow::host::{AccessLevel, CheckStatus, HostingService, MergeRequest};
use crates::ghostflow::utils::TrailerRef;
use crates::git_workarea::{CommitId, GitContext};
use crates::itertools::Itertools;
use crates::json_job_dispatch::HandlerResult;
use crates::json_job_dispatch::Result as JobResult;
use crates::serde_json::Value;

use config::{Branch, Host, CheckAction, MergeAction, Project, ReformatAction, StageAction,
             StageUpdatePolicy, TestAction, TestBackend};
use handlers::common::data::{BackportInfo, MergeRequestInfo, MergeRequestNoteInfo};
use handlers::common::handlers::commands::Commands;
use handlers::common::handlers::error::Error;
use handlers::common::handlers::utils;

use std::iter;

/// Handle an update to a merge request.
pub fn handle_merge_request_update(data: &Value, host: &Host, mr: MergeRequestInfo)
                                   -> JobResult<HandlerResult> {
    let (project, branch) = try_action!(utils::get_branch(host,
                                                          &mr.merge_request.target_repo.name,
                                                          &mr.merge_request.target_branch));

    let mut messages = Vec::new();
    let mut errors = Vec::new();

    if let Some(test_action) = branch.test() {
        handle_test_on_update(data, test_action, &mr);
    }

    if mr.was_merged {
        handle_merged_merge_request(project, branch, &mr, &mut messages, &mut errors);
    }

    if !mr.is_open {
        if let Some(stage_action) = branch.stage() {
            handle_stage_on_close(host, stage_action, &mr);
        }

        return Ok(HandlerResult::Reject(format!("the merge request for the branch {} has been \
                                                 closed",
                                                mr.merge_request.source_branch)));
    }

    // TODO: If the MR has been edited to change the target branch, we may need to remove it from
    // other branches, but we don't have the old branch name :/ .

    if !mr.is_source_branch_deleted() {
        let fetch_res = host.service.fetch_mr(&project.context, &mr.merge_request);
        if let Err(err) = fetch_res {
            error!(target: "ghostflow-director/handler",
                   "failed to fetch from the {} repository: {:?}",
                   mr.merge_request.source_repo.url,
                   err);

            errors.push(format!("Failed to fetch from the repository: {}.", err));
        }

        let reserve_res = project.context
            .reserve_ref(&format!("mr/{}", mr.merge_request.id),
                         &mr.merge_request.commit.id);
        if let Err(err) = reserve_res {
            error!(target: "ghostflow-director/handler",
                   "failed to reserve ref {} for merge request {}: {:?}",
                   mr.merge_request.commit.id,
                   mr.merge_request.url,
                   err);

            errors.push(format!("Failed to reserve ref {} for the merge request: `{}`.",
                                mr.merge_request.commit.id,
                                err));
        }
    }

    let is_ok = if mr.is_source_branch_deleted() {
        // A merge request with a deleted source branch is never ok.
        false
    } else {
        match mr.check_status(host.service.as_ref(), &project.context) {
            Ok(status) =>  {
                if status.is_checked() {
                    status.is_ok()
                } else {
                    match check_mr(project, branch, &mr) {
                        Ok(b) => b,
                        Err(err) => {
                            error!(target: "ghostflow-director/handler",
                                   "failed to run checks for merge request {}: {:?}",
                                   mr.merge_request.url,
                                   err);

                            errors.push(format!("Failed to run the checks: `{}`.", err));

                            false
                        },
                    }
                }
            },
            Err(err) => {
                error!(target: "ghostflow-director/handler",
                       "failed to determine the check status for merge request {}: {:?}",
                       mr.merge_request.url,
                       err);

                errors.push(format!("Failed to determine the check status: `{}`.", err));

                false
            },
        }
    };
    let is_wip = mr.merge_request.work_in_progress;

    if let Some(stage_action) = branch.stage() {
        handle_stage_on_update(is_ok, is_wip, stage_action, &mr, &mut errors);
    }

    Ok(utils::handle_result(messages,
                            errors,
                            false,
                            |comment| host.service.post_mr_comment(&mr.merge_request, comment)))
}

fn handle_merged_merge_request(project: &Project, branch: &Branch, mr: &MergeRequestInfo,
                               messages: &mut Vec<String>, errors: &mut Vec<String>) {
    if mr.merge_request.remove_source_branch {
        remove_source_branch(&project.context, &mr.merge_request, messages);
    }

    if !branch.issue_labels.is_empty() {
        label_closed_issues(project.service.as_ref(),
                            &branch.issue_labels,
                            &mr.merge_request,
                            errors);
    }
}

fn remove_source_branch(ctx: &GitContext, mr: &MergeRequest, messages: &mut Vec<String>) {
    info!(target: "ghostflow-director/handler",
          "removing the source branch for {}",
          mr.url);

    let push_res = ctx.git()
        .arg("push")
        .arg("--atomic")
        .arg("--porcelain")
        .arg(format!("--force-with-lease=refs/heads/{}:{}",
                     mr.source_branch,
                     mr.commit.id))
        .arg(&mr.source_repo.url)
        .arg(format!(":refs/heads/{}", mr.source_branch))
        .output();
    match push_res {
        Ok(push) => {
            if !push.status.success() {
                warn!(target: "ghostflow-director/handler",
                      "failed to remove the source branch of {} from the remote server: {}",
                      mr.url,
                      String::from_utf8_lossy(&push.stderr));

                messages.push("Failed to delete the source branch as requested. Access may need \
                               to be granted in order to allow for branch deletion.".to_string());
            }
        },
        Err(err) => {
            error!(target: "ghostflow-director/handler",
                   "failed to construct push command: {:?}",
                   err);
        },
    }
}

fn label_closed_issues(service: &HostingService, labels: &[String], mr: &MergeRequest,
                       errors: &mut Vec<String>) {
    info!(target: "ghostflow-director/handler",
          "labeling issues closed by {}",
          mr.url);

    let closed_issues = service.issues_closed_by_mr(mr)
        .unwrap_or_else(|err| {
            error!(target: "ghostflow-director/handler",
                   "failed to fetch issues closed by MR {}: {:?}",
                   mr.url,
                   err);

            errors.push("Failed to fetch the issues that this merge request closes.".to_string());

            Vec::new()
        });

    let labels = labels.iter()
        .map(AsRef::as_ref)
        .collect::<Vec<_>>();

    let failed_issues = closed_issues.into_iter()
        .filter_map(|issue| {
            if let Err(err) = service.add_issue_labels(&issue, &labels) {
                error!(target: "ghostflow-director/handler",
                       "failed to add labels to issue {}: {:?}",
                       issue.url,
                       err);

                Some(issue.reference)
            } else {
                None
            }
        })
        .join(", ");

    if !failed_issues.is_empty() {
        errors.push(format!("Failed to label issues {} with `{}`.",
                            failed_issues,
                            labels.iter().format("`, `")));
    }
}

/// Handle the test action when a merge request is updated.
fn handle_test_on_update(data: &Value, action: &TestAction, mr: &MergeRequestInfo) {
    match *action.test() {
        TestBackend::Jobs { action: ref test_jobs, .. } => {
            let job_data = utils::test_job("mr_update", data);
            if let Err(err) = test_jobs.test_update(job_data) {
                error!(target: "ghostflow-director/handler",
                        "failed to drop a job file for an update to {}: {:?}",
                        mr.merge_request.url,
                        err);
            }
        },
        TestBackend::Refs(ref test_refs) => {
            if !mr.is_open {
                if let Err(err) = test_refs.untest_mr(&mr.merge_request) {
                    error!(target: "ghostflow-director/handler",
                            "failed to remove the test ref for an update to {}: {:?}",
                            mr.merge_request.url,
                            err);
                }
            }

            // TODO: Anything else here? Forcefully untest the MR?
        },
    }
}

/// Handle removal of a merge request from the stage when it closes.
fn handle_stage_on_close(host: &Host, action: &StageAction, mr: &MergeRequestInfo) {
    let res = action.stage()
        .unstage_update_merge_request(&mr.merge_request, "because it has been closed");

    if let Err(err) = res {
        error!(target: "ghostflow-director/handler",
                "failed to unstage a closed merge request {}: {:?}",
                mr.merge_request.url,
                err);

        let errors = vec![
            format!("Error occurred when unstaging a closed merge request ({}): `{}`.",
                    host.maintainers.join(" "),
                    err),
        ];
        utils::handle_result(Vec::new(),
                             errors,
                             false,
                             |comment| host.service.post_mr_comment(&mr.merge_request, comment));
    }
}

/// Handle the stage policy when a merge request is updated.
fn handle_stage_on_update(is_ok: bool, is_wip: bool, action: &StageAction, mr: &MergeRequestInfo,
                          errors: &mut Vec<String>) {
    let mut stage = action.stage();

    let (policy, reason) = if !is_ok {
        (StageUpdatePolicy::Unstage, "since it is failing its checks")
    } else if is_wip {
        (StageUpdatePolicy::Unstage, "since it is marked as work-in-progress")
    } else if let Some(staged_topic) =
        stage.stager().find_topic_by_id(mr.merge_request.id) {
        if staged_topic.topic.commit == mr.merge_request.commit.id {
            (StageUpdatePolicy::Ignore, "it is already on the stage as-is")
        } else {
            (action.policy, "as per policy")
        }
    } else {
        (StageUpdatePolicy::Ignore, "it is not on the stage currently")
    };

    let (what, res) = match policy {
        StageUpdatePolicy::Ignore => ("ignore", Ok(())),
        StageUpdatePolicy::Restage => {
            ("restage",
             stage.stage_merge_request_named(&mr.merge_request,
                                             mr.topic_name(),
                                             &mr.merge_request.author.identity(),
                                             mr.date))
        },
        StageUpdatePolicy::Unstage => {
            ("unstage", stage.unstage_update_merge_request(&mr.merge_request, reason))
        },
    };

    if let Err(err) = res {
        error!(target: "ghostflow-director/handler",
                "failed to {} merge request {}: {:?}",
                what,
                mr.merge_request.url,
                err);

        errors.push(format!("Failed to {}: `{}`.", what, err));
    }
}

/// Check a merge request against the target branch's checks.
fn check_mr(project: &Project, branch: &Branch, mr: &MergeRequestInfo) -> check::Result<bool> {
    let mut errors = Vec::new();

    let result = mr.backports()
        .into_iter()
        .chain(iter::once(BackportInfo::main_target(&branch.name)))
        .map(|backport| {
            let branch = if let Some(branch) = project.branches.get(backport.branch()) {
                branch
            } else {
                errors.push(format!("the {} target branch is not watched",
                                    backport.branch()));
                return Ok(check::CheckStatus::Fail);
            };
            let commit = match backport.commit(&mr.merge_request, &project.context) {
                Ok(commit) => commit,
                Err(err) => {
                    error!(target: "ghostflow-director/handler",
                           "failed to determine the commit to backport into {}: {:?}",
                           backport.branch(),
                           err);

                    errors.push(format!("failed to determine the commit to backport into {}: {}",
                                        backport.branch(),
                                        err));
                    return Ok(check::CheckStatus::Fail);
                },
            };
            project.check_mr_with(branch, &mr.merge_request, &commit)
        })
        // Collect all of the results.
        .collect::<check::Result<Vec<_>>>()
        // Ensure that all of them passed.
        .map(|results| results.into_iter().all(|status| status == check::CheckStatus::Pass));

    if !errors.is_empty() {
        let comment = format!("The backport configuration is not valid:\n\
                               \n  - {}",
                              errors.into_iter().join("\n  - "));
        if let Err(err) = project.service.post_mr_comment(&mr.merge_request, &comment) {
            error!(target: "ghostflow-director/handler",
                   "failed to post backport configuration failure comment: {:?}",
                   err);
        }
    }

    result
}

/// Handle a note on a merge request.
pub fn handle_merge_request_note(data: &Value, host: &Host, mr_note: MergeRequestNoteInfo)
                                 -> JobResult<HandlerResult> {
    let mr = &mr_note.merge_request;
    let note = &mr_note.note;
    let award_name = host.service.comment_award_name();

    if !mr.is_open {
        return Ok(HandlerResult::Reject(format!("the merge request for the branch {} has been \
                                                 closed",
                                                mr.merge_request.source_branch)));
    }

    let already_handled = match host.service.get_mr_comment_awards(&mr.merge_request, note) {
        Ok(awards) => {
            awards.into_iter()
                .any(|award| {
                    award.name == award_name && award.author.id == host.service.service_user().id
                })
        },
        Err(err) => {
            error!(target: "ghostflow-director/handler",
                   "failed to get the awards for a note at {}: {:?}",
                   mr.merge_request.url,
                   err);

            false
        },
    };

    if already_handled {
        return Ok(HandlerResult::Reject("this note has already been handled".to_string()));
    }

    let (project, branch) = try_action!(utils::get_branch(host,
                                                          &mr.merge_request.target_repo.name,
                                                          &mr.merge_request.target_branch));

    let mut commands = Commands::new(branch);

    let trailers = TrailerRef::extract(&mr_note.note.content);

    if trailers.is_empty() {
        return Ok(HandlerResult::Reject("no trailers present".to_string()));
    }

    let access = project.access_level(&note.author);
    let is_submitter = mr.merge_request.author.id == note.author.id;
    commands.add_from_trailers(access, is_submitter, &trailers);

    if commands.is_empty() {
        return Ok(HandlerResult::Reject("no commands present".to_string()));
    }

    let fetch_res = host.service.fetch_mr(&project.context, &mr.merge_request);
    if let Err(err) = fetch_res {
        return Ok(HandlerResult::Fail(Error::chain(err,
                                                   format!("failed to fetch source for {}",
                                                           mr.merge_request.url))));
    }

    let mut messages = Vec::new();
    let mut errors = commands.error_messages(&note.author);

    let defer = if errors.is_empty() {
        let mut command_data = CommandData {
            data: data,
            host: host,
            project: project,
            branch: branch,
            mr_note: &mr_note,
            messages: &mut messages,
            errors: &mut errors,
            access: access,
            is_submitter: is_submitter,
        };

        handle_commands(&mut command_data, &commands)
    } else {
        false
    };

    if !defer {
        // Award the command comment to indicate that we handled it.
        if let Err(err) = host.service.award_mr_comment(&mr.merge_request, note, award_name) {
            error!(target: "ghostflow-director/handler",
                   "failed to mark the comment on {} as seen: {:?}",
                   mr.merge_request.url,
                   err);

            errors.push(format!("Failed to mark the comment as seen: `{}`.", err));
        }
    }

    Ok(utils::handle_result(messages,
                            errors,
                            defer,
                            |comment| host.service.post_mr_comment(&mr.merge_request, comment)))
}

struct CommandData<'a> {
    data: &'a Value,
    host: &'a Host,
    project: &'a Project,
    branch: &'a Branch,
    mr_note: &'a MergeRequestNoteInfo,
    messages: &'a mut Vec<String>,
    errors: &'a mut Vec<String>,
    access: AccessLevel,
    is_submitter: bool,
}

enum CommandResult {
    Continue,
    Defer,
    Stop,
}

macro_rules! try_command {
    ( $action:expr ) => {
        match $action {
            Either::Right(res) => res,
            Either::Left(command_result) => return command_result,
        }
    }
}

impl<'a> CommandData<'a> {
    fn check_status(&mut self) -> Either<CommandResult, CheckStatus> {
        let mr = &self.mr_note.merge_request;
        match mr.check_status(self.host.service.as_ref(), &self.project.context) {
            Ok(status) => Either::Right(status),
            Err(err) => {
                error!(target: "ghostflow-director/handler",
                       "failed to determine the check status of {}: {:?}",
                       mr.merge_request.url,
                       err);

                self.errors.push(format!("Failed to determine the check status: `{}`.", err));

                Either::Left(CommandResult::Stop)
            },
        }
    }
}

macro_rules! handle_command_requests {
    ( $defer:ident, $stop:ident, $commands:ident, $command_data:expr,
      $( $action:ident -> $action_member:ident => $action_impl:ident, )* ) => {
        $( if !$stop && $commands.$action.requested() {
            $command_data.branch
                .$action_member()
                .as_ref()
                .map(|action| {
                    let res = $action_impl($command_data,
                                           action,
                                           $commands.$action.arguments());

                    match res {
                        CommandResult::Continue => (),
                        CommandResult::Defer => $defer = true,
                        CommandResult::Stop => $stop = false,
                    }
                });
        } )*
    };
}

/// Handle commands in a merge request note.
fn handle_commands(command_data: &mut CommandData, commands: &Commands) -> bool {
    let mut stop = false;
    let mut defer = false;

    handle_command_requests!(defer, stop, commands, command_data,
        help -> help => handle_help_command,
        check -> check => handle_check_command,
        stage -> stage => handle_stage_command,
        unstage -> stage => handle_unstage_command,
        test -> test => handle_test_command,
        merge -> merge => handle_merge_command,
        reformat -> reformat => handle_reformat_command,
    );

    defer
}

fn command_app(name: &'static str) -> App<'static, 'static> {
    App::new(name)
        .setting(AppSettings::ColorNever)
        .setting(AppSettings::DisableVersion)
        .setting(AppSettings::NoBinaryName)
}

macro_rules! action_help {
    ( $all:ident, $command_data:expr, $action:expr, ($name:expr, $allow_submitter:expr), $help:expr ) => {
        {
            let level_allowed = $action.access_level <= $command_data.access;
            let submitter_allowed = $allow_submitter && $command_data.is_submitter;

            if $all || level_allowed || submitter_allowed {
                $command_data.messages.push($help($action));
            }
        }
    };

    ( $all:ident, $command_data:expr, $action:expr, $name:expr, $help:expr ) => {
        $command_data.messages.push($help($action));
    };
}

macro_rules! command_help {
    ( $all:ident, $command_data:expr,
      $( $action:ident -> $action_spec:tt => $help:expr, )* ) => {
        $( if let Some(action) = $command_data.branch.$action() {
            action_help!($all, $command_data, action, $action_spec, $help)
        } )*
    };
}

/// Handle the `Do: help` command.
fn handle_help_command(data: &mut CommandData, _: &(), arguments: &[String])
                       -> CommandResult {
    let matches = command_app("help-action")
        .arg(Arg::with_name("ALL")
            .short("a")
            .long("all"))
        .get_matches_from_safe(arguments);

    match matches {
        Ok(matches) => {
            let all = matches.is_present("ALL");

            command_help!(all, data,
                check -> "check" => |_| {
                    "The `check` action performs checks on the content and structure of the merge \
                     request according to the project's settings. Generally, all messages should \
                     be addressed. Those which mention a specific commit require that the commit \
                     itself be fixed. That is, an additional commit which fixes the issue will not \
                     address the problem with the original commit.".to_string()
                },
                stage -> ("stage", false) => |action: &StageAction| {
                    let policy_desc = match action.policy {
                        StageUpdatePolicy::Ignore => "left as-is on the stage",
                        StageUpdatePolicy::Restage => "automatically restaged",
                        StageUpdatePolicy::Unstage => "removed from the stage",
                    };

                    format!("The `stage` action takes the topic from the merge request and adds \
                             it to the \"stage\". This is a branch which contains all of the \
                             staged topics merged in on top of the target branch. Conflicts \
                             between topics on the stage are handled in a \"first-come, \
                             first-served\" fashion, so if a topic conflicts with another, \
                             waiting until it is either merged into the target branch or removed \
                             from the stage is best. When updating a merge request with new code, \
                             a topic that has already been staged is {}.",
                            policy_desc)
                },
                stage -> ("unstage", true) => |_| {
                    "The `unstage` action removes the topic from the stage.".to_string()
                },
                test -> ("test", false) => |action: &TestAction| {
                    match *action.test() {
                        TestBackend::Jobs { ref help, .. } => {
                            help.clone()
                        },
                        TestBackend::Refs(_) => {
                            "The `test` action creates a special reference on the repository \
                             which indicates that the merge request should be tested. Test \
                             machines look at these special references and run the tests. The \
                             `--stop` argument may be given to remove the special reference. The \
                             reference is also removed when the merge request is closed (for any \
                             reason). Updates to the merge request leave any existing test \
                             reference in place and require either a new `Do: test` to update it \
                             to the new code or a `Do: test --stop` to remove it.".to_string()
                        },
                    }.to_string()
                },
                merge -> ("merge", false) => |_| {
                    "The `merge` action merges the merge request into the target branch. It fails \
                     if the merge request has not passed the checks or it is considered a \
                     work-in-progress. The name of the topic merged into the target branch may be \
                     changed with either the `--topic` argument or using a `Topic-rename` trailer \
                     in the description.".to_string()
                },
                reformat -> ("reformat", true) => |_| {
                    "The `reformat` action rewrites the topic for the merge request according to \
                     the project coding guidelines. Every commit will have its content changed so \
                     that it follows the coding guidelines. Any commit which is empty after the \
                     reformatting will be dropped from the topic. After performing a reformat, \
                     the resulting topic is force-pushed as the source topic of the merge \
                     request. In order to use the reformatted branch, it must be fetched and \
                     updated locally, otherwise the reformatting will need to be \
                     redone.".to_string()
                },
            );
        },
        Err(err) => {
            data.errors.push(format!("Unrecognized `help` arguments: `{}`.",
                                     err.message.lines().next().unwrap()));
        },
    }

    CommandResult::Continue
}

/// Handle the `Do: check` command.
fn handle_check_command(data: &mut CommandData, _: &CheckAction, arguments: &[String])
                        -> CommandResult {
    let mr = &data.mr_note.merge_request;

    if arguments.is_empty() {
        match check_mr(data.project, data.branch, &mr) {
            Ok(true) => (),
            Ok(false) => return CommandResult::Stop,
            Err(err) => {
                error!(target: "ghostflow-director/handler",
                       "failed to run checks on {}: {:?}",
                       mr.merge_request.url,
                       err);

                data.errors.push(format!("failed to run checks: `{}`", err));
            },
        }
    } else {
        data.errors.push(format!("Unrecognized `check` arguments: `{}`.",
                                 arguments.iter().join("`, `")));
    }

    CommandResult::Continue
}

/// Handle the `Do: stage` command.
fn handle_stage_command(data: &mut CommandData, action: &StageAction, arguments: &[String])
                        -> CommandResult {
    let mr = &data.mr_note.merge_request;
    let note = &data.mr_note.note;
    let mut stage = action.stage();

    let check_status = try_command!(data.check_status());
    if !check_status.is_checked() {
        data.errors.push("Refusing to stage; topic is missing the checks.".to_string())
    } else if !check_status.is_ok() {
        data.errors.push("Refusing to stage; topic is failing the checks.".to_string())
    } else if arguments.is_empty() {
        let res = stage.stage_merge_request_named(&mr.merge_request,
                                                  mr.topic_name(),
                                                  &note.author.identity(),
                                                  note.created_at);

        if let Err(err) = res {
            error!(target: "ghostflow-director/handler",
                   "failed during the stage action on {}: {:?}",
                   mr.merge_request.url,
                   err);

            data.errors.push(format!("Error occurred during stage action ({}): `{}`",
                                     data.host.maintainers.join(" "),
                                     err));
        }
    } else {
        data.errors.push(format!("Unrecognized `stage` arguments: `{}`.",
                                 arguments.iter().join("`, `")));
    }

    CommandResult::Continue
}

/// Handle the `Do: unstage` command.
fn handle_unstage_command(data: &mut CommandData, action: &StageAction, arguments: &[String])
                          -> CommandResult {
    let mr = &data.mr_note.merge_request;
    let mut stage = action.stage();

    if arguments.is_empty() {
        let res = stage.unstage_merge_request(&mr.merge_request);

        if let Err(err) = res {
            error!(target: "ghostflow-director/handler",
                   "failed during the unstage action on {}: {:?}",
                   mr.merge_request.url,
                   err);

            data.errors.push(format!("Error occurred during unstage action ({}): `{}`",
                                     data.host.maintainers.join(" "),
                                     err));
        }
    } else {
        data.errors.push(format!("Unrecognized `unstage` arguments: `{}`.",
                                 arguments.iter().join("`, `")));
    }

    CommandResult::Continue
}

/// Handle the `Do: test` command.
fn handle_test_command(data: &mut CommandData, action: &TestAction, arguments: &[String])
                       -> CommandResult {
    let mr = &data.mr_note.merge_request;
    let test = action.test();

    let res: test::Result<_> = match *test {
        TestBackend::Jobs { action: ref test_jobs, .. } => {
            let args = Value::Array(arguments.iter()
                .map(|arg| Value::String(arg.clone()))
                .collect());
            let job_data = utils::test_job("test_action",
                                           &json!({
                "arguments": args,
                "merge_request": data.data.clone(),
            }));
            test_jobs.test_mr(&mr.merge_request, job_data)
                .map_err(Into::into)
        },
        TestBackend::Refs(ref test_refs) => {
            let matches = command_app("test-action")
                .arg(Arg::with_name("STOP")
                    .long("stop")
                    .takes_value(false))
                .get_matches_from_safe(arguments);

            match matches {
                Ok(matches) => {
                    let res = if matches.is_present("STOP") {
                        test_refs.untest_mr(&mr.merge_request)
                    } else {
                        test_refs.test_mr(&mr.merge_request)
                    };

                    res.map_err(Into::into)
                },
                Err(err) => {
                    data.errors.push(format!("Unrecognized `merge` arguments: `{}`.",
                                             err.message.lines().next().unwrap()));

                    Ok(())
                },
            }
        },
    };

    if let Err(err) = res {
        data.errors.push(format!("Error occurred during test action ({}): `{:?}`",
                                 data.host.maintainers.join(" "),
                                 err));
    }

    CommandResult::Continue
}

/// Handle the `Do: merge` command.
fn handle_merge_command(data: &mut CommandData, action: &MergeAction, arguments: &[String])
                        -> CommandResult {
    let mr = &data.mr_note.merge_request;
    let note = &data.mr_note.note;

    let check_status = try_command!(data.check_status());
    if check_status.is_ok() {
        let matches = command_app("merge-action")
            .arg(Arg::with_name("TOPIC")
                .short("t")
                .long("topic")
                .takes_value(true))
            .get_matches_from_safe(arguments);

        match matches {
            Ok(matches) => {
                let merge = action.merge();

                let topic_name = matches.value_of("TOPIC")
                    .unwrap_or_else(|| mr.topic_name())
                    .to_string();

                if data.project.branch_names.contains(&topic_name) {
                    data.errors
                        .push(format!("The name of the topic (`{}`) is the same as a branch in the \
                                       project. Use the `--topic` option to merge with a different \
                                       name.",
                                      topic_name));
                    return CommandResult::Continue;
                }

                let res = merge.merge_mr_named(&mr.merge_request,
                                               topic_name,
                                               &note.author.identity(),
                                               note.created_at);

                match res {
                    Ok(MergeActionResult::Success) |
                    Ok(MergeActionResult::Failed) => (),
                    // Defer the action if merging failed.
                    Ok(MergeActionResult::PushFailed) => {
                        data.errors.push("Failed to push the resulting merge".to_string());
                        return CommandResult::Defer;
                    },
                    Err(err) => {
                        error!(target: "ghostflow-director/handler",
                               "failed during the merge action on {}: {:?}",
                               mr.merge_request.url,
                               err);

                        data.errors.push(format!("Error occurred during merge action ({}): \
                                                  `{}`",
                                                 data.host.maintainers.join(" "),
                                                 err));
                    },
                }
            },
            Err(err) => {
                data.errors.push(format!("Unrecognized `merge` arguments: `{}`.",
                                        err.message.lines().next().unwrap()));
            },
        }
    } else if check_status.is_checked() {
        data.errors.push("Refusing to merge; topic is failing the checks.".to_string())
    } else {
        data.errors.push("Refusing to merge; topic is missing the checks.".to_string())
    }

    CommandResult::Continue
}

/// Handle the `Do: reformat` command.
fn handle_reformat_command(data: &mut CommandData, action: &ReformatAction, arguments: &[String])
                           -> CommandResult {
    let mr = &data.mr_note.merge_request;
    let reformat = action.reformat();

    if arguments.is_empty() {
        let res = reformat.reformat_mr(&CommitId::new(&data.branch.name), &mr.merge_request);

        if let Err(err) = res {
            error!(target: "ghostflow-director/handler",
                   "failed during the reformat action on {}: {:?}",
                   mr.merge_request.url,
                   err);

            data.errors.push(format!("Error occurred during reformat action ({}): `{}`",
                                     data.host.maintainers.join(" "),
                                     err));
        }
    } else {
        data.errors.push(format!("Unrecognized `reformat` arguments: `{}`.",
                                 arguments.iter().join("`, `")));
    }

    CommandResult::Continue
}
