// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::ghostflow::host::Result as HostResult;
use crates::json_job_dispatch::HandlerResult;
use crates::serde_json::Value;

use config::{Branch, Host, Project};

macro_rules! try_action {
    ( $action:expr ) => {
        match $action {
            Ok(res) => res,
            Err(handler_result) => return Ok(handler_result),
        }
    }
}

/// Get the project from a host.
pub fn get_project<'a>(host: &'a Host, project_name: &str) -> Result<&'a Project, HandlerResult> {
    host.projects
        .get(project_name)
        .ok_or_else(|| HandlerResult::Reject(format!("unwatched project {}", project_name)))
}

/// Get the project and branch from a host.
pub fn get_branch<'a>(host: &'a Host, project_name: &str, branch_name: &str)
                      -> Result<(&'a Project, &'a Branch), HandlerResult> {
    get_project(host, project_name).and_then(|project| {
        project.branches
            .get(branch_name)
            .map(|branch| (project, branch))
            .ok_or_else(|| {
                HandlerResult::Reject(format!("unwatched branch {} for project {}",
                                              branch_name,
                                              project_name))
            })
    })
}

/// Handle a job result and commit if errors occurred.
pub fn handle_result<F>(messages: Vec<String>, errors: Vec<String>, defer: bool, post_comment: F)
                        -> HandlerResult
    where F: Fn(&str) -> HostResult<()>,
{
    let txt_comment = format!("messages:\n{}\n\nerrors:\n{}",
                              messages.join("\n"),
                              errors.join("\n"));

    let (comment, result) = if defer {
        info!(target: "ghostflow-director/handler",
              "Deferring a job: {}",
              txt_comment);

        (None, HandlerResult::Defer(txt_comment))
    } else if errors.is_empty() {
        let md_comment = if messages.is_empty() {
            None
        } else {
            Some(format!("Messages:\n\n  - {}",
                         messages.join("\n  - ")))
        };

        (md_comment, HandlerResult::Accept)
    } else {
        let md_comment = if messages.is_empty() {
            format!("Errors:\n\n  - {}",
                    errors.join("\n  - "))
        } else {
            format!("Messages:\n\n  - {}\n\nErrors: \n\n  - {}",
                    messages.join("\n  - "),
                    errors.join("\n  - "))
        };

        (Some(md_comment), HandlerResult::Reject(txt_comment))
    };

    if let Some(comment) = comment {
        if let Err(err) = post_comment(&comment) {
            error!(target: "ghostflow-director/handler",
                   "Failed to post comment:\n'{}'\n'{:?}'.",
                   comment, err);
        }
    }

    result
}

/// Create a test job.
pub fn test_job<K>(kind: K, data: &Value) -> Value
    where K: ToString,
{
    json!({
        "kind": kind.to_string(),
        "data": data.clone()
    })
}
