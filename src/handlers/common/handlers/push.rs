// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::json_job_dispatch::HandlerResult;
use crates::json_job_dispatch::Result as JobResult;
use crates::serde_json::Value;

use config::{Host, TestBackend};
use handlers::common::data::PushInfo;
use handlers::common::handlers::utils;

/// Handle a push to a repository.
pub fn handle_push(data: &Value, host: &Host, push: PushInfo) -> JobResult<HandlerResult> {
    if let Ok(root_project) = utils::get_project(host, &push.commit.repo.fork_root().name) {
        if let Some(ref data) = root_project.data {
            if let Err(err) = data.fetch_data(&push.commit.repo) {
                error!(target: "ghostflow-director/handler",
                       "failed to fetch data from {}: {:?}",
                       push.commit.repo.url,
                       err);
            }
        }
    }

    let refs_heads_prefix = "refs/heads/";
    let refname = if let Some(ref name) = push.commit.refname {
        if name.starts_with(refs_heads_prefix) {
            name
        } else {
            return Ok(HandlerResult::Reject(format!("non-branch ref push {}", name)));
        }
    } else {
        return Ok(HandlerResult::Reject("unknown refname".to_string()));
    };

    let branch_name = &refname[refs_heads_prefix.len()..];
    let (project, branch) =
        try_action!(utils::get_branch(host, &push.commit.repo.name, branch_name));

    // Garbage collect the repository.
    let gc = project.context
        .git()
        .arg("gc")
        .arg("--auto")
        .output();
    match gc {
        Ok(gc) => {
            if !gc.status.success() {
                error!(target: "ghostflow-director/handler",
                       "failed to perform garbage collection for {}: {:?}",
                       push.commit.repo.name,
                       String::from_utf8_lossy(&gc.stderr));
            }
        },
        Err(err) => {
            error!(target: "ghostflow-director/handler",
                   "failed to construct gc command: {:?}",
                   err);
        },
    }

    if let Some(test_action) = branch.test() {
        if test_action.test_updates {
            if let TestBackend::Jobs { action: ref test_jobs, .. } = *test_action.test() {
                let job_data = utils::test_job("branch_push", data);
                if let Err(err) = test_jobs.test_update(job_data) {
                    error!(target: "ghostflow-director/handler",
                           "failed to drop a job file for an update to the {} branch on {}: {:?}",
                           branch.name,
                           push.commit.repo.name,
                           err);
                }
            }
        }
    }

    let mut errors = Vec::new();

    let fetch_res = project.context.force_fetch_into("origin", refname, refname);
    if let Err(err) = fetch_res {
        error!(target: "ghostflow-director/handler",
               "failed to fetch from the {} repository: {:?}",
               push.commit.repo.url,
               err);

        errors.push(format!("Failed to fetch from the repository: {}.", err));
    }

    if let Some(stage_action) = branch.stage() {
        let mut stage = stage_action.stage();
        let res = stage.base_branch_update(&push.commit, &push.author.identity(), push.date);

        if let Err(err) = res {
            error!(target: "ghostflow-director/handler",
                   "failed update the base branch for the {} stage in the {} repository: {:?}",
                   branch.name,
                   push.commit.repo.url,
                   err);

            errors.push(format!("Error occurred when updating the base branch ({}): `{}`.",
                                host.maintainers.join(" "),
                                err));
        }
    }

    Ok(utils::handle_result(Vec::new(),
                            errors,
                            false,
                            |comment| host.service.post_commit_comment(&push.commit, comment)))
}
