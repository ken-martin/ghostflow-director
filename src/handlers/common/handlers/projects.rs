// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::ghostflow::host::Repo;
use crates::json_job_dispatch::HandlerResult;
use crates::json_job_dispatch::Result as JobResult;
use crates::serde_json::Value;

use config::Host;
use handlers::common::handlers::error::Error;

/// Handle the creation of a project on the service.
pub fn handle_project_creation(_: &Value, host: &Host, project: &Repo) -> JobResult<HandlerResult> {
    let hook_result = host.webhook_url
        .as_ref()
        .map(|url| {
            host.service
                .add_hook(&project.name, url)
                .map(|_| HandlerResult::Accept)
                .or_else(|err| {
                    Ok(HandlerResult::Fail(Error::chain(err,
                                                        format!("failed to add webhook for {}",
                                                                project.name))))
                })
        });

    hook_result.unwrap_or_else(|| Ok(HandlerResult::Reject("nothing to do".to_string())))
}
