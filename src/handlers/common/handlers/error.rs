// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::ghostflow;
use crates::git_topic_stage;
use crates::git_workarea;

use std::error;

error_chain! {
    links {
        GitWorkarea(git_workarea::Error, git_workarea::ErrorKind);
        Host(ghostflow::host::Error, ghostflow::host::ErrorKind);
        Stage(git_topic_stage::Error, git_topic_stage::ErrorKind);
    }
}

impl Error {
    /// Create an error from a string.
    pub fn from_string<M>(msg: M) -> Box<error::Error>
        where M: ToString,
    {
        let err: Error = ErrorKind::Msg(msg.to_string()).into();
        Box::new(err)
    }

    /// Chain an error into a handler error.
    pub fn chain<C, M>(cause: C, msg: M) -> Box<error::Error>
        where C: error::Error + Send + 'static,
              M: ToString,
    {
        let mut err: Error = ErrorKind::Msg(msg.to_string()).into();
        err.1.next_error = Some(Box::new(cause));
        Box::new(err)
    }
}
