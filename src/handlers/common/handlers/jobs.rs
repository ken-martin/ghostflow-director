// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::ghostflow::actions::test;
use crates::json_job_dispatch::HandlerResult;
use crates::serde_json::Value;

use config::{Branch, Host, TestBackend};
use handlers::common::handlers::error::Error;
use handlers::common::handlers::utils;
use handlers::common::jobs::{BatchBranchJob, ClearTestRefs, TagStage, UpdateFollowRefs};

use std::error;

/// Scan all branches detecting which support an action.
fn branch_job_scanner<A, F, T, E>(host: &Host, data: BatchBranchJob<T>, action_name: A,
                                  handler: F)
                                  -> HandlerResult
    where A: AsRef<str>,
          F: Fn(T, &Branch) -> Option<Result<(), E>>,
          E: error::Error + Send + 'static,
{
    data.into_iter_branch()
        .map(|(project_name, branch_name, job_data)| {
            utils::get_branch(host, &project_name, &branch_name)
                .map(|(_, branch)| (project_name, branch_name, handler(job_data, branch)))
        })
        .fold(HandlerResult::Accept, |result, branch_result| {
            let branch_handler_result = match branch_result {
                Ok((project, branch, action_result)) => {
                    match action_result {
                        Some(Ok(())) => HandlerResult::Accept,
                        Some(Err(err)) => {
                            HandlerResult::Fail(Error::chain(err,
                                                             format!("error on {}/{} in the {} \
                                                                      action",
                                                                     project,
                                                                     branch,
                                                                     action_name.as_ref())))
                        },
                        None => {
                            HandlerResult::Reject(format!("no {} action for {}/{}",
                                                          action_name.as_ref(),
                                                          project,
                                                          branch))
                        },
                    }
                },
                Err(err) => err,
            };

            result.combine(branch_handler_result)
        })
}

/// Handle requests to tag staging branches.
pub fn handle_stage_tag(_: &Value, host: &Host, data: BatchBranchJob<TagStage>) -> HandlerResult {
    branch_job_scanner(host, data, "stage", |tag_stage, branch| {
        branch.stage()
            .map(|stage| {
                stage.stage().tag_stage(&tag_stage.reason,
                                        &tag_stage.ref_date_format,
                                        tag_stage.policy.into())
            })
    })
}

/// Handle requests to clear test refs.
pub fn handle_clear_test_refs(_: &Value, host: &Host, data: BatchBranchJob<ClearTestRefs>)
                              -> HandlerResult {
    branch_job_scanner(host, data, "test", |_, branch| {
        branch.test()
            .map(|test| if let TestBackend::Refs(ref test_refs) = *test.test() {
                test_refs.clear_all_mrs()
            } else {
                Err(test::refs::ErrorKind::Msg("non-refs test backend".to_string()).into())
            })
    })
}

/// Handle requests to update follow refs.
pub fn handle_update_follow_refs(_: &Value, host: &Host, data: BatchBranchJob<UpdateFollowRefs>)
                                 -> HandlerResult {
    branch_job_scanner(host, data, "follow", |update_follow, branch| {
        branch.follow()
            .map(|follow| follow.follow().update(&update_follow.name))
    })
}
