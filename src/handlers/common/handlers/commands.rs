// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! Command parsing and checking.
//!
//! Comments may contain commands for the robot to perform. This handles parsing out those commands
//! from a set of trailers.

use crates::ghostflow::host::{AccessLevel, User};
use crates::ghostflow::utils::TrailerRef;

use config::Branch;

/// The various state command requests may be in.
pub enum CommandState {
    /// The command was requested and is allowed.
    Requested(Vec<String>),
    /// The command is not available for the branch.
    Unavailable,
    /// The command was not requested.
    Unrequested,
    /// The command was requested, but the user lacked permissions.
    Disallowed,
}

impl CommandState {
    /// Returns the arguments to the command.
    pub fn arguments(&self) -> &[String] {
        match *self {
            CommandState::Requested(ref args) => args,
            CommandState::Unavailable |
            CommandState::Unrequested |
            CommandState::Disallowed => &[],
        }
    }

    /// Returns `true` if the command was present.
    pub fn present(&self) -> bool {
        match *self {
            CommandState::Requested(_) |
            CommandState::Unavailable |
            CommandState::Disallowed => true,
            CommandState::Unrequested => false,
        }
    }

    /// Returns `true` if the command was requested, but unavailable.
    pub fn unavailable(&self) -> bool {
        match *self {
            CommandState::Unavailable => true,
            CommandState::Requested(_) |
            CommandState::Unrequested |
            CommandState::Disallowed => false,
        }
    }

    /// Returns `true` if the command was requested and allowed.
    pub fn requested(&self) -> bool {
        match *self {
            CommandState::Requested(_) => true,
            CommandState::Unavailable |
            CommandState::Unrequested |
            CommandState::Disallowed => false,
        }
    }

    /// Returns `true` if the command was requested but disallowed.
    pub fn disallowed(&self) -> bool {
        match *self {
            CommandState::Disallowed => true,
            CommandState::Requested(_) |
            CommandState::Unavailable |
            CommandState::Unrequested => false,
        }
    }
}

/// Structure for the state of requested commands.
pub struct Commands<'a> {
    /// The branch the commands apply to.
    branch: &'a Branch,

    /// The `help` command.
    pub help: CommandState,
    /// The `stage` command.
    pub stage: CommandState,
    /// The `unstage` command.
    pub unstage: CommandState,
    /// The `merge` command.
    pub merge: CommandState,
    /// The `check` command.
    pub check: CommandState,
    /// The `test` command.
    pub test: CommandState,
    /// The `reformat` command.
    pub reformat: CommandState,

    /// List of unrecognized commands.
    unrecognized: Vec<String>,
}

macro_rules! check_action {
    // Actions with permission checks.
    ( $args:expr, $access:expr, $is_submitter:expr,
      ($action:expr, $branch_action:expr, $allow_submitter:expr) ) => {
        $action = if let Some(action) = $branch_action {
            if action.access_level <= $access || ($allow_submitter && $is_submitter) {
                CommandState::Requested($args.iter()
                    .map(|a| a.to_string())
                    .collect())
            } else {
                CommandState::Disallowed
            }
        } else {
            CommandState::Unavailable
        };
    };
    // Unconditional actions.
    ( $args:expr, $access:expr, $is_submitter:expr,
      ($action:expr) ) => {
        $action = CommandState::Requested($args.iter()
            .map(|a| a.to_string())
            .collect());
    };
}

macro_rules! check_actions {
    ( $command:expr, $args:expr, $access:expr, $is_submitter:expr, $unrecognized:expr,
      $( $name:expr => $action_spec:tt, )* ) => {
        match *$command {
            $( $name => { check_action!($args, $access, $is_submitter, $action_spec) }, )*
            unrecognized => $unrecognized.push(unrecognized.to_string()),
        }
    };
}

macro_rules! check_action_ok {
    { $method:ident, $( $action:expr => $name:expr, )* } => {
        let mut results = Vec::new();

        $( if $action.$method() {
            results.push($name);
        } )*

        results
    };
}

macro_rules! check_action_consistency {
    { $( ($action1:expr, $action2:expr) => ($name1:expr, $name2:expr), )* } => {
        let mut results = Vec::new();

        $( if $action1.requested() && $action2.requested() {
            results.push(concat!($name1, "` and `", $name2));
        } )*

        results
    };
}

impl<'a> Commands<'a> {
    /// Create a new commands parser for a branch.
    pub fn new(branch: &'a Branch) -> Self {
        Commands {
            branch: branch,

            help: CommandState::Unrequested,
            check: CommandState::Unrequested,
            stage: CommandState::Unrequested,
            unstage: CommandState::Unrequested,
            merge: CommandState::Unrequested,
            test: CommandState::Unrequested,
            reformat: CommandState::Unrequested,

            unrecognized: Vec::new(),
        }
    }

    /// Error messages from the commands.
    pub fn error_messages(&self, author: &User) -> Vec<String> {
        let mut messages = Vec::new();

        if !self.unrecognized.is_empty() {
            messages.push(format!("@{}: the following commands are not recognized at all: `{}`.",
                                  author.handle,
                                  self.unrecognized.join("`, `")));
        }

        let unavailable = self.unavailable_commands();
        if !unavailable.is_empty() {
            messages.push(format!("@{}: the following commands are not supported for `{}`: `{}`.",
                                  author.handle,
                                  self.branch.name,
                                  unavailable.join("`, `")));
        }

        let disallowed = self.disallowed_commands();
        if !disallowed.is_empty() {
            messages.push(format!("@{}: insufficient privileges for the commands for `{}`: `{}`.",
                                  author.handle,
                                  self.branch.name,
                                  disallowed.join("`, `")));
        }

        let consistency = self.consistency_messages();
        if !consistency.is_empty() {
            messages.push(format!("@{}: inconsistent commands: `{}`.",
                                  author.handle,
                                  consistency.join("`; `")));
        }

        messages
    }

    /// The set of unavailable commands.
    fn unavailable_commands(&self) -> Vec<&'static str> {
        check_action_ok! { unavailable,
            self.help => "help",
            self.check => "check",
            self.stage => "stage",
            self.unstage => "unstage",
            self.merge => "merge",
            self.test => "test",
            self.reformat => "reformat",
        }
    }

    /// The set of disallowed commands.
    fn disallowed_commands(&self) -> Vec<&'static str> {
        check_action_ok! { disallowed,
            self.help => "help",
            self.check => "check",
            self.stage => "stage",
            self.unstage => "unstage",
            self.merge => "merge",
            self.test => "test",
            self.reformat => "reformat",
        }
    }

    /// Checks whether the set of commands requested is consistent or not.
    ///
    /// Returns a list of messages about problems with the requested commands.
    fn consistency_messages(&self) -> Vec<&'static str> {
        check_action_consistency! {
            (self.check, self.stage) => ("check", "stage"),
            (self.check, self.unstage) => ("check", "unstage"),
            (self.check, self.merge) => ("check", "merge"),
            (self.check, self.reformat) => ("check", "reformat"),
            (self.stage, self.unstage) => ("stage", "unstage"),
            (self.stage, self.merge) => ("stage", "merge"),
            (self.stage, self.reformat) => ("stage", "reformat"),
            (self.unstage, self.reformat) => ("unstage", "reformat"),
            (self.test, self.merge) => ("test", "merge"),
            (self.test, self.reformat) => ("test", "reformat"),
            (self.merge, self.reformat) => ("merge", "reformat"),
        }
    }

    /// Parse commands from a set of trailers.
    pub fn add_from_trailers(&mut self, access: AccessLevel, is_submitter: bool,
                             trailers: &[TrailerRef])
                             -> &mut Self {
        for trailer in trailers.iter() {
            match trailer.token {
                "do" | "Do" => {
                    // TODO: handle quoting
                    let values = trailer.value
                        .split_whitespace()
                        .collect::<Vec<_>>();
                    let (command, args) = values.split_first()
                        .expect("the regular expression should require a non-whitespace value");
                    check_actions!(command, args, access, is_submitter, self.unrecognized,
                        "help" => (self.help),
                        "check" => (self.check),
                        "stage" => (self.stage, self.branch.stage(), false),
                        "unstage" => (self.unstage, self.branch.stage(), true),
                        "merge" => (self.merge, self.branch.merge(), false),
                        "test" => (self.test, self.branch.test(), false),
                        "reformat" => (self.reformat, self.branch.reformat(), true),
                    );
                },
                // Ignore other trailers.
                _ => (),
            }
        }

        self
    }

    /// Returns `true` if there are no commands which were processed.
    pub fn is_empty(&self) -> bool {
        self.unrecognized.is_empty() &&
            !self.help.present() &&
            !self.check.present() &&
            !self.stage.present() &&
            !self.unstage.present() &&
            !self.merge.present() &&
            !self.test.present() &&
            !self.reformat.present()
    }
}

#[cfg(test)]
mod tests {
    use crates::ghostflow::host::{AccessLevel, HostedProject, User};
    use crates::ghostflow::utils::TrailerRef;
    use crates::git_workarea::{GitContext, Identity};
    use crates::itertools;
    use crates::tempdir::TempDir;

    use config::{Branch, Checks};
    use config::io;
    use handlers::common::handlers::commands::{Commands, CommandState};
    use tests::utils;

    use std::process::Command;
    use std::sync::Arc;

    mod bogus {
        use crates::ghostflow::host::*;
        use crates::git_workarea::CommitId;

        pub struct BogusService;

        impl HostingService for BogusService {
            fn service_user(&self) -> &User {
                unreachable!("the bogus service should not be called")
            }

            fn add_member(&self, _: &str, _: &User, _: AccessLevel) -> Result<()> {
                unreachable!("the bogus service should not be called")
            }

            fn members(&self, _: &str) -> Result<Vec<Membership>> {
                unreachable!("the bogus service should not be called")
            }

            fn add_hook(&self, _: &str, _: &str) -> Result<()> {
                unreachable!("the bogus service should not be called")
            }

            fn user(&self, _: &str) -> Result<User> {
                unreachable!("the bogus service should not be called")
            }

            fn commit(&self, _: &str, _: &CommitId) -> Result<Commit> {
                unreachable!("the bogus service should not be called")
            }

            fn issue(&self, _: &str, _: u64) -> Result<Issue> {
                unreachable!("the bogus service should not be called")
            }

            fn merge_request(&self, _: &str, _: u64) -> Result<MergeRequest> {
                unreachable!("the bogus service should not be called")
            }

            fn repo(&self, _: &str) -> Result<Repo> {
                unreachable!("the bogus service should not be called")
            }

            fn user_by_id(&self, _: u64) -> Result<User> {
                unreachable!("the bogus service should not be called")
            }

            fn commit_by_id(&self, _: u64, _: &CommitId) -> Result<Commit> {
                unreachable!("the bogus service should not be called")
            }

            fn issue_by_id(&self, _: u64, _: u64) -> Result<Issue> {
                unreachable!("the bogus service should not be called")
            }

            fn merge_request_by_id(&self, _: u64, _: u64) -> Result<MergeRequest> {
                unreachable!("the bogus service should not be called")
            }

            fn repo_by_id(&self, _: u64) -> Result<Repo> {
                unreachable!("the bogus service should not be called")
            }

            fn get_issue_comments(&self, _: &Issue) -> Result<Vec<Comment>> {
                unreachable!("the bogus service should not be called")
            }

            fn post_issue_comment(&self, _: &Issue, _: &str) -> Result<()> {
                unreachable!("the bogus service should not be called")
            }

            fn get_mr_comments(&self, _: &MergeRequest) -> Result<Vec<Comment>> {
                unreachable!("the bogus service should not be called")
            }

            fn post_mr_comment(&self, _: &MergeRequest, _: &str) -> Result<()> {
                unreachable!("the bogus service should not be called")
            }

            fn post_commit_comment(&self, _: &Commit, _: &str) -> Result<()> {
                unreachable!("the bogus service should not be called")
            }

            fn get_commit_statuses(&self, _: &Commit) -> Result<Vec<CommitStatus>> {
                unreachable!("the bogus service should not be called")
            }

            fn post_commit_status(&self, _: PendingCommitStatus) -> Result<()> {
                unreachable!("the bogus service should not be called")
            }

            fn get_mr_awards(&self, _: &MergeRequest) -> Result<Vec<Award>> {
                unreachable!("the bogus service should not be called")
            }

            fn get_mr_comment_awards(&self, _: &MergeRequest, _: &Comment) -> Result<Vec<Award>> {
                unreachable!("the bogus service should not be called")
            }

            fn award_mr_comment(&self, _: &MergeRequest, _: &Comment, _: &str) -> Result<()> {
                unreachable!("the bogus service should not be called")
            }

            fn comment_award_name(&self) -> &str {
                unreachable!("the bogus service should not be called")
            }

            fn issues_closed_by_mr(&self, _: &MergeRequest) -> Result<Vec<Issue>> {
                unreachable!("the bogus service should not be called")
            }

            fn add_issue_labels(&self, _: &Issue, _: &[&str]) -> Result<()> {
                unreachable!("the bogus service should not be called")
            }
        }
    }
    use self::bogus::BogusService;

    fn create_user() -> User {
        User {
            id: 0,
            handle: "test-user".to_string(),
            name: "Test User".to_string(),
            email: "ghostflow-director-user@example.com".to_string(),
        }
    }

    fn create_branch() -> Branch {
        let ctx = GitContext::new(concat!(env!("CARGO_MANIFEST_DIR"), "/.git"));
        let identity = Identity::new("Ghostflow Director Test", "ghostflow-director@example.com");
        Branch::new("master", Checks::new(), ctx, identity)
    }

    fn create_branch_full(tempdir: &TempDir) -> Branch {
        let origindir = tempdir.path().join("origin");
        let clone = Command::new("git")
            .arg("clone")
            .arg("--bare")
            .arg(concat!(env!("CARGO_MANIFEST_DIR"), "/.git"))
            .arg(&origindir)
            .output()
            .unwrap();
        if !clone.status.success() {
            panic!("origin clone failed: {}",
                   String::from_utf8_lossy(&clone.stderr));
        }

        let originctx = GitContext::new(&origindir);
        let update_ref = originctx.git()
            .arg("update-ref")
            .arg("refs/heads/master")
            .arg("a191569eab9672b48ca49c9415bc9843bc8c90f2")
            .output()
            .unwrap();
        if !update_ref.status.success() {
            panic!("creating `master` failed: {}",
                   String::from_utf8_lossy(&update_ref.stderr));
        }

        let gitdir = tempdir.path().join("git");
        let clone = Command::new("git")
            .arg("clone")
            .arg("--bare")
            .arg(&origindir)
            .arg(&gitdir)
            .output()
            .unwrap();
        if !clone.status.success() {
            panic!("working clone failed: {}",
                   String::from_utf8_lossy(&clone.stderr));
        }

        let ctx = GitContext::new(gitdir);
        let identity = Identity::new("Ghostflow Director Test", "ghostflow-director@example.com");
        let mut branch = Branch::new("master", Checks::new(), ctx, identity);

        let project = HostedProject {
            name: "test-commands".to_string(),
            service: Arc::new(BogusService),
        };

        let follow_conf = io::Follow {
            ref_namespace: "follow".to_string(),
        };
        let merge_conf = io::Merge {
            quiet: true,
            log_limit: None,
            required_access_level: io::AccessLevelIO::Maintainer,
            policy: io::MergePolicy,
        };
        let reformat_conf = io::Reformat {
            required_access_level: io::AccessLevelIO::Maintainer,
            formatters: Vec::new(),
        };
        let stage_conf = io::Stage {
            quiet: true,
            required_access_level: io::AccessLevelIO::Maintainer,
            update_policy: io::StageUpdatePolicy::Ignore,
        };
        let test_conf = io::Test {
            required_access_level: io::AccessLevelIO::Maintainer,
            backend: io::TestBackend::Refs,
            config: json!({
                "quiet": true,
                "namespace": "test",
            }),
        };

        branch.add_follow(&follow_conf);
        branch.add_merge(&merge_conf, project.clone()).unwrap();
        branch.add_reformat(&reformat_conf, project.clone()).unwrap();
        branch.add_stage(&stage_conf, project.clone()).unwrap();
        branch.add_test(&test_conf, project.clone()).unwrap();

        branch
    }

    fn create_commands<'a>(branch: &'a Branch, content: &str) -> Commands<'a> {
        let trailers = TrailerRef::extract(content);
        let mut commands = Commands::new(branch);
        commands.add_from_trailers(AccessLevel::Developer, false, &trailers);

        commands
    }

    fn error_messages(content: &str) -> Vec<String> {
        let branch = create_branch();
        let user = create_user();

        let commands = create_commands(&branch, content);
        assert!(!commands.is_empty());
        commands.error_messages(&user)
    }

    fn check_error_messages<'a, I>(actual: I, messages: &[&str])
        where I: Iterator<Item = &'a String>,
    {
        itertools::assert_equal(actual, messages);
    }

    fn check_content_error_messages(content: &str, messages: &[&str]) {
        check_error_messages(error_messages(content).iter(), messages);
    }

    #[test]
    fn test_unknown_commands() {
        check_content_error_messages("Do: bogus",
            &["@test-user: the following commands are not recognized at all: `bogus`."]);
        check_content_error_messages("do: bogus",
            &["@test-user: the following commands are not recognized at all: `bogus`."]);
    }

    fn check_always_supported_command<F>(command: &str, get_state: F)
        where F: for<'a> Fn(&'a Commands) -> &'a CommandState,
    {
        let branch = create_branch();
        let user = create_user();

        {
            let upper_command = format!("Do: {}", command);
            let commands = create_commands(&branch, &upper_command);
            assert!(commands.error_messages(&user).is_empty());
            assert!(!commands.is_empty());
            let state = get_state(&commands);
            assert!(state.present());
            assert!(state.requested());
        }

        {
            let lower_command = format!("do: {}", command);
            let commands = create_commands(&branch, &lower_command);
            assert!(commands.error_messages(&user).is_empty());
            assert!(!commands.is_empty());
            let state = get_state(&commands);
            assert!(state.present());
            assert!(state.requested());
        }
    }

    #[test]
    fn test_always_supported_commands() {
        check_always_supported_command("check", |commands| &commands.check);
        check_always_supported_command("help", |commands| &commands.help);
    }

    fn check_unsupported_command(command: &str) {
        let upper_command = format!("Do: {}", command);
        let lower_command = format!("do: {}", command);
        let message = format!("@test-user: the following commands are not supported for `master`: `{}`.", command);
        check_content_error_messages(&upper_command, &[&message]);
        check_content_error_messages(&lower_command, &[&message]);
    }

    #[test]
    fn test_unsupported_commands() {
        check_unsupported_command("stage");
        check_unsupported_command("unstage");
        check_unsupported_command("merge");
        check_unsupported_command("test");
        check_unsupported_command("reformat");
    }

    fn check_disallowed_command(command: &str) {
        let tempdir = utils::test_workspace_dir(&format!("test_insufficient_permissions[{}]", command));
        let branch = create_branch_full(&tempdir);
        let user = create_user();

        let message = format!("@test-user: insufficient privileges for the commands for `master`: `{}`.", command);

        let commands = create_commands(&branch, &format!("Do: {}", command));
        check_error_messages(commands.error_messages(&user).iter(), &[&message]);
    }

    #[test]
    fn test_insufficient_permissions() {
        check_disallowed_command("stage");
        check_disallowed_command("unstage");
        check_disallowed_command("merge");
        check_disallowed_command("test");
        check_disallowed_command("reformat");
    }

    fn check_submitter_command(command: &str, is_allowed: bool) {
        let tempdir = utils::test_workspace_dir(&format!("test_submitter_permissions[{}]", command));
        let branch = create_branch_full(&tempdir);
        let user = create_user();

        let content = format!("Do: {}", command);
        let trailers = TrailerRef::extract(&content);
        let mut commands = Commands::new(&branch);
        commands.add_from_trailers(AccessLevel::Developer, true, &trailers);
        let errors = commands.error_messages(&user);

        if is_allowed {
            check_error_messages(errors.iter(), &[]);
        } else {
            let message = format!("@test-user: insufficient privileges for the commands for `master`: `{}`.", command);

            check_error_messages(errors.iter(), &[&message]);
        }
    }

    #[test]
    fn test_submitter_permissions() {
        check_submitter_command("stage", false);
        check_submitter_command("unstage", true);
        check_submitter_command("merge", false);
        check_submitter_command("test", false);
        check_submitter_command("reformat", true);
    }

    fn check_command_combination(cmd1: &str, cmd2: &str, is_allowed: bool) {
        let tempdir = utils::test_workspace_dir(&format!("test_command_consistency[{}, {}]", cmd1, cmd2));
        let branch = create_branch_full(&tempdir);
        let user = create_user();

        let content = format!("Do: {}\nDo: {}", cmd1, cmd2);
        let trailers = TrailerRef::extract(&content);
        let mut commands = Commands::new(&branch);
        commands.add_from_trailers(AccessLevel::Maintainer, true, &trailers);
        let errors = commands.error_messages(&user);

        if is_allowed {
            check_error_messages(errors.iter(), &[]);
        } else {
            let message = format!("@test-user: inconsistent commands: `{}` and `{}`.", cmd1, cmd2);

            check_error_messages(errors.iter(), &[&message]);
        }
    }

    macro_rules! command_combinations {
        ( $( $cmd1:expr, $cmd2:expr, $allowed:expr, )* ) => {
            $( check_command_combination($cmd1, $cmd2, $allowed); )*
        };
    }

    #[test]
    fn test_command_consistency() {
        command_combinations!(
            "help", "check", true,
            "help", "stage", true,
            "help", "unstage", true,
            "help", "test", true,
            "help", "merge", true,
            "help", "reformat", true,

            "check", "stage", false,
            "check", "unstage", false,
            "check", "test", true,
            "check", "merge", false,
            "check", "reformat", false,

            "stage", "unstage", false,
            "stage", "test", true,
            "stage", "merge", false,
            "stage", "reformat", false,

            "unstage", "test", true,
            "unstage", "merge", true,
            "unstage", "reformat", false,

            "test", "merge", false,
            "test", "reformat", false,

            "merge", "reformat", false,
        );
    }
}
