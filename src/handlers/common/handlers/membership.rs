// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::ghostflow::host::Repo;
use crates::json_job_dispatch::HandlerResult;
use crates::json_job_dispatch::Result as JobResult;
use crates::serde_json::Value;

use config::Host;
use handlers::common::data::{MembershipAdditionInfo, MembershipRemovalInfo};
use handlers::common::handlers::error::Error;
use handlers::common::handlers::utils;

/// Handle a user being added to a project.
pub fn handle_user_addition(_: &Value, host: &Host, membership: MembershipAdditionInfo)
                            -> JobResult<HandlerResult> {
    let project = try_action!(utils::get_project(host, &membership.repo.name));

    project.add_member(membership.membership);

    Ok(HandlerResult::Accept)
}

/// Handle a user being added to a project.
pub fn handle_user_removal(_: &Value, host: &Host, membership: MembershipRemovalInfo)
                           -> JobResult<HandlerResult> {
    let project = try_action!(utils::get_project(host, &membership.repo.name));

    project.remove_member(&membership.user);

    Ok(HandlerResult::Accept)
}

/// Handle a project's membership needing completely refreshed.
pub fn handle_project_membership_refresh(_: &Value, host: &Host, repo: &Repo)
                                         -> JobResult<HandlerResult> {
    let project = try_action!(utils::get_project(host, &repo.name));

    project.refresh_membership(&repo.name)
        .map(|_| HandlerResult::Accept)
        .or_else(|err| {
            Ok(HandlerResult::Fail(Error::chain(err,
                                                format!("failed to update membership \
                                                         for {}",
                                                        repo.name))))
        })
}

/// Handle a group's membership needing completely refreshed.
pub fn handle_group_membership_refresh(_: &Value, host: &Host, group: &str)
                                       -> JobResult<HandlerResult> {
    let prefix = format!("{}/", group);

    let (succeeded, failed) = host.projects
        .iter()
        .map(|(name, project)| {
            if name.starts_with(&prefix) {
                project.refresh_membership(name)
            } else {
                Ok(())
            }
        })
        .fold((0, 0), |(succeeded, failed), res| {
            match res {
                Ok(_) => (succeeded + 1, failed),
                Err(_) => (succeeded, failed + 1),
            }
        });

    Ok(if failed == 0 {
        HandlerResult::Accept
    } else {
        HandlerResult::Fail(Error::from_string(format!("failed to update {} repository \
                                                        memberships ({} succeeded)",
                                                       failed,
                                                       succeeded)))
    })
}
