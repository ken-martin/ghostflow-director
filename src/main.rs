// Copyright 2016 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

//! The Kitware robot
//!
//! This program watches a directory for JSON jobs in order to manage the Kitware workflow for
//! repositories.

#![warn(missing_docs)]

#[macro_use]
extern crate clap;

#[macro_use]
extern crate error_chain;

#[macro_use]
extern crate log;

#[macro_use]
extern crate serde_derive;

#[macro_use]
extern crate serde_json;

mod crates {
    pub extern crate chrono;
    pub extern crate clap;
    pub extern crate either;
    pub extern crate ghostflow;
    pub extern crate ghostflow_gitlab;
    pub extern crate git_checks;
    pub extern crate git_topic_stage;
    pub extern crate git_workarea;
    pub extern crate gitlab;
    pub extern crate itertools;
    pub extern crate json_job_dispatch;
    pub extern crate log;
    pub extern crate rayon;
    pub extern crate regex;
    pub extern crate serde;
    pub extern crate serde_json;
    pub extern crate serde_yaml;
    pub extern crate systemd;
    pub extern crate yaml_merge_keys;

    #[cfg(test)]
    pub extern crate tempdir;
}

use crates::clap::{App, Arg};
use crates::json_job_dispatch::{self, Director, DirectorWatchdog, Handler, RunResult};
use crates::log::LogLevel;
use crates::systemd::journal::JournalLog;

#[macro_use]
pub mod config;
use config::{Config, ConfigRead};

mod error;
use error::*;

pub mod handlers;

#[cfg(test)]
mod tests;

use std::path::Path;
use std::io::{self, Write};

// Include git information from `build.rs`.
include!(concat!(env!("OUT_DIR"), "/git_info.rs"));

/// Run the director with the configuration file's path.
fn run_director(config_path: &Path) -> Result<RunResult> {
    let mut config = Config::from_path(&config_path, handlers::connect_to_host)?;

    let watchdog = DirectorWatchdog {};
    let mut handlers = Vec::new();

    for (name, host) in config.hosts.drain() {
        handlers.push(handlers::create_handler(host, &name)?)
    }

    let mut director = Director::new(&config.archive_dir).chain_err(|| {
        format!("failed to create director for {}",
                config.archive_dir.display())
    })?;

    watchdog.add_to_director(&mut director)
        .chain_err(|| "failed to add watchdog to director")?;
    for handler in &handlers {
        handler.add_to_director(&mut director)
            .chain_err(|| "failed to add handler to director")?;
    }

    Ok(director.watch_directory(&config.queue_dir)
        .chain_err(|| format!("failed to watch directory {}", config.queue_dir.display()))?)
}

fn version_string() -> String {
    format!("{}{}",
            crate_version!(),
            if let Some(hash) = COMMIT_HASH {
                format!(" ({}{})",
                        hash,
                        if let Some(false) = WORKTREE_CLEAN {
                            " worktree dirty"
                        } else {
                            ""
                        })
            } else {
                String::new()
            })
}

/// A `main` function which supports `try!`.
fn try_main() -> Result<()> {
    let version = version_string();
    let matches = App::new("ghostflow-director")
        .version(version.as_str())
        .author("Ben Boeckel <ben.boeckel@kitware.com>")
        .about("Watch a directory for project events and perform workflow actions on them")
        .arg(Arg::with_name("CONFIG")
            .short("c")
            .long("config")
            .help("Path to the configuration file")
            .required(true)
            .takes_value(true))
        .arg(Arg::with_name("DEBUG")
            .short("d")
            .long("debug")
            .help("Increase verbosity")
            .multiple(true))
        .arg(Arg::with_name("VERIFY")
            .short("v")
            .long("verify")
            .help("Check the configuration file and exit"))
        .arg(Arg::with_name("DUMP_CONFIG")
            .long("dump-config")
            .help("Read the configuration file, dump it as JSON, and exit"))
        .arg(Arg::with_name("RELOAD")
            .long("reload")
            .help("Reload the configuration"))
        .arg(Arg::with_name("EXIT")
            .long("exit")
            .help("Cause the director to exit"))
        .arg(Arg::with_name("ARCHIVE")
            .long("archive")
            .help("Archive jobs into tarballs in an archive directory")
            .takes_value(true))
        .get_matches();

    let log_level = match matches.occurrences_of("DEBUG") {
        0 => LogLevel::Error,
        1 => LogLevel::Warn,
        2 => LogLevel::Info,
        3 => LogLevel::Debug,
        4 | _ => LogLevel::Trace,
    };
    JournalLog::init_with_level(log_level.to_log_level_filter())
        .chain_err(|| "failed to init logging")?;

    let config_path = Path::new(matches.value_of("CONFIG")
        .expect("the configuration option is required"));

    if matches.is_present("VERIFY") {
        let config = ConfigRead::from_path(&config_path)?;
        config.verify_all_check_configs()?;
    } else if matches.is_present("DUMP_CONFIG") {
        let config = ConfigRead::from_path(&config_path)?;
        let stdout = io::stdout();
        let mut out = stdout.lock();
        serde_json::to_writer_pretty(&mut out, &config)?;
        out.write(b"\n")?;
    } else if matches.is_present("EXIT") {
        let config = ConfigRead::from_path(&config_path)?;
        json_job_dispatch::utils::exit(config.queue_dir()).chain_err(|| "failed to exit")?;
    } else if matches.is_present("RELOAD") {
        let config = ConfigRead::from_path(&config_path)?;
        json_job_dispatch::utils::restart(config.queue_dir()).chain_err(|| "failed to restart")?;
    } else if matches.is_present("ARCHIVE") {
        let config = ConfigRead::from_path(&config_path)?;
        let archive_path = matches.value_of("ARCHIVE")
            .expect("the archive option requires a value");
        json_job_dispatch::utils::archive_queue(config.archive_dir(), archive_path)
            .chain_err(|| "failed to archive")?;
    } else {
        while let RunResult::Restart = run_director(config_path)? {
        }
    }

    Ok(())
}

/// The entry point.
///
/// Wraps around `try_main` to panic on errors.
fn main() {
    if let Err(err) = try_main() {
        panic!("{:?}", err);
    }
}
