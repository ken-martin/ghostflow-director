// Copyright 2017 Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

use crates::tempdir::TempDir;

use tests::log;

use std::env;

pub fn test_workspace_dir(test_name: &str) -> TempDir {
    log::setup_logging();

    let mut working_dir = env::current_exe().unwrap();
    working_dir.pop();

    TempDir::new_in(working_dir, test_name).unwrap()
}
